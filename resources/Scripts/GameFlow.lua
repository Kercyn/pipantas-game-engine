-- Initialization
--Camera = PCamera.new()
--Pipantas = PGame.new()

Player = {}
Characters = {}
Texts = {}

CharacterBodies = {}
CharacterAttackColliders = {}

CameraZoom = 22000

-- 	The coordinates of the previous position of each actor.
--	Needed to know whether or not an actor is falling and/or moving.
local PrevActorPos = {}

--	Pipantas flow	--
function Initialize()
	Camera:SetZoom(CameraZoom)
end

function OnLevelLoad()
	for Key, Character in pairs(Characters) do
		Character = nil;
		Characters[Key] = nil
	end
	
	Pipantas:GetCharacters("Characters")

	Player = PCharacter.new("Player")
	
	for Key, Character in pairs(Characters) do
		local ActorX, ActorY = Character:GetPosition()
		PrevActorPos[Key] = {}
		PrevActorPos[Key].x = ActorX
		PrevActorPos[Key].y = ActorY

		Character:SetOrientation(1.0, 1.0)
		
		CharacterBodies[Key] = PCollider.new(Character:GetColliderID("Body"))

		if Character:HasCollider("Attack") then
			Character:SetProperty("CanAttack", "true")
			CharacterAttackColliders[Key] = PCollider.new(Character:GetColliderID("Attack"))
		end
	end
	
	PlayCharAnimations()
end

function Update()
	CameraUpdate()
	
	Pipantas:GetTexts("Texts")
	local PlayerX, PlayerY = Player:GetPosition()

	Texts["txtLifeLabel"]:SetPosition(PlayerX - 790, PlayerY + 660)
	Texts["txtLifeCounter"]:SetPosition(PlayerX - 650, PlayerY + 660)
	Texts["txtLifeCounter"]:SetText(Player:GetProperty("CurrHP"))
end

function FixedUpdate()
	Pipantas:GetCharacters("Characters")
	
	HandleKeypress()
	UpdateCharFallingIdleState()
	PlayCharAnimations()
	
	for Key, Character in pairs(Characters) do
		local ActorX, ActorY = Character:GetPosition()
		PrevActorPos[Key] = {}
		PrevActorPos[Key].x = ActorX
		PrevActorPos[Key].y = ActorY
	end
end

function OnKeyDown(KeyName)
	local Keybinds = {
		Space = ControllableChar.StartJumping,
		Z = ControllableChar.StartAttacking,
		L = test}
	
	local Func = Keybinds[KeyName]
	
	if AreControlsEnabled() then
		if Func then
			Func("Player")
		end
	end
end


function OnKeyUp(KeyName)
local Keybinds = {
		Space = ControllableChar.StopJumping
		}
	
	local Func = Keybinds[KeyName]
	
	if AreControlsEnabled() then
		if Func then
			Func("Player")
		end
	end
end

--	User-defined functions	--

-- Checks if a character is either falling or is idle and updates its state accordingly.
function UpdateCharFallingIdleState()
	-- Characters have been updated in FixedUpdate, before this is called so we don't need to re-update them.
	for Key, Character in pairs(Characters) do
		local State = Character:GetProperty("State")

		if State ~= "dead" then
			ActorX, ActorY = Character:GetPosition()

			local PrevPos = {}
			PrevPos.x = PrevActorPos[Key].x
			PrevPos.y = PrevActorPos[Key].y

			local IsFalling = ActorY < PrevPos.y
			local IsMoving = ((ActorX - PrevPos.x) ~= 0.0)
			
			local CharID = Character:GetID()
			local _, BodyHeight = CharacterBodies[CharID]:GetSize()
			local HalfBodyHeight = BodyHeight / 2.0		
			
			local IsGrounded = Pipantas:Raycast(ActorX, ActorY, 0.0, -1.0, HalfBodyHeight + 31.0, "00000000001", false)
			Character:SetProperty("IsGrounded", tostring(IsGrounded))
			
			local IsNotAttacking = (State ~= "attacking") and (State ~= "jump_attacking") and (State ~= "fall_attacking")

			if IsNotAttacking then
				if IsFalling then
					if State ~= "jumping" then
						Character:SetProperty("State", "falling")
					end
				end

				if Character:GetProperty("IsGrounded") == "true" and not IsMoving then
					Character:SetProperty("State", "idle")
				end
			else
				if IsFalling then
					Character:SetProperty("State", "fall_attacking")
				end
			end
		end
	end
end

function PlayCharAnimations()

	for _, Character in pairs(Characters) do
		local State = Character:GetProperty("State")
		local Orientation, _ = Character:GetOrientation()
		local PlayInReverse
		
		if tonumber(Orientation) < 0.0 then
			PlayInReverse = true
		else
			PlayInReverse = false
		end
		
		if State == "idle" then
			Character:ChangeAnimation("IDLE", Pipantas:AnimInfLoop(), PlayInReverse)
		elseif State == "attacking" then
			Character:ChangeAnimation("ATTACK", 1, PlayInReverse)
		elseif State == "jump_attacking" or State == "fall_attacking" then
			Character:ChangeAnimation("JUMP_ATTACK", 1, PlayInReverse)
		elseif State == "running" then
			Character:ChangeAnimation("RUN", Pipantas:AnimInfLoop(), PlayInReverse)
		elseif State == "jumping" then
			Character:ChangeAnimation("JUMP", 1, PlayInReverse)
		elseif State == "falling" then
			Character:ChangeAnimation("GLIDE", Pipantas:AnimInfLoop(), PlayInReverse)
		elseif State == "dead" then
			Character:ChangeAnimation("DEATH", 1, PlayInReverse)
		end
	end
end

function HandleKeypress()
	local Keybinds = {
		Left = ControllableChar.MoveLeft,
		Right = ControllableChar.MoveRight,
		Space = ControllableChar.Jump
		}

	if AreControlsEnabled() then
		for Keycode, Func in pairs(Keybinds) do
			if Pipantas:IsKeyPressed(Keycode) then
				Func("Player")
			end
		end
	end
end
