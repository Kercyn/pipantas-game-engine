function Attack_OnTriggerEnter(ThisID, OtherID)
	-- Our attack collider only connects with enemies, so we know the collider has an attached actor.
	local ThisCollider = PCollider.new(ThisID)
	local OtherCollider = PCollider.new(OtherID)
	
	local AttackerID = ThisCollider:GetAttachedActorID()
	local EnemyID = OtherCollider:GetAttachedActorID()
	
	local Attacker = Characters[AttackerID]
	local Enemy = Characters[EnemyID]
	
	local AttackerDmg = tonumber(Attacker:GetProperty("Dmg"))
	local EnemyState = Enemy:GetProperty("State")

	if EnemyState ~= "dead" then
		local CurrHP = tonumber(Enemy:GetProperty("CurrHP")) - AttackerDmg
		Enemy:SetProperty("CurrHP", CurrHP)

		if CurrHP <= 0 then
			local Orientation, _ = Enemy:GetOrientation()
			-- ternary operator hack
			-- PlayInReverse = (Orientation == -1.0) ? : true : false
			local PlayInReverse = (Orientation == -1.0) and true or false;
			
			Enemy:SetProperty("State", "dead")
		end
	end
end

function Attack_OnTriggerExit(ThisID, OtherID)
end

function Attack_OnTriggerStay(ThisID, OtherID)
end
