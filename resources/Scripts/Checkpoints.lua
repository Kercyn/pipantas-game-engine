local CurrentCheckpoint = 0
local CurrentCheckpointX = 0
local CurrentCheckpointY = 0

UpdateCurrentCheckpoint = function(Number, x, y)
	if CurrentCheckpoint <= Number then
		CurrentCheckpoint = Number
		CurrentCheckpointX = x
		CurrentCheckpointY = y
	end
end

GetCurrentCheckpointPos = function()
	return CurrentCheckpointX, CurrentCheckpointY
end