#version 330 core

layout (std140) uniform shared_data
{
	mat4 camera;
};

uniform mat4 model_mat;
uniform vec2 tex_orientation;

in vec2 vert;
in vec2 vertTexCoord;

out vec2 fragTexCoord;

void main()
{
    fragTexCoord = vertTexCoord;
	fragTexCoord = tex_orientation * fragTexCoord;
    
    gl_Position = camera * model_mat * vec4(vert, 0.0, 1.0);
}
