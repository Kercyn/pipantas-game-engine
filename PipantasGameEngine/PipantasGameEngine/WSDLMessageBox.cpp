#include "WSDLMessageBox.hpp"

WSDLMessageBox::WSDLMessageBox(std::string Title_, std::string Message_, EMsgboxType MsgboxType_)
	: WMessageBox(Title_, Message_, MsgboxType)
{
	switch (MsgboxType)
	{
	case EMsgboxType::Error:
		Flags = SDL_MESSAGEBOX_ERROR;
		break;

	case EMsgboxType::Info:
		Flags = SDL_MESSAGEBOX_INFORMATION;
		break;

	case EMsgboxType::Warning:
		Flags = SDL_MESSAGEBOX_WARNING;
		break;
	}
}

WSDLMessageBox::~WSDLMessageBox()
{
}

int WSDLMessageBox::Show() const
{
	return SDL_ShowSimpleMessageBox(Flags, Title.c_str(), Message.c_str(), nullptr);
}