#include "PTriggerCollider.hpp"
#include "Managers.hpp"
#include "Pipanikos.hpp"

bool PTriggerCollider::IsTrigger() const
{
	return true;
}

void PTriggerCollider::SetProperties(const std::map<std::string, std::string>& NewProperties)
{
	Properties = NewProperties;
}

std::string PTriggerCollider::GetProperty(std::string PropertyID) const
{
	if (Properties.count(PropertyID) != 0)
	{
		return Properties.at(PropertyID);
	}
	else
	{
		throw(Pipanikos("Trigger collider " + ID + std::string(" has no property ") + PropertyID, __FILE__, __LINE__));
		return std::string();
	}
}
