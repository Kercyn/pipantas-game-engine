#include "MAudioPlayback.hpp"
#include "Managers.hpp"
#include "SDL_mixer.h"
#include "rapidxml.hpp"
#include "Pipanikos.hpp"

void MAudioPlayback::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	auto Flags = MIX_INIT_FLAC | MIX_INIT_OGG | MIX_INIT_MP3;

	if (Mix_Init(Flags) != Flags)
	{
		throw(Pipanikos("Failed to initialize mixer for flags " + std::to_string(Flags) + "\n\tMix error: " + std::string(Mix_GetError()), __FILE__, __LINE__));
	}

	using namespace rapidxml;

	std::string GameDataRaw = ResourceStore.RequestString(ResourceStore.Keys.GAMEDATA_FILE_KEY);
	std::vector<TCHAR> GameData(GameDataRaw.c_str(), GameDataRaw.c_str() + GameDataRaw.size() + 1);

	xml_document<> XMLDoc;
	XMLDoc.parse<0>(GameData.data());

	auto AudioNode = XMLDoc.first_node("game")->first_node("settings")->first_node("audio");
	if (AudioNode)
	{
		int Freq = 44100;
		Uint16 Format = AUDIO_S16SYS;
		int OutputChannels = 2;
		int Chunksize = 512;
		int MixingChannels = 16;

		auto FreqAttr = AudioNode->first_attribute("freq");
		auto OutputAttr = AudioNode->first_attribute("output");
		auto ChunksizeAttr = AudioNode->first_attribute("chunksize");
		auto MixChannelsAttr = AudioNode->first_attribute("mix_channels");

		if (FreqAttr == nullptr)
		{
			Logger.Log(ELogLevel::Warning, "Error parsing audio data: freq attribute doesn't exist. Defaulting to 44100.");
		}
		else if (OutputAttr == nullptr)
		{
			Logger.Log(ELogLevel::Warning, "Error parsing audio data: output attribute doesn't exist. Defaulting to stereo.");
		}
		else if (ChunksizeAttr == nullptr)
		{
			Logger.Log(ELogLevel::Warning, "Error parsing audio data: chunksize attribute doesn't exist. Defaulting to 512.");
		}
		else if (MixChannelsAttr == nullptr)
		{
			Logger.Log(ELogLevel::Warning, "Error parsing audio data: mix_channels attribute doesn't exist. Defaulting to 16.");
		}
		else
		{
			// All XML attributes exist.

			Freq = std::stoi(FreqAttr->value());
			Chunksize = std::stoi(ChunksizeAttr->value());
			MixingChannels = std::stoi(MixChannelsAttr->value());

			if (OutputAttr->value() == std::string("stereo"))
			{
				OutputChannels = 2;
			}
			else if (OutputAttr->value() == std::string("mono"))
			{
				OutputChannels = 1;
			}
			else
			{
				Logger.Log(ELogLevel::Warning, "Invalid output value. Defaulting to stereo.");
			}

			if (Mix_OpenAudio(Freq, Format, OutputChannels, Chunksize) == -1)
			{
				throw(Pipanikos("Failed to initialize mixer: " + std::string(Mix_GetError()), __FILE__, __LINE__));
			}

			Mix_AllocateChannels(MixingChannels);

			std::stringstream Message;
			Message << "Opened audio device with parameters:\n";
			Message << "\tFrequency: " << Freq << std::endl;
			Message << "\tFormat: 0x" << std::hex << Format << std::endl;
			Message << "\tOutput channels: " << std::dec << OutputChannels << std::endl;
			Message << "\tChunk size: " << Chunksize << std::endl;
			Message << "\tMixing channels: " << MixingChannels;

			Logger.Log(ELogLevel::Trace, Message.str());
		}
	}
	else
	{
		throw(Pipanikos("No audio node found.", __FILE__, __LINE__));
	}

	Logger.Log(ELogLevel::Info, "MAudioPlayback initialized.");
}

void MAudioPlayback::Shutdown(const KeyGameCoordinator &)
{
	// From the docs:
	// NOTE: Since each call to Mix_Init may set different flags, there is no way, currently, to request how many times each one was initted.
	// In other words, the only way to quit for sure is to do a loop like so:
	while (Mix_Init(0) != 0)
	{
		Mix_Quit();
	}

	Logger.Log(ELogLevel::Info, "MAudioPlayback shutdown.");
}

void MAudioPlayback::FadeInMusic(const KeyAudioController&, BMixMusic& Music, int Loops, int FadeTime)
{
	if (Mix_FadeInMusic(Music, Loops, FadeTime) == -1)
	{
		Logger.Log(ELogLevel::Warning, Mix_GetError());
	}
}

void MAudioPlayback::FadeOutMusic(const KeyAudioController&, int ms) const
{
	if (Mix_FadeOutMusic(ms) == 0)
	{
		Logger.Log(ELogLevel::Warning, Mix_GetError());
	}
}

void MAudioPlayback::FadeInChannel(const KeyAudioController&, std::size_t Channel, BMixChunk& Chunk, int Loops, int FadeTime)
{
	if (Mix_FadeInChannel(Channel, Chunk, Loops, FadeTime) == -1)
	{
		Logger.Log(ELogLevel::Warning, Mix_GetError());
	}
}

void MAudioPlayback::FadeOutChannel(const KeyAudioController&, std::size_t Channel, int FadeTime)
{
	int ChannelsFading = Mix_FadeOutChannel(Channel, FadeTime);
	if (ChannelsFading != 1)
	{
		Logger.Log(ELogLevel::Warning, "Fading out " + std::to_string(ChannelsFading) + std::string(" channels: ") + std::string(Mix_GetError()));
	}
}
