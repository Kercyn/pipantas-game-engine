#include "Utils.hpp"
#include "Defs.hpp"
#include "SDL.h"
#include "PEndPoint.hpp"
#include "ErrorHandling.hpp"
#include "zlib.h"
#include "Managers.hpp"

#include <fstream>
#include <iostream>
#include <ctime>
#include <string>

namespace Utils
{
	bool EndpointComparator(const PEndPoint& A, const PEndPoint& B)
	{
		if (A.Value != B.Value)
		{
			return A.Value < B.Value;
		}
		else
		{
			// If A and B's axis values are the same, then the "bigger"
			// endpoint is the one with the smaller (i.e. first created) collider ID.
			return A.ColliderID < B.ColliderID;
		}
	}

	std::string ExtractFilename(std::string Path)
	{
		size_t Pos = Path.rfind('/', Path.length());
		
		if (Pos != std::string::npos)
		{
			return Path.substr(Pos + 1, Path.length() - 1);
		}
		else
		{
			return Path;
		}
	}

	bool ToBool(std::string Str)
	{
		std::transform(Str.begin(), Str.end(), Str.begin(), ::tolower);
		return (Str == "true") ? true : false;
	}

	bool ToBool(char* CStr)
	{
		return ToBool(std::string(CStr));
	}

	bool ToBool(int Value)
	{
		return (Value != 0);
	}

	bool ToBool(float Value)
	{
		return (std::fabs(Value) > EPSILON);
	}

	std::string StripCRLF(const std::string Str)
	{
		std::string StrippedString = Str;
		StrippedString.erase(std::remove(StrippedString.begin(), StrippedString.end(), '\n'), StrippedString.end());

		return StrippedString;
	}

	std::string Trim(std::string Str)
	{
		std::string TrimmedString = Str;
		TrimmedString.erase(remove_if(TrimmedString.begin(), TrimmedString.end(), ::isspace), TrimmedString.end());

		return TrimmedString;
	}

	std::vector<std::string> Split(std::string Str, TCHAR Delim)
	{
		std::vector<std::string> Parts;

		auto Pos = Str.find_first_of(Delim);
		while (Pos != std::string::npos)
		{
			Parts.emplace_back(Str.substr(0, Pos));
			Str = Str.substr(Pos + 1, Str.size());
			Pos = Str.find_first_of(Delim);

			if (Pos == std::string::npos)
			{
				// No more delimiters found; add the remainder of the string to the Parts.
				Parts.emplace_back(Str);
			}
		}

		return Parts;
	}

	std::string strcompress(std::string const& str, int compressionLevel)
	{
		z_stream zs;
		memset(&zs, 0, sizeof(zs));

		if (deflateInit(&zs, compressionLevel) != Z_OK)
			throw(Pipanikos("deflateInit failed while decompressing with compression level " + std::to_string(compressionLevel), __FILE__, __LINE__));

		zs.next_in = (Bytef*)str.data();
		zs.avail_in = str.size();

		int ret;
		char outbuffer[32768];
		std::string outstring;

		do
		{
			zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
			zs.avail_out = sizeof(outbuffer);

			ret = deflate(&zs, Z_FINISH);

			if (outstring.size() < zs.total_out)
			{
				outstring.append(outbuffer, zs.total_out - outstring.size());
			}
		} while (ret == Z_OK);

		deflateEnd(&zs);

		if (ret != Z_STREAM_END)
		{
			std::ostringstream oss;
			oss << "Exception during zlib compression: (" << ret << ") " << zs.msg;
			throw(Pipanikos(oss.str(), __FILE__, __LINE__));
		}

		return outstring;
	}

	std::string strdecompress(std::string const& str)
	{
		z_stream zs;
		memset(&zs, 0, sizeof(zs));

		if (inflateInit(&zs) != Z_OK)
			throw(Pipanikos("inflateInit failed while decompressing", __FILE__, __LINE__));

		zs.next_in = (Bytef*)str.data();
		zs.avail_in = str.size();

		int ret;
		char outbuffer[32768];
		std::string outstring;

		do
		{
			zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
			zs.avail_out = sizeof(outbuffer);

			ret = inflate(&zs, 0);

			if (outstring.size() < zs.total_out)
			{
				outstring.append(outbuffer,
					zs.total_out - outstring.size());
			}

		} while (ret == Z_OK);

		inflateEnd(&zs);

		if (ret != Z_STREAM_END)
		{
			std::ostringstream oss;
			oss << "Exception during zlib decompression: (" << ret << ") " << zs.msg;
			throw(Pipanikos(oss.str(), __FILE__, __LINE__));
		}

		return outstring;
	}

	std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len) {
		std::string ret;
		int i = 0;
		int j = 0;
		unsigned char char_array_3[3];
		unsigned char char_array_4[4];

		while (in_len--) {
			char_array_3[i++] = *(bytes_to_encode++);
			if (i == 3) {
				char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
				char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
				char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
				char_array_4[3] = char_array_3[2] & 0x3f;

				for (i = 0; (i < 4); i++)
					ret += base64_chars[char_array_4[i]];
				i = 0;
			}
		}

		if (i)
		{
			for (j = i; j < 3; j++)
				char_array_3[j] = '\0';

			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for (j = 0; (j < i + 1); j++)
				ret += base64_chars[char_array_4[j]];

			while ((i++ < 3))
				ret += '=';

		}

		return ret;

	}
	std::string base64_decode(std::string const& encoded_string) {
		int in_len = encoded_string.size();
		int i = 0;
		int j = 0;
		int in_ = 0;
		unsigned char char_array_4[4], char_array_3[3];
		std::string ret;

		while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
			char_array_4[i++] = encoded_string[in_]; in_++;
			if (i == 4) {
				for (i = 0; i < 4; i++)
					char_array_4[i] = static_cast<unsigned char>(base64_chars.find(char_array_4[i]));

				char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
				char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
				char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

				for (i = 0; (i < 3); i++)
					ret += char_array_3[i];
				i = 0;
			}
		}

		if (i) {
			for (j = i; j <4; j++)
				char_array_4[j] = 0;

			for (j = 0; j <4; j++)
				char_array_4[j] = static_cast<unsigned char>(base64_chars.find(char_array_4[j]));

			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
		}

		return ret;
	}
}