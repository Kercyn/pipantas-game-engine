#pragma once

#include <memory>
#include <string>
#include <vector>

#include "SDL_ttf.h"
#include "SDL.h"
#include "physfs.h"

class MResourceLoader;

class BTTFFont final
{
	using FontPtr = std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)>;
	using RWopsPtr = std::unique_ptr<SDL_RWops, decltype(&SDL_FreeRW)>;
	
	public:
		BTTFFont();
		BTTFFont(FontPtr Font_, RWopsPtr RWops_, std::string Key_, int Size_);
		BTTFFont(BTTFFont&& Other);
		BTTFFont(const BTTFFont& Other) = delete;
		~BTTFFont() = default;

		BTTFFont& operator=(const BTTFFont& Other) = delete;
		BTTFFont& operator=(BTTFFont&& Other);

		operator TTF_Font*();

		friend MResourceLoader;

	private:
		std::string Key;
		FontPtr Font;
		RWopsPtr RWops;
		std::vector<PHYSFS_sint64> FontData;

		int Size;
};
