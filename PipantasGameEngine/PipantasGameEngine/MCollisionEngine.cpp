#include "MCollisionEngine.hpp"
#include "SShape.hpp"
#include "SCircle.hpp"
#include "SOBB.hpp"
#include "glm\glm.hpp"
#include "glm\gtx\rotate_vector.hpp"
#include "glm/gtx/norm.hpp"
#include "Utils.hpp"
#include "Managers.hpp"
#include "PLine.hpp"
#include "PTriggerCollider.hpp"
#include "rapidxml.hpp"
#include "AccessKeys.hpp"
#include "Pipanikos.hpp"

#include <boost\algorithm\clamp.hpp>
#include <memory>
#include <bitset>
#include <array>
#include <algorithm>
#include <limits>
#include <cmath>

MCollisionEngine::MCollisionEngine()
	: Colliders(std::map<std::uint16_t, PCollider*>()),
	TranslationVectors(std::map<std::string, glm::vec2>()),
	Gravity(glm::vec2())
{
	NarrowCollisionCallbacks[EShapeType::Circle][EShapeType::Circle] = &MCollisionEngine::CircleToCircle;
	NarrowCollisionCallbacks[EShapeType::Circle][EShapeType::OBB] = &MCollisionEngine::CircleToOBB;

	NarrowCollisionCallbacks[EShapeType::OBB][EShapeType::Circle] = &MCollisionEngine::OBBToCircle;
	NarrowCollisionCallbacks[EShapeType::OBB][EShapeType::OBB] = &MCollisionEngine::OBBToOBB;
}

void MCollisionEngine::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	using namespace rapidxml;

	std::string GameDataRaw = ResourceStore.RequestString(ResourceStore.Keys.GAMEDATA_FILE_KEY);
	std::vector<TCHAR> GameData(GameDataRaw.c_str(), GameDataRaw.c_str() + GameDataRaw.size() + 1);

	xml_document<> XMLDoc;
	XMLDoc.parse<0>(GameData.data());

	auto GravityNode = XMLDoc.first_node("game")->first_node("settings")->first_node("gravity");
	if (GravityNode != nullptr)
	{
		Gravity.x = std::stof(GravityNode->first_attribute("x")->value());
		Gravity.y = std::stof(GravityNode->first_attribute("y")->value());
	}

	ParseLayerData();

	Logger.Log(ELogLevel::Info, "MCollisionEngine initialized.");
}

void MCollisionEngine::Shutdown(const KeyGameCoordinator&)
{
	Logger.Log(ELogLevel::Info, "MCollisionEngine shutdown.");
}


void MCollisionEngine::Reset()
{
	Colliders.clear();
	ColliderIDGenerator.Reset();
}

void MCollisionEngine::Tick()
{
	TranslationVectors.clear();

	ApplyGravity();
	UpdateColliders();
	UpdateColliderTriggers();
	BroadPhase();
	NarrowPhase();
	TriggerColliders();
	CollisionResolution();
}

const std::map<std::uint16_t, PCollider*>& MCollisionEngine::GetColliders() const
{
	return Colliders;
}

const PCollider* const MCollisionEngine::GetCollider(std::uint16_t ID) const
{
	if (Colliders.count(ID) != 0)
	{
		return Colliders.at(ID);
	}
	else
	{
		throw(Pipanikos("Collider " + std::to_string(ID) + " not found.", __FILE__, __LINE__));
		return nullptr;
	}
}

PCollisionLayer* const MCollisionEngine::GetLayer(std::string Key) const
{
	if (Layers.count(Key) == 1)
	{
		return Layers.at(Key).get();
	}
	else
	{
		throw(Pipanikos("Layer with key " + Key + " not found.", __FILE__, __LINE__));
	}
}

void MCollisionEngine::RegisterCollider(PCollider* Collider, bool bIsColliderStatic)
{
	// New olliders can only be registered during loading.
	if (GameCoordinator.GetGameState() != EGameState::Loading)
	{
		throw(Pipanikos("Attempted to add a collider when not loading. Collider ID: " + std::to_string(Collider->ID) + std::string(" Layer index: ") + std::to_string(Collider->Layer->Index), __FILE__, __LINE__));
	}
	else
	{
		Colliders.emplace(Collider->ID, Collider);
	}
}

bool MCollisionEngine::BroadAABBToAABB(const SAABB* const a, const SAABB* const b)
{
	const glm::vec2& PosA = a->GetPosition();
	const glm::vec2& PosB = b->GetPosition();

	bool bIntersectX = ((a->GetMax().x + PosA.x) > (b->GetMin().x + PosB.x))
		&& ((a->GetMin().x + PosA.x) < (b->GetMax().x + PosB.x));

	bool bIntersectY = ((a->GetMax().y + PosA.y) > (b->GetMin().y + PosB.y))
		&& ((a->GetMin().y + PosA.y) < (b->GetMax().y + PosB.y));

	return (bIntersectX && bIntersectY);
}

bool MCollisionEngine::BroadCircleToShape(const SCircle& Circle, const SShape* const Shape)
{
	switch (Shape->GetType())
	{
		case EShapeType::AABB:
			return BroadCircleToAABB(Circle, static_cast<const SAABB*>(Shape));
			break;

		case EShapeType::Circle:
			return BroadCircleToCircle(Circle, static_cast<const SCircle*>(Shape));
			break;

		case EShapeType::OBB:
			return BroadCircleToOBB(Circle, static_cast<const SOBB*>(Shape));
			break;
	}
}

bool MCollisionEngine::BroadCircleToAABB(const SCircle& Circle, const SAABB* const AABB)
{
	return BroadCircleToAABB(Circle.GetPosition(), Circle.GetRadius(), *AABB);
}

bool MCollisionEngine::BroadCircleToAABB(const glm::vec2& Center, float Radius, const SAABB& AABB)
{
	// https://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection
	glm::vec2 Closest;
	glm::vec2 DistanceVec;

	const glm::vec2& BoxPos = AABB.GetPosition();

	//std::clamp has not been implemented yet
	Closest.x = boost::algorithm::clamp(Center.x, AABB.GetMin().x + BoxPos.x, AABB.GetMax().x + BoxPos.x);
	Closest.y = boost::algorithm::clamp(Center.y, AABB.GetMin().y + BoxPos.y, AABB.GetMax().y + BoxPos.y);

	DistanceVec.x = Center.x - Closest.x;
	DistanceVec.y = Center.y - Closest.y;

	float Distance = glm::length(DistanceVec);
	return Distance < Radius;
}

bool MCollisionEngine::BroadCircleToCircle(const SCircle& CircleA, const SCircle* const CircleB)
{
	float DistanceSq = glm::distance2(CircleA.GetPosition(), CircleB->GetPosition());
	float RadiiSq = std::pow(CircleA.GetRadius() + CircleB->GetRadius(), 2);

	// if DistanceSq == RadiiSq then the circles touch, so we treat equality
	// as if they don't collide for the purpose of this function.
	return (DistanceSq < RadiiSq);
}

bool MCollisionEngine::BroadCircleToOBB(const SCircle& Circle, const SOBB* const OBB)
{
	const auto& Normals = OBB->GetNormals();

	for (int i = 0; i < 2; i++)
	{
		const auto& Normal = Normals[i];

		auto CircleProj = Circle.ProjectInto(Normal);
		auto OBBProj = OBB->ProjectInto(Normal);

		if (glm::length(CircleProj.CalculatePenetrationVector(OBBProj)) < Utils::EPSILON)
		{
			return false;
		}
	}

	return true;
}

bool MCollisionEngine::RayIntersectsAABB(glm::vec2 Origin, glm::vec2 Direction, float MaxDistance, const SAABB* AABB)
{
	// https://tavianator.com/fast-branchless-raybounding-box-intersections/

	glm::vec2 InvDir = 1.0f / Direction;
	const glm::vec2& Min = AABB->GetMin();
	const glm::vec2& Max = AABB->GetMax();

	float tx1 = (Min.x - Origin.x) * InvDir.x;
	float tx2 = (Max.x - Origin.x) * InvDir.x;

	float tmin = std::min(tx1, tx2);
	float tmax = std::max(tx1, tx2);

	float ty1 = (Min.y - Origin.y) * InvDir.x;
	float ty2 = (Max.y - Origin.y) * InvDir.x;

	tmin = std::max(tmin, std::min(ty1, ty2));
	tmax = std::min(tmax, std::max(ty1, ty2));

	return tmax >= tmin;
}

bool MCollisionEngine::RayIntersectsCircle(glm::vec2 Origin, glm::vec2 Direction, float MaxDistance, const SCircle* Circle)
{
	// http://mathworld.wolfram.com/Circle-LineIntersection.html
	
	// The algorithm assumes the circle's center is at the origin (0,0),
	// so the ray's origin must take this into consideration.
	Origin -= Circle->GetPosition();

	float Radius = Circle->GetRadius();
	glm::vec2 Endpoint = Origin + (MaxDistance * Direction);
	glm::vec2 d = Endpoint - Origin;
	float dr = glm::length2(d);

	float D = (Origin.x * Endpoint.y) - (Endpoint.x * Origin.y);

	float Discriminant = (Radius * Radius * dr) - (D * D);

	return Discriminant >= 0.0f;
}

bool MCollisionEngine::RayIntersectsOBB(glm::vec2 Origin, glm::vec2 Direction, float MaxDistance, const SOBB* OBB)
{
	const glm::mat2 InvRotationMat = glm::inverse(OBB->GetRotationMat());
	const glm::vec2 InvOrigin = InvRotationMat * Origin;
	glm::vec2 InvDirection = glm::normalize(InvRotationMat * Direction);

	InvDirection = 1.0f / InvDirection;

	const auto& Pos = OBB->GetPosition();
	const float HalfHeight = OBB->GetHalfHeight();
	const float HalfWidth = OBB->GetHalfWidth();

	const glm::vec2 Min = glm::vec2(Pos.x - HalfWidth, Pos.y - HalfHeight);
	const glm::vec2 Max = glm::vec2(Pos.x + HalfWidth, Pos.y + HalfHeight);

	float t1 = (Min.x - Origin.x) * InvDirection.x;
	float t2 = (Max.x - Origin.x) * InvDirection.x;
	float t3 = (Min.y - Origin.y) * InvDirection.y;
	float t4 = (Max.y - Origin.y) * InvDirection.y;

	float tmin = std::max(std::min(t1, t2), std::min(t3, t4));
	float tmax = std::min(std::max(t1, t2), std::max(t3, t4));

	if ((tmax < 0.0f) || (tmin > tmax) || (tmin > MaxDistance))
	{
		return false;
	}

	return true;
}

bool MCollisionEngine::DoLayersCollide(const PCollisionLayer* const A, const PCollisionLayer* B) const
{
	return (A->CollisionMask.test(B->Index) && B->CollisionMask.test(A->Index));
}

bool MCollisionEngine::Raycast(glm::vec2 Origin, glm::vec2 Direction, float MaxDistance, std::string LayerMask, bool bHitTriggers) const
{
	auto SAPs = GetSAPsOfRay(Origin, Direction, MaxDistance);

	const auto CollisionMask = std::bitset<32>(LayerMask);

	for (const auto& SAP : SAPs)
	{
		for (const auto& KVP : SAP.get().GetColliders())
		{
			PCollider* const Collider = KVP.second;

			if (CollisionMask.test(Collider->Layer->Index))
			{
				SShape* Shape = Collider->Shape.get();
				switch (Shape->GetType())
				{
				case EShapeType::AABB:
					if (RayIntersectsAABB(Origin, Direction, MaxDistance, static_cast<SAABB*>(Shape)))
					{
						return true;
					}
					break;

				case EShapeType::Circle:
					if (RayIntersectsCircle(Origin, Direction, MaxDistance, static_cast<SCircle*>(Shape)))
					{
						return true;
					}
					break;

				case EShapeType::OBB:
					if (RayIntersectsOBB(Origin, Direction, MaxDistance, static_cast<SOBB*>(Shape)))
					{
						return true;
					}
					break;
				}
			}
		}
	}

	return false;
}

std::vector<std::reference_wrapper<const PSAP>> MCollisionEngine::GetSAPsOfCircle(const glm::vec2& Center, float Radius) const
{
	std::vector<std::reference_wrapper<const PSAP>> SAPs;

	for (const auto& SAP : PairGenerator.GetSAPs())
	{
		if (BroadCircleToAABB(Center, Radius, SAP.GetBoundaries()))
		{
			SAPs.emplace_back(std::cref(SAP));
		}
	}

	return SAPs;
}

std::vector<std::reference_wrapper<const PSAP>> MCollisionEngine::GetSAPsOfRay(const glm::vec2& Origin, const glm::vec2& Direction, float MaxDistance) const
{
	std::vector<std::reference_wrapper<const PSAP>> SAPs;

	for (const auto& SAP : PairGenerator.GetSAPs())
	{
		if (RayIntersectsAABB(Origin, Direction, MaxDistance, &SAP.GetBoundaries()))
		{
			SAPs.emplace_back(std::cref(SAP));
		}
	}

	return SAPs;
}

std::size_t MCollisionEngine::GenerateColliderID()
{
	return ColliderIDGenerator.RequestID();
}

void MCollisionEngine::ApplyGravity() const
{
	for (auto Actor : ActorHandler.GetAllActors())
	{
		const auto& Rigidbody = Actor->GetRigidbody();

		if (Actor->IsEnabled() && Rigidbody.bUsesGravity)
		{
			if ((Actor->GetRigidbody().Constraints & EMovementConstraints::RestrictY) != EMovementConstraints::RestrictY)
			{
				Actor->Translate(Rigidbody.Mass * Gravity * GameCoordinator.GetDeltaTime());
			}
		}
	}
}

void MCollisionEngine::UpdateColliders()
{
	for (auto KVP : Colliders)
	{
		PCollider* Collider = KVP.second;

		if (Collider->bNeedsUpdating)
		{
			Collider->Update();
		}
	}
}

void MCollisionEngine::UpdateColliderTriggers()
{
	for (auto KVP : Colliders)
	{
		if (KVP.second->TriggerType != ETriggerType::None)
		{
			auto TriggerCollider = static_cast<PTriggerCollider*>(KVP.second);
			TriggerCollider->PrevCollisions.clear();

			for (const auto& Collider : TriggerCollider->CurrCollisions)
			{
				TriggerCollider->PrevCollisions.emplace_back(Collider->GetID());
			}

			TriggerCollider->CurrCollisions.clear();
		}
	}
}

void MCollisionEngine::BroadPhase()
{
	PairGenerator.GeneratePairs({});
}

void MCollisionEngine::NarrowPhase()
{
	for (const auto& Pair : PairGenerator.GetCollidingPairs())
	{
		const auto ColliderA = Colliders.at(Pair.first);
		const auto ColliderB = Colliders.at(Pair.second);

		auto ShapeTypeA = ColliderA->Shape->GetType();
		auto ShapeTypeB = ColliderB->Shape->GetType();

		(this->*NarrowCollisionCallbacks[ShapeTypeA][ShapeTypeB])(ColliderA, ColliderB);
	}
}

void MCollisionEngine::TriggerColliders()
{
	for (auto KVP : Colliders)
	{
		if (KVP.second->TriggerType != ETriggerType::None)
		{
			auto TriggerCollider = static_cast<PTriggerCollider*>(KVP.second);
			bool bIsProgrammableTrigger = (TriggerCollider->TriggerType == ETriggerType::Programmable);

			std::string ColliderName = TriggerCollider->GetName();

			for (const auto Collider : TriggerCollider->CurrCollisions)
			{
				if (std::find(TriggerCollider->PrevCollisions.begin(), TriggerCollider->PrevCollisions.end(), Collider->GetID()) == std::end(TriggerCollider->PrevCollisions))
				{
					if (bIsProgrammableTrigger)
					{
						ScriptMiddleware.AddOnTriggerEvent(EPhysicsTrigger::Enter, ColliderName, static_cast<LUA_INTEGER>(TriggerCollider->GetID()), static_cast<LUA_INTEGER>(Collider->GetID()));
					}
					else
					{
						// We KNOW the attached actor is an AAudioArea, hence static_cast.
						auto AudioArea = static_cast<AAudioArea*>(TriggerCollider->AttachedActor);
						AudioController.AddOnTriggerEvent(EPhysicsTrigger::Enter, AudioArea);
					}
				}
				else
				{
					if (bIsProgrammableTrigger)
					{
						ScriptMiddleware.AddOnTriggerEvent(EPhysicsTrigger::Stay, ColliderName, static_cast<LUA_INTEGER>(TriggerCollider->GetID()), static_cast<LUA_INTEGER>(Collider->GetID()));
					}
					else
					{
						auto AudioArea = static_cast<AAudioArea*>(TriggerCollider->AttachedActor);
						AudioController.AddOnTriggerEvent(EPhysicsTrigger::Stay, AudioArea);
					}
				}
			}

			for (auto& ColliderID : TriggerCollider->PrevCollisions)
			{
				if (std::find_if(TriggerCollider->CurrCollisions.begin(), TriggerCollider->CurrCollisions.end(),
					[ColliderID](const PCollider* C) { return C->GetID() == ColliderID; }) == std::end(TriggerCollider->CurrCollisions))
				{
					if (bIsProgrammableTrigger)
					{
						ScriptMiddleware.AddOnTriggerEvent(EPhysicsTrigger::Exit, ColliderName, static_cast<LUA_INTEGER>(TriggerCollider->GetID()), static_cast<LUA_INTEGER>(ColliderID));
					}
					else
					{
						auto AudioArea = static_cast<AAudioArea*>(TriggerCollider->AttachedActor);
						AudioController.AddOnTriggerEvent(EPhysicsTrigger::Exit, AudioArea);
					}
				}
			}
		}
	}
}

void MCollisionEngine::CollisionResolution()
{
	for (const auto& KVP : TranslationVectors)
	{
		auto Actor = ActorHandler.GetActor(KVP.first);
		Actor->Translate(KVP.second);
		Actor->Update();
	}
}

void MCollisionEngine::ParseLayerData()
{
	using namespace rapidxml;

	std::string GameDataRaw = ResourceStore.RequestString(ResourceStore.Keys.GAMEDATA_FILE_KEY);
	std::vector<TCHAR> GameData(GameDataRaw.c_str(), GameDataRaw.c_str() + GameDataRaw.size() + 1);

	xml_document<> XMLDoc;
	XMLDoc.parse<0>(GameData.data());

	auto LayerNode = XMLDoc.first_node("game")->first_node("physics")->first_node("layers")->first_node("layer");

	while (LayerNode != nullptr)
	{
		std::string Key = LayerNode->first_attribute("key")->value();
		std::string CollisionTable = LayerNode->first_attribute("collision_table")->value();
		int Index = std::stoi(LayerNode->first_attribute("index")->value());

		Layers.emplace(Key, std::make_unique<PCollisionLayer>(Index, std::bitset<32>(CollisionTable)));

		LayerNode = LayerNode->next_sibling();
	}
}

void MCollisionEngine::CircleToCircle(PCollider* a, PCollider* b)
{
	SCircle* A = static_cast<SCircle*>(a->Shape.get());
	SCircle* B = static_cast<SCircle*>(b->Shape.get());

	glm::vec2 PosA = a->GetAbsolutePosition();
	glm::vec2 PosB = b->GetAbsolutePosition();

	// Positional information to properly adjust axis signs
	bool AisUp = (PosA.y > PosB.y) ? true : false;
	bool AisRight = (PosA.x > PosB.x) ? true : false;

	glm::vec2 Normal = PosB - PosA;
	glm::vec2 Axis = glm::normalize(Normal);
	float Distance = glm::length(Normal);
	float RadiiSum = A->GetRadius() + B->GetRadius();
	float MinOverlap = 0.0f;

	if (Distance < RadiiSum)
	{
		MinOverlap = Distance - RadiiSum;
	}

	// Minimum translation vector
	glm::vec2 MTV = MinOverlap * Axis;

	if (a->IsTrigger() || b->IsTrigger())
	{
		// If the MTV is non-zero...
		if (glm::any(glm::greaterThan(glm::abs(MTV), glm::vec2(Utils::EPSILON))))
		{
			if (a->IsTrigger())
			{
				auto TriggerCollider = static_cast<PTriggerCollider*>(a);
				TriggerCollider->CurrCollisions.emplace_back(b);
			}
			if (b->IsTrigger())
			{
				auto TriggerCollider = static_cast<PTriggerCollider*>(b);
				TriggerCollider->CurrCollisions.emplace_back(a);
			}
		}
	}
	else
	{
		// Find which collider has the actor
		const AActor* const ActorA = a->AttachedActor;
		if (ActorA != nullptr)
		{
			if (!a->IsStatic())
			{
				auto& TranslationVec = TranslationVectors[ActorA->GetID()];
				if (std::abs(TranslationVec.x) < std::abs(MTV.x))
				{
					TranslationVec.x = (AisRight) ? std::abs(MTV.x) : -std::abs(MTV.x);
				}

				if (std::abs(TranslationVec.y) < std::abs(MTV.y))
				{
					TranslationVec.y = (AisUp) ? std::abs(MTV.y) : -std::abs(MTV.y);
				}
			}
		}

		const AActor* const ActorB = b->AttachedActor;
		if (ActorB != nullptr)
		{
			if (!b->IsStatic())
			{
				auto& TranslationVec = TranslationVectors[ActorB->GetID()];
				if (std::abs(TranslationVec.x) < std::abs(MTV.x))
				{
					TranslationVec.x = (!AisRight) ? std::abs(MTV.x) : -std::abs(MTV.x);
				}

				if (std::abs(TranslationVec.y) < std::abs(MTV.y))
				{
					TranslationVec.y = (!AisUp) ? std::abs(MTV.y) : -std::abs(MTV.y);
				}
			}
		}
	}
}

void MCollisionEngine::CircleToOBB(PCollider* a, PCollider* b)
{
	SCircle* Circle = static_cast<SCircle*>(a->Shape.get());
	SOBB* OBB = static_cast<SOBB*>(b->Shape.get());
	float MinOverlap = std::numeric_limits<float>::max();
	glm::vec2 BestAxis;

	const glm::vec2& CircleCenter = Circle->GetPosition();
	const glm::vec2& OBBPos = OBB->GetPosition();
	const float HalfWidth = OBB->GetHalfWidth();
	const float HalfHeight = OBB->GetHalfHeight();
	
	// Positional information to properly adjust axis signs
	bool CircleIsUp = (CircleCenter.y > OBBPos.y) ? true : false;
	bool CircleIsRight = (CircleCenter.x > OBBPos.x) ? true : false;

	const auto& Normals = OBB->GetNormals();

	// Find the vertex of the OBB that is closest to the circle's center.
	glm::vec2 ClosestVertex;
	float MinDistSq = std::numeric_limits<float>::max();

	for (auto Vertex : OBB->GetVertices())
	{
		// GetVertices does not returned the "real" (translated, rotated, scaled) 
		// position of the vertices, so we must do some magic first.
		glm::vec4 v4 = glm::vec4(Vertex, 0.0f, 1.0f);
		v4 = OBB->GetModelMatrix() * v4;
		Vertex = glm::vec2(v4.x, v4.y);

		float DistSq = glm::distance2(CircleCenter, Vertex);

		if (DistSq < MinDistSq)
		{
			ClosestVertex = Vertex;
		}
	}

	// OBB-Circle SAT needs an extra axis;
	// the one that connects the circle center to the OBB's closest vertex to the center.
	glm::vec2 CenterVertexAxis = glm::normalize(CircleCenter - ClosestVertex);

	auto ProjCircle = Circle->ProjectInto(CenterVertexAxis);
	auto ProjOBB = OBB->ProjectInto(CenterVertexAxis);

	auto PenVector = ProjCircle.CalculatePenetrationVector(ProjOBB);
	auto Overlap = glm::length(PenVector);

	if (Overlap < Utils::EPSILON)
	{
		// If there is no intersection on at least one axis, then the objects
		// cannot be colliding.
		return;
	}
	else if (Overlap < MinOverlap)
	{
		MinOverlap = Overlap;
		BestAxis = CenterVertexAxis;
	}


	for (std::size_t i = 0; i < 2; i++)
	{
		const auto& Normal = Normals[i];

		auto ProjCircle = Circle->ProjectInto(Normal);
		auto ProjOBB = OBB->ProjectInto(Normal);

		auto PenVector = ProjCircle.CalculatePenetrationVector(ProjOBB);
		auto Overlap = glm::length(PenVector);

		if (Overlap < Utils::EPSILON)
		{
			// If there is no intersection on at least one axis, then the objects
			// cannot be colliding.
			return;
		}
		else if (Overlap < MinOverlap)
		{
			MinOverlap = Overlap;
			BestAxis = Normal;
		}
	}

	glm::vec2 MTV = MinOverlap * BestAxis;

	if (a->IsTrigger() || b->IsTrigger())
	{
		// If the MTV is non-zero...
		if (glm::any(glm::greaterThan(glm::abs(MTV), glm::vec2(Utils::EPSILON))))
		{
			if (a->IsTrigger())
			{
				auto TriggerCollider = static_cast<PTriggerCollider*>(a);
				TriggerCollider->CurrCollisions.emplace_back(b);
			}
			if (b->IsTrigger())
			{
				auto TriggerCollider = static_cast<PTriggerCollider*>(b);
				TriggerCollider->CurrCollisions.emplace_back(a);
			}
		}
	}
	else
	{
		// Find which collider has the actor
		const AActor* const ActorA = a->AttachedActor;
		if (ActorA != nullptr)
		{
			if (!a->IsStatic())
			{
				auto& TranslationVec = TranslationVectors[ActorA->GetID()];
				if (std::abs(TranslationVec.x) < std::abs(MTV.x))
				{
					TranslationVec.x = (CircleIsRight) ? std::abs(MTV.x) : -std::abs(MTV.x);
				}

				if (std::abs(TranslationVec.y) < std::abs(MTV.y))
				{
					TranslationVec.y = (CircleIsUp) ? std::abs(MTV.y) : -std::abs(MTV.y);
				}
			}
		}

		const AActor* const ActorB = b->AttachedActor;
		if (ActorB != nullptr)
		{
			if (!b->IsStatic())
			{
				auto& TranslationVec = TranslationVectors[ActorB->GetID()];
				if (std::abs(TranslationVec.x) < std::abs(MTV.x))
				{
					TranslationVec.x = (CircleIsRight) ? -std::abs(MTV.x) : std::abs(MTV.x);
				}

				if (std::abs(TranslationVec.y) < std::abs(MTV.y))
				{
					TranslationVec.y = (CircleIsUp) ? -std::abs(MTV.y) : std::abs(MTV.y);
				}
			}
		}
	}
}

void MCollisionEngine::OBBToCircle(PCollider* a, PCollider* b)
{
	CircleToOBB(b, a);
}

void MCollisionEngine::OBBToOBB(PCollider* a, PCollider* b)
{
	SOBB* A = static_cast<SOBB*>(a->Shape.get());
	SOBB* B = static_cast<SOBB*>(b->Shape.get());

	glm::vec2 PosA = a->GetAbsolutePosition();
	glm::vec2 PosB = b->GetAbsolutePosition();

	// Positional information to properly adjust axis signs
	bool AisUp = (PosA.y > PosB.y) ? true : false;
	bool AisRight = (PosA.x > PosB.x) ? true : false;

	float MinOverlap = std::numeric_limits<float>::max();
	glm::vec2 BestAxis;

	const auto& NormalsA = A->GetNormals();
	const auto& NormalsB = B->GetNormals();

	for (std::size_t i = 0; i < 2; i++)
	{
		const auto& Normal = NormalsA[i];

		auto ProjA = A->ProjectInto(Normal);
		auto ProjB = B->ProjectInto(Normal);

		auto PenVector = ProjA.CalculatePenetrationVector(ProjB);
		auto Overlap = glm::length(PenVector);

		if (Overlap < Utils::EPSILON)
		{
			// If there is no intersection on at least one axis, then the objects
			// cannot be colliding.
			return;
		}
		else if (Overlap < MinOverlap)
		{
			MinOverlap = Overlap;
			BestAxis = Normal;
		}
	}

	for (std::size_t i = 0; i < 2; i++)
	{
		const auto& Normal = NormalsB[i];

		auto ProjA = A->ProjectInto(Normal);
		auto ProjB = B->ProjectInto(Normal);

		auto PenVector = ProjA.CalculatePenetrationVector(ProjB);
		auto Overlap = glm::length(PenVector);

		if (Overlap < Utils::EPSILON)
		{
			// If there is no intersection on at least one axis, then the objects
			// cannot be colliding.
			return;
		}
		else if (Overlap < MinOverlap)
		{
			MinOverlap = Overlap;
			BestAxis = Normal;
		}
	}

	// Minimum translation vector
	glm::vec2 MTV = MinOverlap * BestAxis;

	if (a->IsTrigger() || b->IsTrigger())
	{
		// If the MTV is non-zero...
		if (glm::any(glm::greaterThan(glm::abs(MTV), glm::vec2(Utils::EPSILON))))
		{
			if (a->IsTrigger())
			{
				auto TriggerCollider = static_cast<PTriggerCollider*>(a);
				TriggerCollider->CurrCollisions.emplace_back(b);
			}
			if (b->IsTrigger())
			{
				auto TriggerCollider = static_cast<PTriggerCollider*>(b);
				TriggerCollider->CurrCollisions.emplace_back(a);
			}
		}
	}
	else
	{
		// Find which collider has the actor
		const AActor* const ActorA = a->AttachedActor;
		if (ActorA != nullptr)
		{
			if (!a->IsStatic())
			{
				auto& TranslationVec = TranslationVectors[ActorA->GetID()];
				if (std::abs(TranslationVec.x) < std::abs(MTV.x))
				{
					TranslationVec.x = (AisRight) ? std::abs(MTV.x) : -std::abs(MTV.x);
				}

				if (std::abs(TranslationVec.y) < std::abs(MTV.y))
				{
					TranslationVec.y = (AisUp) ? std::abs(MTV.y) : -std::abs(MTV.y);
				}
			}
		}

		const AActor* const ActorB = b->AttachedActor;
		if (ActorB != nullptr)
		{
			if (!b->IsStatic())
			{
				auto& TranslationVec = TranslationVectors[ActorB->GetID()];
				if (std::abs(TranslationVec.x) < std::abs(MTV.x))
				{
					TranslationVec.x = (!AisRight) ? std::abs(MTV.x) : -std::abs(MTV.x);
				}

				if (std::abs(TranslationVec.y) < std::abs(MTV.y))
				{
					TranslationVec.y = (!AisUp) ? std::abs(MTV.y) : -std::abs(MTV.y);
				}
			}
		}
	}
}
