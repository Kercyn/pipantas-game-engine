#include "MSceneSwitcher.hpp"
#include "Managers.hpp"
#include "rapidxml.hpp"
#include "glm\glm.hpp"
#include "MapElements.hpp"
#include "AActor.hpp"
#include "AText.hpp"
#include "AAudioArea.hpp"
#include "AccessKeys.hpp"
#include "Utils.hpp"
#include "Pipanikos.hpp"

#include <algorithm>

void MSceneSwitcher::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	using namespace rapidxml;
	std::string GameDataRaw = ResourceStore.RequestString(ResourceStore.Keys.GAMEDATA_FILE_KEY);
	std::vector<TCHAR> GameData = std::vector<TCHAR>(GameDataRaw.c_str(), GameDataRaw.c_str() + GameDataRaw.length() + 1);

	xml_document<> XMLDoc;
	XMLDoc.parse<0>(GameData.data());

	// Skip through stuff to get the first map.
	// Resource manager has already been initialized, so the gamedata file has already been checked for (some) errors.

	auto FirstSceneAttr = XMLDoc.first_node("game")->first_node("scenes")->first_attribute("first_scene");
	FirstSceneKey = FirstSceneAttr->value();

	Logger.Log(ELogLevel::Info, "MSceneSwitcher initialized.");
}

void MSceneSwitcher::Shutdown(const KeyGameCoordinator&)
{
	Logger.Log(ELogLevel::Info, "MSceneSwitcher shutdown.");
}

void MSceneSwitcher::ChangeScene(std::string ID)
{
	using namespace MapElements;
	
	GameCoordinator.RequestLoading({});
	
	Renderer.Render();

	ActorHandler.Reset();
	AudioController.Reset();
	PairGenerator.Reset();
	CollisionEngine.Reset();
	ParallaxStore.Reset();
	Renderer.Reset();
	
	CurrentScene = std::make_unique<CScene>();

	CurrentScene->Load(ID);
	const auto CurrentMap = &CurrentScene->LevelMap;
	
	auto ActorLayers = CurrentMap->GetObjectLayersOfType(EObjectLayerType::Actors);
	auto CharacterLayers = CurrentMap->GetObjectLayersOfType(EObjectLayerType::Characters);
	auto TextLayers = CurrentMap->GetObjectLayersOfType(EObjectLayerType::Text);
	auto SoundLayers = CurrentMap->GetObjectLayersOfType(EObjectLayerType::Sounds);

	for (const auto& ActorLayer : ActorLayers)
	{
		for (auto& ActorSpawner : ActorLayer.Objects)
		{
			ActorHandler.AddActor({}, ActorSpawner, EActorType::Actor);
			ActorHandler.UpdateActorInformation({}, ActorSpawner);
		}
	}

	for (const auto& CharacterLayer : CharacterLayers)
	{
		for (auto& CharacterSpawner : CharacterLayer.Objects)
		{
			ActorHandler.AddActor({}, CharacterSpawner, EActorType::Character);
			ActorHandler.UpdateActorInformation({}, CharacterSpawner);
		}
	}

	for (const auto& TextLayer : TextLayers)
	{
		for (auto& TextSpawner : TextLayer.Objects)
		{
			ActorHandler.AddActor({}, TextSpawner, EActorType::Text);
			ActorHandler.UpdateActorInformation({}, TextSpawner);
		}
	}

	for (const auto& SoundLayer : SoundLayers)
	{
		for (auto& SoundArea : SoundLayer.Objects)
		{
			ActorHandler.AddActor({}, SoundArea, EActorType::Audio);
			ActorHandler.UpdateActorInformation({}, SoundArea);
		}

		ActorHandler.GenerateAudioAreaColliders({}, SoundLayer);
	}
	
	auto SceneResources = CalculateSceneResources();

	PopulateParallaxPaths();
	ParallaxStore.UpdateParallaxesForScene();
	ResourceLoader.UnloadUnusedResources(SceneResources);
	ResourceLoader.LoadSceneResources(SceneResources);
	CollisionEngine.UpdateColliders();
	ScriptMiddleware.LoadMapScripts(ID);
	ScriptMiddleware.OnLevelLoad();
	PairGenerator.UpdateSAPAreas({});
	Renderer.InitializeParallaxRenderData();
	Renderer.GenerateNewMapRenderData();
	
	Logger.Log(ELogLevel::Info, "Scene switched to " + ID);
	GameCoordinator.FinishedLoading({});
}

const CMap* const MSceneSwitcher::GetCurrentMap() const
{
	return &CurrentScene->LevelMap;
}

std::string MSceneSwitcher::GetFirstSceneKey() const
{
	return FirstSceneKey;
}

const CScene* const MSceneSwitcher::GetCurrentScene() const
{
	return CurrentScene.get();
}

std::set<std::string> MSceneSwitcher::CalculateSceneResources() const
{
	using namespace MapElements;
	
	const CMap* const Map = GetCurrentMap();
	const auto& LoadedResources = ResourceStore.GetLoadedResources();
	std::set<std::string> RequiredResources;

	// Tilesets
	const auto TilesetName = Utils::ExtractFilename(Map->Tileset.Name);
	if (LoadedResources.find(TilesetName) == LoadedResources.end())
	{
		RequiredResources.emplace(TilesetName);
	}

	// Sounds
	auto SoundLayers = Map->GetObjectLayersOfType(EObjectLayerType::Sounds);

	for (const auto& SoundLayer : SoundLayers)
	{
		for (const auto& Sound : SoundLayer.Objects)
		{
			if (auto Name = Sound.Properties.GetProperty(AAudioArea::AudioProperties.SOUND_ID))
			{
				Name.emplace(Utils::ExtractFilename(*Name));

				if (LoadedResources.find(*Name) == LoadedResources.end())
				{
					RequiredResources.emplace(*Name);
				}
			}
			else
			{
				throw(Pipanikos("No " + AAudioArea::AudioProperties.SOUND_ID + " property found for " + Sound.Name + " in map " + Map->ID, __FILE__, __LINE__));
			}
		}
	}

	// Fonts
	auto TextLayers = Map->GetObjectLayersOfType(EObjectLayerType::Text);

	for (const auto& TextLayer : TextLayers)
	{
		for (const auto& TextSpawner : TextLayer.Objects)
		{
			if (auto FontName = TextSpawner.Properties.GetProperty(AText::TextProperties.FONT))
			{
				if (auto FontSize = TextSpawner.Properties.GetProperty(AText::TextProperties.SIZE))
				{
					std::string FontKey = *FontName + "_" + *FontSize;

					if (LoadedResources.find(FontKey) == LoadedResources.end())
					{
						RequiredResources.emplace(FontKey);
					}
				}
				else
				{
					throw(Pipanikos("Size property not found for " + TextSpawner.Name + " in map " + Map->ID, __FILE__, __LINE__));
				}
			}
			else
			{
				throw(Pipanikos("Font property not found for " + TextSpawner.Name + " in map " + Map->ID, __FILE__, __LINE__));
			}

			if (auto ID = TextSpawner.Properties.GetProperty(AActor::ActorProperties.A_ID))
			{
				// Text actors don't have an existing texture; it's created on demand.
				// We're adding a prefix so that the LoadSceneResources function knows that this is a text texture,
				// and that it should create it and not actually "load" it from somewhere.
				// The prefix is added only for the LoadSceneResources function and is NOT part of the texture's actual key!
				if (LoadedResources.find(ResourceStore.Keys.RES_PREFIX_TEXT + *ID) == LoadedResources.end())
				{
					RequiredResources.emplace(ResourceStore.Keys.RES_PREFIX_TEXT + *ID);
				}

			}
			else
			{
				throw(Pipanikos("A_ID property not found for " + TextSpawner.Name + " in map " + Map->ID, __FILE__, __LINE__));
			}
		}
	}

	// The map only has the key of the actor, the actual asset paths are on the gamedata file.
	// Parse it and create a list of actor data. Then on the object layer iterations we can look information up
	// based on the data list instead of parsing the file every time.
	struct CActorData
	{
		CActorData(std::string Type_, std::string Path_)
			: Type(Type_), Path(Path_)
		{}

		std::string Type;
		std::string Path;
	};

	std::map<std::string, CActorData> ActorData;

	using namespace rapidxml;

	std::string GameDataRaw = ResourceStore.RequestString(MResourceStore::Keys.GAMEDATA_FILE_KEY);
	std::vector<TCHAR> GameData = std::vector<TCHAR>(GameDataRaw.c_str(), GameDataRaw.c_str() + GameDataRaw.length() + 1);

	{
		xml_document<> XMLDoc;
		XMLDoc.parse<0>(GameData.data());

		auto ActorNode = XMLDoc.first_node("game")->first_node("actors")->first_node("actor");

		while (ActorNode != nullptr)
		{
			std::string Key = ActorNode->first_attribute("key")->value();
			std::string Type = ActorNode->first_attribute("type")->value();
			std::string Path = ResourceStore.Keys.ACTOR_PATH + ActorNode->first_attribute("path")->value();

			ActorData.emplace(Key, CActorData(Type, Path));

			ActorNode = ActorNode->next_sibling();
		}

		// Enemies
		auto CharacterLayers = Map->GetObjectLayersOfType(EObjectLayerType::Characters);

		for (const auto& CharacterLayer : CharacterLayers)
		{
			for (const auto& Spawner : CharacterLayer.Objects)
			{
				if (auto Key = Spawner.Properties.GetProperty(ActorMapProperties::KEY))
				{
					auto ActorAnimKeys = ActorHandler.GetActorAnimKeys(*Key);
					RequiredResources.insert(ActorAnimKeys.begin(), ActorAnimKeys.end());
				}
				else
				{
					throw(Pipanikos("key property not found for " + Spawner.Name + " in map " + Map->ID, __FILE__, __LINE__));
				}
			}
		}

		// Font data
		for (const auto& KVP : Map->GetTextData())
		{
			RequiredResources.emplace(KVP.second);
		}

		// Parallaxes
		for (const auto& ImageLayer : CurrentScene->LevelMap.GetImageLayers())
		{
			RequiredResources.emplace(*ImageLayer.Properties.GetProperty(ActorMapProperties::KEY));
		}
	}

	// Preloads
	{
		std::string PreloadsDataRaw = ResourceStore.RequestString(MResourceStore::Keys.PRELOADS_FILE_KEY);
		std::vector<TCHAR> PreloadsData = std::vector<TCHAR>(PreloadsDataRaw.c_str(), PreloadsDataRaw.c_str() + PreloadsDataRaw.length() + 1);

		xml_document<> XMLDoc;
		XMLDoc.parse<0>(PreloadsData.data());

		auto SceneNode = XMLDoc.first_node("preloads")->first_node("scene");

		while (SceneNode != nullptr)
		{
			std::string CurrSceneName = SceneNode->first_attribute("key")->value();

			if ((CurrSceneName == "ALL") || (CurrSceneName == CurrentScene->GetKey()))
			{
				auto AssetNode = SceneNode->first_node("asset");
				while (AssetNode != nullptr)
				{
					RequiredResources.emplace(AssetNode->first_attribute("key")->value());
					AssetNode = AssetNode->next_sibling();
				}
			}

			SceneNode = SceneNode->next_sibling();
		}
	}

	return RequiredResources;
}

void MSceneSwitcher::PopulateParallaxPaths() const
{
	const auto& ImageLayers = CurrentScene->LevelMap.GetImageLayers();

	for (const auto& ImageLayer : ImageLayers)
	{
		ResourceLoader.AddResourcePath(*ImageLayer.Properties.GetProperty(ActorMapProperties::KEY), ResourceStore.Keys.PARALLAX_PATH + Utils::ExtractFilename(ImageLayer.Path));
	}
}
