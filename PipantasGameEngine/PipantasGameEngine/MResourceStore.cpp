#include "MResourceStore.hpp"
#include "Managers.hpp"
#include "SDL_image.h"
#include "rapidxml.hpp"
#include "AText.hpp"
#include "physfs.h"
#include "Pipanikos.hpp"

#include <memory>
#include <regex>

using RWopsPtr = std::unique_ptr<SDL_RWops, decltype(&SDL_FreeRW)>;
using SurfacePtr = std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)>;
using FontPtr = std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)>;

StringKeys MResourceStore::Keys = StringKeys();

void MResourceStore::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	Logger.Log(ELogLevel::Info, "MResourceStore initialized.");
}

void MResourceStore::Shutdown(const KeyGameCoordinator&)
{
	Logger.Log(ELogLevel::Info, "MResourceStore shutdown.");
}

const std::set<std::string>& MResourceStore::GetLoadedResources() const
{
	return LoadedResources;
}

std::string MResourceStore::RequestString(std::string ID) const
{
	std::string RequestedString = std::string();

	if (Strings.count(ID) == 0)
	{
		throw(Pipanikos("No string value found for " + ID, __FILE__, __LINE__));
	}
	else
	{
		RequestedString = Strings.at(ID);
	}

	return RequestedString;
}

const BShaderProgram& MResourceStore::RequestShaderProgram(std::string ID) const
{
	if (Shaders.count(ID) == 0)
	{
		throw(Pipanikos("No shader program found for " + ID, __FILE__, __LINE__));
	}
	else
	{
		return Shaders.at(ID);
	}
}

const BTexture& MResourceStore::RequestTexture(std::string ID) const
{
	if (Textures.count(ID) == 0)
	{
		Logger.Log(ELogLevel::Warning, "Could not find texture " + ID);
		ID = Keys.DEFAULT_TEXTURE_KEY;
	}

	return Textures.at(ID);
}

BTTFFont& MResourceStore::RequestFont(std::string ID)
{
	if (Fonts.count(ID) == 0)
	{
		ID = Keys.DEFAULT_FONT_KEY;
	}

	return Fonts.at(ID);
}

const BTTFFontData& MResourceStore::RequestFontData(std::string ID) const
{
	auto TextData = SceneSwitcher.GetCurrentMap()->GetTextData().at(ID);
	if (FontData.count(TextData) == 0)
	{
		TextData = Keys.DEFAULT_FONT_DATA_KEY;
	}

	return FontData.at(TextData);
}

BMixChunk& MResourceStore::RequestChunk(std::string ID)
{
	if (Chunks.count(ID) == 0)
	{
		Logger.Log(ELogLevel::Warning, "No chunk found for " + ID);
		return Chunks.at(Keys.DEFAULT_SFX_KEY);
	}
	else
	{
		return Chunks.at(ID);
	}
}

BMixMusic& MResourceStore::RequestMusic(std::string ID)
{
	if (MusicFiles.count(ID) == 0)
	{
		Logger.Log(ELogLevel::Warning, "No music found for " + ID);
		return MusicFiles.at(Keys.DEFAULT_MUSIC_KEY);
	}
	else
	{
		return MusicFiles.at(ID);
	}
}
