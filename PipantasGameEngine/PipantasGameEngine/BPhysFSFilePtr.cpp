#include "BPhysFSFilePtr.hpp"
#include "Managers.hpp"
#include "Pipanikos.hpp"

#include <memory>
#include <functional>
#include <string>

BPhysFSFilePtr::BPhysFSFilePtr()
	: Path(""), File(nullptr, nullptr)
{}

BPhysFSFilePtr::BPhysFSFilePtr(std::string Path_)
	: File(nullptr, nullptr)
{
	Path = Path_;

	auto Deleter = std::bind(&PHYSFS_close, std::placeholders::_1);

	if (Path.empty())
	{
		throw(Pipanikos("Path is empty.", __FILE__, __LINE__));
	}

	auto FileRaw = PHYSFS_openRead(Path.c_str());

	if (FileRaw == nullptr)
	{
		throw(Pipanikos(Path + ": " + PHYSFS_getLastError(), __FILE__, __LINE__));
	}

	File = std::unique_ptr<PHYSFS_File, decltype(&PHYSFS_close)>(FileRaw, PHYSFS_close);
}

std::string BPhysFSFilePtr::GetPath() const
{
	return Path;
}

BPhysFSFilePtr::operator PHYSFS_File *()
{
	return File.get();
}