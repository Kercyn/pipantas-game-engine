#include "MParallaxLogic.hpp"
#include "Managers.hpp"
#include "rapidxml.hpp"

void MParallaxLogic::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	Logger.Log(ELogLevel::Info, "MParallaxLogic initialized.");
}

void MParallaxLogic::Shutdown(const KeyGameCoordinator&)
{
	Logger.Log(ELogLevel::Info, "MParallaxLogic shutdown.");
}

void MParallaxLogic::OnCameraTranslate(const KeyCamera&, glm::vec2 Translation)
{
	auto& CurrentGroup = ParallaxStore.GetCurrentGroup({});

	for (auto& KVP : CurrentGroup)
	{
		auto& Parallax = KVP.second;

		glm::vec2 HalfSize = Parallax.Size / 2.0f;

		switch (Parallax.Type)
		{
			case EParallaxType::Bi_X:
				Parallax.Offset.x -= (Translation * Parallax.Speed).x;
				Parallax.Offset.y -= Translation.y;

				if (std::abs(Parallax.Offset.x) >= HalfSize.x)
				{
					Parallax.Offset.x += (std::signbit(Parallax.Offset.x)) ? Parallax.Size.x : -Parallax.Size.x;
				}

				break;

			case EParallaxType::Bi_Y:
				Parallax.Offset.x -= Translation.x;
				Parallax.Offset.y -= (Translation * Parallax.Speed).y;

				if (std::abs(Parallax.Offset.y) >= HalfSize.y)
				{
					Parallax.Offset.y += (std::signbit(Parallax.Offset.y)) ? Parallax.Size.y : -Parallax.Size.y;
				}
				break;

			case EParallaxType::Multi:
				Parallax.Offset -= (Translation * Parallax.Speed);

				if (std::abs(Parallax.Offset.x) >= HalfSize.x)
				{
					Parallax.Offset.x += (std::signbit(Parallax.Offset.x)) ? Parallax.Size.x : -Parallax.Size.x;
				}

				if (std::abs(Parallax.Offset.y) >= HalfSize.y)
				{
					Parallax.Offset.y += (std::signbit(Parallax.Offset.y)) ? Parallax.Size.y : -Parallax.Size.y;
				}
				break;

			case EParallaxType::Fixed:
				// Do nothing; fixed parallaxes retain their original offsets at all times.
				[[fallthrough]]
				break;
		}
	}
}
