#include "AActor.hpp"
#include "Managers.hpp"
#include "rapidxml.hpp"
#include "PTriggerCollider.hpp"
#include "Utils.hpp"
#include "Pipanikos.hpp"

const std::string ActorMapProperties::A_ID = "A_ID";
const std::string ActorMapProperties::KEY = "Key";
const std::string ActorMapProperties::CONSTRAINTS = "Constraints";
const std::string ActorMapProperties::ENABLED = "Enabled";
const std::string ActorMapProperties::SCALE_X = "ScaleX";
const std::string ActorMapProperties::SCALE_Y = "ScaleY";
const std::string ActorMapProperties::USES_GRAVITY = "UsesGravity";
const std::string ActorMapProperties::MASS = "Mass";

const std::string ActorMapValues::CONSTRAINTS_NONE = "none";
const std::string ActorMapValues::CONSTRAINTS_X = "restrict_x";
const std::string ActorMapValues::CONSTRAINTS_Y = "restrict_y";
const std::string ActorMapValues::CONSTRAINTS_ALL = "restrict_all";

AActor::AActor()
	: Orientation(glm::vec2(1.0f, 1.0f))
{
}

AActor::AActor(const AActor& Other)
{
	ID = Other.ID;
	Key = Other.Key;
	Body = Other.Body->Clone();
	Orientation = Other.Orientation;
	Properties = Other.Properties;
	bIsEnabled = Other.bIsEnabled;

	Colliders.reserve(Other.Colliders.size());

	for (auto& Collider : Other.Colliders)
	{
		Colliders.emplace_back(std::make_unique<PCollider>(*Collider));
	}
}

AActor& AActor::operator=(const AActor& Other)
{
	if (this != &Other)
	{
		ID = Other.ID;
		Key = Other.Key;
		Body = Other.Body->Clone();
		Orientation = Other.Orientation;
		Properties = Other.Properties;
		bIsEnabled = Other.bIsEnabled;

		Colliders.reserve(Other.Colliders.size());

		for (auto& Collider : Other.Colliders)
		{
			Colliders.emplace_back(std::make_unique<PCollider>(*Collider));
		}
	}

	return *this;
}

AActor& AActor::operator=(AActor&& Other)
{
	if (this != &Other)
	{
		ID = Other.ID;
		Key = Other.Key;
		Body = Other.Body->Clone();
		Orientation = Other.Orientation;
		Properties = Other.Properties;
		bIsEnabled = Other.bIsEnabled;

		Colliders.reserve(Other.Colliders.size());

		for (auto& Collider : Other.Colliders)
		{
			Colliders.emplace_back(std::move(Collider));
		}
	}

	return *this;
}

void AActor::Update()
{
	for (auto& Collider : Colliders)
	{
		if (Collider->bNeedsUpdating)
		{
			Collider->Update();
		}
	}
}

void AActor::Translate(const glm::vec2& Offset)
{
	Translate(Offset.x, Offset.y);
}

void AActor::Translate(float OffsetX, float OffsetY)
{
	if (bIsEnabled)
	{
		bool bCollidersNeedUpdate = false;

		if (std::abs(OffsetX) > Utils::EPSILON)
		{
			if ((Rigidbody.Constraints & EMovementConstraints::RestrictX) != EMovementConstraints::RestrictX)
			{
				bCollidersNeedUpdate = true;
				Body->Translate(OffsetX, 0.0f);
			}
		}

		if (std::abs(OffsetY) > Utils::EPSILON)
		{
			if ((Rigidbody.Constraints & EMovementConstraints::RestrictY) != EMovementConstraints::RestrictY)
			{
				bCollidersNeedUpdate = true;
				Body->Translate(0.0f, OffsetY);
			}
		}

		if (bCollidersNeedUpdate)
		{
			for (auto& Collider : Colliders)
			{
				Collider->NeedsUpdate();
			}
		}
	}
}


void AActor::SetPosition(const glm::vec2& NewPosition)
{
	if (bIsEnabled)
	{
		Body->SetPosition(NewPosition);

		for (auto& Collider : Colliders)
		{
			Collider->NeedsUpdate();
		}
	}
}

void AActor::SetPosition(float NewX, float NewY)
{
	if (bIsEnabled)
	{
		Body->SetPosition(NewX, NewY);
	}
}

void AActor::Rotate(float Angle)
{
	if (bIsEnabled)
	{
		switch (Body->GetType())
		{
			case EShapeType::AABB:
				// AABBs don't rotate.
				break;

			case EShapeType::Circle:
			{
				auto Circle = static_cast<SCircle*>(Body.get());
				Circle->Rotate(Angle);

				for (auto& Collider : Colliders)
				{
					Collider->NeedsUpdate();
				}
			}

			case EShapeType::OBB:
			{
				auto OBB = static_cast<SOBB*>(Body.get());
				OBB->Rotate(Angle);

				for (auto& Collider : Colliders)
				{
					Collider->NeedsUpdate();
				}
				break;
			}
		}
	}
}

void AActor::SetRotation(float NewRotation)
{
	if (bIsEnabled)
	{
		switch (Body->GetType())
		{
		case EShapeType::AABB:
			// AABBs don't rotate.
			break;

		case EShapeType::Circle:
		{
			auto Circle = static_cast<SCircle*>(Body.get());
			Circle->SetRotation(NewRotation);

			for (auto& Collider : Colliders)
			{
				Collider->NeedsUpdate();
			}
		}

		case EShapeType::OBB:
		{
			auto OBB = static_cast<SOBB*>(Body.get());
			OBB->SetRotation(NewRotation);

			for (auto& Collider : Colliders)
			{
				Collider->NeedsUpdate();
			}
			break;
		}
		}
	}
}

void AActor::SetScale(const glm::vec2& NewScale)
{
	Body->SetScale(NewScale);
}

void AActor::Scale(float x, float y)
{
	Body->ChangeScale(x, y);
}

void AActor::SetOrientation(const glm::vec2& NewOrientation)
{
	SetOrientation(NewOrientation.x, NewOrientation.y);
}

void AActor::SetOrientation(float OrientX, float OrientY)
{
	if (bIsEnabled)
	{
		Orientation.x = OrientX;
		Orientation.y = OrientY;

		for (auto& Collider : Colliders)
		{
			Collider->NeedsUpdate();
		}
	}
}

void AActor::SetProperty(std::string PropKey, std::string Value)
{
	Properties[PropKey] = Value;
}

void AActor::RemoveProperty(std::string PropKey)
{
	Properties.erase(PropKey);
}

std::string AActor::GetID() const
{
	return ID;
}

std::string AActor::GetKey() const
{
	return Key;
}

EActorType AActor::GetActorType() const
{
	return ActorType;
}

const std::vector<std::unique_ptr<PCollider>>& AActor::GetColliders() const
{
	return Colliders;
}

std::vector<std::unique_ptr<PCollider>>& AActor::GetColliders(KeyActorHandler)
{
	return Colliders;
}

bool AActor::IsEnabled() const
{
	return bIsEnabled;
}

glm::vec2 AActor::GetOrientation() const
{
	return Orientation;
}

PCollider const* const AActor::GetCollider(std::string Name) const
{
	for (const auto& Collider : Colliders)
	{
		if (Collider->GetName() == Name)
		{
			return Collider.get();
		}
	}

	return nullptr;
}

const PRigidbody& AActor::GetRigidbody() const
{
	return Rigidbody;
}

const SShape* const AActor::GetBody() const
{
	return Body.get();
}

std::string AActor::GetProperty(std::string PropKey) const
{
	if (Properties.count(PropKey) == 1)
	{
		return Properties.at(PropKey);
	}
	else
	{
		std::string Error = "Property \"" + PropKey + "\" not found." +
			"\n\tActor Key: " + Key +
			"\n\tActor ID: " + ID;

		Logger.Log(ELogLevel::Warning, Error);
		
		return std::string();
	}
}

const std::map<std::string, std::string>& AActor::GetProperties() const
{
	return Properties;
}

TextureCoords AActor::GetTexCoords() const
{
	return TextureCoords(glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f));
}

BTexture AActor::GetCurrentTexture() const
{
	return ResourceStore.RequestTexture(ID);
}

void AActor::SetID(KeyActorHandler, std::string ID_)
{
	ID = ID_;
}

void AActor::SetKey(KeyActorHandler, std::string Key_)
{
	Key = Key_;
}

void AActor::SetActorType(KeyActorHandler, EActorType ActorType_)
{
	ActorType = ActorType_;
}

void AActor::SetBody(KeyActorHandler, std::unique_ptr<SShape> Body_)
{
	Body = std::move(Body_);
}

void AActor::Enable()
{
	bIsEnabled = true;
}

void AActor::Disable()
{
	bIsEnabled = false;
}

void AActor::CopyProperties(const MapElements::Object& SpawnerObj)
{
	for (const auto& PropertyPair : SpawnerObj.Properties.GetAllProperties())
	{
		SetProperty(PropertyPair.first, PropertyPair.second);
	}
}

void AActor::LoadProperties(std::string Key)
{
	using namespace rapidxml;

	std::string ActorPropertiesRaw = ResourceStore.RequestString(Key);
	std::vector<TCHAR> ActorProperties = std::vector<TCHAR>(ActorPropertiesRaw.c_str(), ActorPropertiesRaw.c_str() + ActorPropertiesRaw.length() + 1);

	xml_document<> PropertiesXML;
	PropertiesXML.parse<0>(ActorProperties.data());

	auto ActorNode = PropertiesXML.first_node("actor");

	if (auto PropertiesNode = ActorNode->first_node("properties"))
	{
		auto PropertyNode = PropertiesNode->first_node("property");

		while (PropertyNode != nullptr)
		{
			std::string Key = PropertyNode->first_attribute("key")->value();
			std::string Value = PropertyNode->first_attribute("value")->value();

			Properties.emplace(Key, Value);
			PropertyNode = PropertyNode->next_sibling();
		}
	}

	if (auto EnabledAttr = ActorNode->first_attribute(AActor::ActorProperties.ENABLED.c_str()))
	{
		bIsEnabled = Utils::ToBool(EnabledAttr->value());
	}
	else
	{
		bIsEnabled = true;
	}

	if (auto MassAttr = ActorNode->first_attribute(AActor::ActorProperties.MASS.c_str()))
	{
		Rigidbody.Mass = std::stof(MassAttr->value());
	}
	else
	{
		Rigidbody.Mass = 1.0f;
	}

	if (auto UsesGravityAttr = ActorNode->first_attribute(AActor::ActorProperties.USES_GRAVITY.c_str()))
	{
		Rigidbody.bUsesGravity = Utils::ToBool(UsesGravityAttr->value());
	}
	else
	{
		Rigidbody.bUsesGravity = true;
	}

	if (auto ConstraintsAttr = ActorNode->first_attribute(AActor::ActorProperties.CONSTRAINTS.c_str()))
	{
		std::string ConstraintsAttrValue = ConstraintsAttr->value();

		if (ConstraintsAttrValue == AActor::ActorValues.CONSTRAINTS_NONE)
		{
			Rigidbody.Constraints = EMovementConstraints::None;
		}
		else if (ConstraintsAttrValue == AActor::ActorValues.CONSTRAINTS_X)
		{
			Rigidbody.Constraints = EMovementConstraints::RestrictX;
		}
		else if (ConstraintsAttrValue == AActor::ActorValues.CONSTRAINTS_Y)
		{
			Rigidbody.Constraints = EMovementConstraints::RestrictY;
		}
		else if (ConstraintsAttrValue == AActor::ActorValues.CONSTRAINTS_ALL)
		{
			Rigidbody.Constraints = EMovementConstraints::RestrictAll;
		}
		else
		{
			Logger.Log(ELogLevel::Warning, ConstraintsAttrValue + " is not a valid constraint. Defaulting to None.");
			Rigidbody.Constraints = EMovementConstraints::None;
		}
	}
	else
	{
		Rigidbody.Constraints = EMovementConstraints::None;
	}

	// Actor body
	auto BodyNode = ActorNode->first_node("body");
	if (BodyNode != nullptr)
	{
		if (auto ShapeAttr = BodyNode->first_attribute("shape"))
		{
			std::string ShapeType = std::string(ShapeAttr->value());
			
			if (ShapeType == "box")
			{
				Body = std::make_unique<SOBB>();
				auto OBB = static_cast<SOBB*>(Body.get());
				float Height;
				float Width;

				if (auto HeightAttr = BodyNode->first_attribute("height"))
				{
					Height = std::stof(HeightAttr->value());
				}
				else
				{
					Logger.Log(ELogLevel::Warning, "No height attribute found for actor " + Key + ". Defaulting to 0.");
					Height = 0.0f;
				}

				if (auto WidthAttr = BodyNode->first_attribute("width"))
				{
					Width = std::stof(WidthAttr->value());
				}
				else
				{
					Logger.Log(ELogLevel::Warning, "No width attribute found for actor " + Key + ". Defaulting to 0.");
					Width = 0.0f;
				}

				OBB->SetSize(Height, Width);
			}
			else if (ShapeType == "circle")
			{
				Body = std::make_unique<SCircle>();
				auto Circle = static_cast<SCircle*>(Body.get());

				if (auto RadiusAttr = BodyNode->first_attribute("radius"))
				{
					Circle->SetRadius(std::stof(RadiusAttr->value()));
				}
				else
				{
					throw(Pipanikos("No radius attribute found for actor " + Key, __FILE__, __LINE__));
				}
			}
			else
			{
				throw(Pipanikos("Invalid shape attribute found for actor " + Key + ": " + ShapeType, __FILE__, __LINE__));
			}
		}
		else
		{
			throw(Pipanikos("No shape attribute found for actor " + Key, __FILE__, __LINE__));
		}
		glm::vec2 NewScale = glm::vec2(1.0f, 1.0f);
		if (auto ScaleNode = BodyNode->first_node("scale"))
		{
			NewScale.x = std::stof(ScaleNode->first_attribute("x")->value());
			NewScale.y = std::stof(ScaleNode->first_attribute("y")->value());
		}
		Body->SetScale(NewScale);
		Body->SetPosition(glm::vec2());
	}
	else
	{
		throw(Pipanikos("No body node found for actor " + Key, __FILE__, __LINE__));
	}

	// Parse and attach colliders
	std::string GameDataRaw = ResourceStore.RequestString(ResourceStore.Keys.GAMEDATA_FILE_KEY);
	std::vector<TCHAR> GameData = std::vector<TCHAR>(GameDataRaw.c_str(), GameDataRaw.c_str() + GameDataRaw.length() + 1);

	xml_document<> GamedataXML;
	GamedataXML.parse<0>(GameData.data());

	auto ActorColliderNode = ActorNode->first_node("colliders")->first_node("collider");

	// Parse all colliders in the properties file, find them in the Colliders file and attach a new appropriate collider to the actor.
	while (ActorColliderNode != nullptr)
	{
		std::string ActorColliderKey = ActorColliderNode->first_attribute("key")->value();

		auto GamedataColliderNode = GamedataXML.first_node("game")->first_node("physics")->first_node("colliders")->first_node("collider");

		// Find the appropriate collider
		while (GamedataColliderNode->first_attribute("key")->value() != ActorColliderKey)
		{
			GamedataColliderNode = GamedataColliderNode->next_sibling();
		}

		// Found the collider, generate the actual collider object with its info.
		std::unique_ptr<PCollider> Collider;

		auto TriggerNode = GamedataColliderNode->first_attribute("is_trigger");
		if (TriggerNode != nullptr)
		{
			if (Utils::ToBool(TriggerNode->value()))
			{
				Collider = std::make_unique<PTriggerCollider>();
			}
			else
			{
				Collider = std::make_unique<PCollider>();
			}

			Collider->TriggerType = (Utils::ToBool(TriggerNode->value())) ? ETriggerType::Programmable : ETriggerType::None;
		}
		else
		{
			Collider = std::make_unique<PCollider>();
		}

		Collider->ID = CollisionEngine.GenerateColliderID();
		Collider->AttachedActor = this;
		Collider->bEnabledSelf = Utils::ToBool(ActorColliderNode->first_attribute("enabled")->value());
		
		auto StaticNode = GamedataColliderNode->first_attribute("is_static");
		Collider->bIsStatic = (StaticNode != nullptr) ? Utils::ToBool(StaticNode->value()) : false;

		Collider->Name = ActorColliderNode->first_attribute("name")->value();
		Collider->Layer = CollisionEngine.GetLayer(ActorColliderNode->first_attribute("layer_key")->value());

		auto ActorPos = Body->GetPosition();

		std::string ShapeType = GamedataColliderNode->first_attribute("shape")->value();
		
		if (ShapeType == "obb")
		{
			Collider->Shape = std::make_unique<SOBB>();

			auto OBB = static_cast<SOBB*>(Collider->Shape.get());

			OBB->SetPosition(ActorPos);
				
			float Height = std::stof(GamedataColliderNode->first_attribute("height")->value());
			float Width = std::stof(GamedataColliderNode->first_attribute("width")->value());
			OBB->SetSize(Height, Width);

			float Rotation = std::stof(ActorColliderNode->first_attribute("rotation")->value());
			OBB->SetRotation(Rotation);

			SAABB Hull;
			Hull.SetBoundaries(glm::vec2(-(Width / 2.0f), -(Height / 2.0f)), glm::vec2(Width / 2.0f, Height / 2.0f));
			Collider->ConvexHull = Hull;
		}
		else if (ShapeType == "circle")
		{
			Collider->Shape = std::make_unique<SCircle>();

			SCircle* Circle = static_cast<SCircle*>(Collider->Shape.get());

			Circle->SetPosition(ActorPos);
			Circle->SetRadius(std::stof(GamedataColliderNode->first_attribute("radius")->value()));
			Circle->SetRotation(std::stof(ActorColliderNode->first_attribute("rotation")->value()));

			SAABB Hull;
			float Radius = Circle->GetRadius();
			Hull.SetBoundaries(glm::vec2(-Radius, -Radius), glm::vec2(Radius, Radius));
			Collider->ConvexHull = Hull;
		}
		else
		{
			throw(Pipanikos(ShapeType + " is not a proper shape.", __FILE__, __LINE__));
		}

		Collider->Offset.x = std::stof(ActorColliderNode->first_attribute("offset_x")->value());
		Collider->Offset.y = std::stof(ActorColliderNode->first_attribute("offset_y")->value());
		Collider->bNeedsUpdating = true;

		Colliders.emplace_back(std::move(Collider));

		// Hulls will be generated after the actor's Colliders vector has been populated.
		ActorColliderNode = ActorColliderNode->next_sibling();
	}

	// Register actor colliders to the physics manager
	for (const auto& Collider : Colliders)
	{
		CollisionEngine.RegisterCollider(Collider.get(), Collider->bIsStatic);
	}
}

void AActor::OverridePropFileAttributes(KeyActorHandler, const MapElements::Object& SpawnerObj)
{
	const glm::vec2& Scale = Body->GetScale();

	if (auto EnabledProp = SpawnerObj.Properties.GetProperty(AActor::ActorProperties.ENABLED))
	{
		bIsEnabled = Utils::ToBool(*EnabledProp);
	}

	if (auto ScaleXProp = SpawnerObj.Properties.GetProperty(AActor::ActorProperties.SCALE_X))
	{
		Body->SetScale(std::stof(*ScaleXProp), Scale.y);
	}

	if (auto ScaleYProp = SpawnerObj.Properties.GetProperty(AActor::ActorProperties.SCALE_Y))
	{
		Body->SetScale(Scale.x, std::stof(*ScaleYProp));
	}

	if (auto MassProp = SpawnerObj.Properties.GetProperty(AActor::ActorProperties.MASS))
	{
		Rigidbody.Mass = std::stof(*MassProp);
	}

	if (auto ConstraintsProp = SpawnerObj.Properties.GetProperty(AActor::ActorProperties.CONSTRAINTS))
	{
		if (*ConstraintsProp == AActor::ActorValues.CONSTRAINTS_NONE)
		{
			Rigidbody.Constraints = EMovementConstraints::None;
		}
		else if (*ConstraintsProp == AActor::ActorValues.CONSTRAINTS_X)
		{
			Rigidbody.Constraints = EMovementConstraints::RestrictX;
		}
		else if (*ConstraintsProp == AActor::ActorValues.CONSTRAINTS_Y)
		{
			Rigidbody.Constraints = EMovementConstraints::RestrictY;
		}
		else if (*ConstraintsProp == AActor::ActorValues.CONSTRAINTS_ALL)
		{
			Rigidbody.Constraints = EMovementConstraints::RestrictAll;
		}
		else
		{
			Logger.Log(ELogLevel::Warning, *ConstraintsProp + " is not a valid constraint. Defaulting to None.");
			Rigidbody.Constraints = EMovementConstraints::None;
		}
	}

	if (auto UsesGravityProp = SpawnerObj.Properties.GetProperty(AActor::ActorProperties.USES_GRAVITY))
	{
		Rigidbody.bUsesGravity = Utils::ToBool(*UsesGravityProp);
	}
}

void AActor::UpdateInformation(KeyActorHandler, const MapElements::Object& SpawnerObj)
{
	const CMap* const CurrentMap = SceneSwitcher.GetCurrentMap();

	auto A_ID = SpawnerObj.Properties.GetProperty(AActor::ActorProperties.A_ID);
	if (A_ID)
	{
		ID = *A_ID;
	}
	else
	{
		throw(Pipanikos("No A_ID attribute found for " + SpawnerObj.Name, __FILE__, __LINE__));
	}

	auto KeyProp = SpawnerObj.Properties.GetProperty(AActor::ActorProperties.KEY);
	if (KeyProp)
	{
		Key = *KeyProp;
	}
	else
	{
		throw(Pipanikos("No key attribute found for " + SpawnerObj.Name, __FILE__, __LINE__));
	}

	// Need to invert the height because OpenGL
	Body->Translate(SpawnerObj.Position.x, (CurrentMap->GetHeight() * CurrentMap->GetTileSize().y) - SpawnerObj.Position.y);

	CopyProperties(SpawnerObj);
}
