#pragma once

/*! \file */

#include <string>
#include <memory>
#include "physfs.h"

class BPhysFSFilePtr final
{
	public:
		BPhysFSFilePtr();
		BPhysFSFilePtr(std::string Path_);
		~BPhysFSFilePtr() = default;

		std::string GetPath() const;

		operator PHYSFS_File *();

	private:
		std::unique_ptr<PHYSFS_File, decltype(&PHYSFS_close)> File;

		std::string Path;
};
