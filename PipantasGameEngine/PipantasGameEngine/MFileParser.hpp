#pragma once

/*! \file */

#include <string>
#include <vector>
#include <fstream>
#include <utility>
#include <boost/optional.hpp>

#include "CParserValue.hpp"
#include "IManager.hpp"


/*! \brief All possible types of data in a text file. */
enum class EParseType
{
	Section = 0,
	Key,
	Comment,
	Value,
	EndOfFile,
	Invalid
};

/*! \brief A parser that reads information from a Pipantas file.

The Pipantas file must be structured properly, as described [here](file:///Pipantas - Standard text file format.pdf).
No more than one file may be open for parsing at any time.
*/

class MFileParser final : public IManager
{
	public:
		MFileParser();
		~MFileParser() = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;

		/*! Opens a file for parsing.

		If a file is already open, it will close first so that the new file can be opened. This function
		must be called before attempting to parse a file.
		\param FilePath Relative path of the file you wish to open.
		*/
		void Open(std::string FilePath);

		/*! Gets the value associated with a key.

		\param Key The key of the value of interest.
		\returns The value associated with Key. Automatically attempts to convert the parsed value to the assigned type.
		*/
		CParserValue GetValue(std::string Key);

		//! Returns the next key-value pair in the opened file.
		//boost::optional<std::pair<std::string, std::string>> GetKVP();

	private:
		std::ifstream File;
};
