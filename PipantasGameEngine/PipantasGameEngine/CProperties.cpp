#include "CProperties.hpp"

void CProperties::AddProperty(std::string Key, std::string Value)
{
	Properties.emplace(Key, Value);
}

boost::optional<std::string> CProperties::GetProperty(std::string Key) const
{
	boost::optional<std::string> RetKey;

	if (Properties.count(Key) != 0)
	{
		RetKey = Properties.at(Key);
	}

	return RetKey;
}

const std::map<std::string, std::string>& CProperties::GetAllProperties() const
{
	return Properties;
}
