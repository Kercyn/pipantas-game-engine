#include "SIText.hpp"
#include "Managers.hpp"
#include "Pipanikos.hpp"

SIText::SIText(std::string Key)
{
	// No null checks needed; Actor Manager will throw an error if the actor doesn't exist.
	Text = static_cast<AText*>(ActorHandler.GetActor(Key));
}

std::tuple<LUA_NUMBER, LUA_NUMBER> SIText::GetPosition() const
{
	const auto& ActorPos = Text->GetBody()->GetPosition();

	return std::make_tuple(ActorPos.x, ActorPos.y);
}

std::string SIText::GetKey() const
{
	return Text->GetKey();
}

std::string SIText::GetID() const
{
	return Text->GetID();
}

std::string SIText::GetProperty(std::string PropKey) const
{
	return Text->GetProperty(PropKey);
}

bool SIText::IsEnabled() const
{
	return Text->IsEnabled();
}

LUA_INTEGER SIText::GetColliderID(std::string Name) const
{
	if (auto Collider = Text->GetCollider(Name))
	{
		return static_cast<LUA_INTEGER>(Collider->GetID());
	}
	else
	{
		throw(Pipanikos("Could not find collider " + Name, __FILE__, __LINE__));
	}
}

void SIText::Translate(LUA_NUMBER x, LUA_NUMBER y)
{
	float dt = GameCoordinator.GetDeltaTime();
	Text->Translate(x * dt, y * dt);
}

void SIText::Rotate(LUA_NUMBER Angle)
{
	float dt = GameCoordinator.GetDeltaTime();
	Text->Rotate(Angle * dt);
}

void SIText::SetPosition(LUA_NUMBER NewX, LUA_NUMBER NewY)
{
	Text->SetPosition(NewX, NewY);
}

void SIText::SetRotation(LUA_NUMBER NewRotation)
{
	Text->SetRotation(NewRotation);
}

void SIText::SetOrientation(LUA_NUMBER OrientX, LUA_NUMBER OrientY)
{
	Text->SetOrientation(OrientX, OrientY);
}

void SIText::SetProperty(std::string PropKey, std::string Value)
{
	Text->SetProperty(PropKey, Value);
}

void SIText::SetText(std::string NewText)
{
	Text->SetText(NewText);
}

void SIText::Enable()
{
	Text->Enable();
}

void SIText::Disable()
{
	Text->Disable();
}

