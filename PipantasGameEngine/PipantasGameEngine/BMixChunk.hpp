#pragma once

#include <memory>
#include "SDL_mixer.h"

class MResourceLoader;

class BMixChunk final
{
	public:
		using ChunkPtr = std::unique_ptr<Mix_Chunk, decltype(&Mix_FreeChunk)>;

		BMixChunk();
		BMixChunk(ChunkPtr Chunk_);
		BMixChunk(BMixChunk&& Other);
		BMixChunk(const BMixChunk& Other) = delete;
		~BMixChunk() = default;

		BMixChunk& operator=(const BMixChunk& Other) = delete;
		BMixChunk& operator=(BMixChunk&& Other);

		operator Mix_Chunk *();

		friend MResourceLoader;

	private:
		ChunkPtr Chunk;
};
