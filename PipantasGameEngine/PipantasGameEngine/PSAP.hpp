#pragma once

/*! \file */

#include <map>
#include <cstdint>
#include <functional>
#include <vector>
#include <set>

#include "PCollider.hpp"
#include "PEndPoint.hpp"
#include "SAABB.hpp"
#include "Utils.hpp"

class MPairGenerator;

class PSAP final
{
	public:
		~PSAP() = default;

		const SAABB& GetBoundaries() const;
		const std::map<std::uint16_t, PCollider*>& GetColliders() const;

		friend MPairGenerator;

	private:
		PSAP();

		//! Reference to the collision engine's colliders.
		const std::map<std::uint16_t, PCollider*>& AllColliders;

		//! Contains the colliders inside the SAP.
		std::map<std::uint16_t, PCollider*> SAPColliders;

		//! SAP area boundaries
		SAABB Boundaries;

		//! Collider AABB hull endpoints on the x axis.
		std::set<PEndPoint, bool(*)(const PEndPoint&, const PEndPoint&)> Endpoints;

		//! ID of current colliders with a min endpoint found on the X axis.
		std::set<std::uint16_t> ActiveIntervals;

		/*! \brief Adds the endpoints of the collider's convex hull that are inside the SAP to the appropriate endpoint vector.
		This function assumes that the collider actually collides with the SAP!
		*/
		void UpdateColliderHullEndpoints(const PCollider* const Collider);

		//! Traverses the scene's colliders and updates the Endpoints set with the endpoints of static colliders inside the SAP. 
		void UpdateStaticColliderEndpoints();

		//! Traverses the scene's colliders and updates the Endpoints set with the endpoints of dynamic colliders inside the SAP. 
		void UpdateDynamicColliderEndpoints();

		//! Generates and returns all colliding hull pairs.
		std::vector<std::pair<std::uint16_t, std::uint16_t>> GeneratePairs();
};
