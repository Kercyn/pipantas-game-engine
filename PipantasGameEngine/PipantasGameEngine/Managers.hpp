#pragma once

/*! \file */

#include "MGameCoordinator.hpp"
#include "MLogger.hpp"
#include "MPhysFS.hpp"
#include "MResourceStore.hpp"
#include "MResourceLoader.hpp"
#include "MActorHandler.hpp"
#include "MSceneSwitcher.hpp"
#include "MRenderer.hpp"
#include "MFileParser.hpp"
#include "MInputRegistrar.hpp"
#include "MCollisionEngine.hpp"
#include "MPairGenerator.hpp"
#include "MScriptMiddleware.hpp"
#include "MAudioController.hpp"
#include "MAudioPlayback.hpp"
#include "MParallaxStore.hpp"
#include "MParallaxLogic.hpp"

#ifdef LoadString
	#undef LoadString
#endif

extern MGameCoordinator GameCoordinator;
extern MLogger Logger;
extern MPhysFS PhysFS;
extern MResourceStore ResourceStore;
extern MResourceLoader ResourceLoader;
extern MActorHandler ActorHandler;
extern MSceneSwitcher SceneSwitcher;
extern MRenderer Renderer;
extern MFileParser FileParser;
extern MInputRegistrar InputRegistrar;
extern MCollisionEngine CollisionEngine;
extern MPairGenerator PairGenerator;
extern MScriptMiddleware ScriptMiddleware;
extern MAudioController AudioController;
extern MAudioPlayback AudioPlayback;
extern MParallaxStore ParallaxStore;
extern MParallaxLogic ParallaxLogic;
