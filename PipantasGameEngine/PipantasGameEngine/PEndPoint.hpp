#pragma once

/*! \file */

#include <cstdint>
 
struct PEndPoint final
{
	public:
		PEndPoint();
		PEndPoint(std::uint16_t ColliderID_, float Value_, bool bIsMax_);
		~PEndPoint() = default;

		bool operator<(const PEndPoint& rhs) const;
		bool operator>(const PEndPoint& rhs) const;
		bool operator==(const PEndPoint& rhs) const;

		//! Owning collider ID.
		std::uint16_t ColliderID;

		//! The axis value of the endpoint.
		float Value;

		//! Min - Max flag. If true, the endpoint is max.
		bool bIsMax;		
};
