#pragma once

#include <cstdint>

enum class EMovementConstraints : std::uint8_t
{
	None = 0x0,
	RestrictX = 0x1,
	RestrictY = 0x2,
	RestrictAll = 0xf
};

inline EMovementConstraints operator&(EMovementConstraints a, EMovementConstraints b)
{
	return static_cast<EMovementConstraints>(static_cast<std::uint8_t>(a) & static_cast<std::uint8_t>(b));
}

class PRigidbody final
{
	public:
		PRigidbody();
		~PRigidbody() = default;

		float Mass;
		bool bUsesGravity;
		EMovementConstraints Constraints;
};
