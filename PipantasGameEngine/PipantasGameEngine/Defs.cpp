#include "Defs.hpp"

namespace Pipantas
{
	namespace Defs
	{
		std::string GAME_TITLE;
		std::string GAME_VERSION;

		const std::string FILE_BINDINGS = "keybinds.txt";
		const std::string FILE_SETTINGS = "settings.txt";
		const std::string FILE_RESOURCES = "resources.zip";
		const std::string FILE_STDOUT = "stdout.txt";
		const std::string FILE_LOG = "Pipantas.log";
		const std::string INVALID_KEY = "<PIPANTAS INVALID KEY>";

		// Settings keys
		const std::string SCREEN_WIDTH = "screenX";
		const std::string SCREEN_HEIGHT = "screenY";
		const std::string SCREEN_BPP = "bpp";
		const std::string SCREEN_FULLSCREEN = "fullscreen";
	}
}
