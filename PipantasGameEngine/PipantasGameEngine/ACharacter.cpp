#include "ACharacter.hpp"
#include "rapidxml.hpp"
#include "Managers.hpp"
#include "SOBB.hpp"
#include "SCircle.hpp"

void ACharacter::LoadProperties(std::string Key)
{
	using namespace rapidxml;
	
	// Yes, I'll be parsing the same file twice...
	AActor::LoadProperties(Key);

	std::string ActorPropertiesRaw = ResourceStore.RequestString(Key);
	std::vector<TCHAR> ActorProperties = std::vector<TCHAR>(ActorPropertiesRaw.c_str(), ActorPropertiesRaw.c_str() + ActorPropertiesRaw.length() + 1);

	xml_document<> PropertiesXML;
	PropertiesXML.parse<0>(ActorProperties.data());

	auto AnimationNode = PropertiesXML.first_node("actor")->first_node("animations")->first_node("animation");

	while (AnimationNode != nullptr)
	{
		CAnimation TempAnim;

		TempAnim.Key = AnimationNode->first_attribute("key")->value();
		TempAnim.CharacterID = ID;
		TempAnim.TimePerFrame = 1.0f / std::stoi(AnimationNode->first_attribute("fps")->value());
		TempAnim.TotalFrames = std::stoi(AnimationNode->first_attribute("frames")->value());
		TempAnim.CurrentFrame = 0;
		TempAnim.NormFrameWidth = 1.0f / TempAnim.TotalFrames;
		TempAnim.TimeOnThisFrame = 0.0f;

		Animations.emplace(TempAnim.Key, TempAnim);

		AnimationNode = AnimationNode->next_sibling();
	}

	if (auto AnimTriggersNode = PropertiesXML.first_node("actor")->first_node("anim_triggers"))
	{
		auto AnimTriggerNode = AnimTriggersNode->first_node("anim_trigger");

		while (AnimTriggerNode != nullptr)
		{
			std::string Key = AnimTriggerNode->first_attribute("anim_key")->value();
			std::uint8_t Frame = static_cast<std::uint8_t>(std::stoi(AnimTriggerNode->first_attribute("frame")->value()));
			std::string Function = AnimTriggerNode->first_attribute("function")->value();

			Animations.at(Key).Triggers.emplace(Frame, Function);
			AnimTriggerNode = AnimTriggerNode->next_sibling();
		}
	}
}

TextureCoords ACharacter::GetTexCoords() const
{
	glm::vec2 Start;
	glm::vec2 End;
	
	Start.x = CurrentAnim.CurrentFrame * CurrentAnim.NormFrameWidth;
	Start.y = 0.0f;

	End.x = Start.x + CurrentAnim.NormFrameWidth;
	End.y = 1.0f;

	return TextureCoords(Start, End);
}

BTexture ACharacter::GetCurrentTexture() const
{
	return ResourceStore.RequestTexture(Key + "_" + CurrentAnim.GetKey());
}

CAnimation& ACharacter::GetCurrentAnim()
{
	return CurrentAnim;
}

void ACharacter::ChangeAnimation(std::string Key, int Loop, bool bPlayInReverse)
{
	if (bIsEnabled)
	{
		if (CurrentAnim.Key != Key)
		{
			// Check if the animation exists
			if (Animations.count(Key) > 0)
			{
				CurrentAnim = Animations.at(Key);
				CurrentAnim.TimeOnThisFrame = 0.0f;
				CurrentAnim.Loop = Loop;
				CurrentAnim.TimesLooped = 0;
				CurrentAnim.bPlayInReverse = bPlayInReverse;

				CurrentAnim.CurrentFrame = (bPlayInReverse) ? CurrentAnim.TotalFrames - 1 : 0;
			}
		}
	}
}
