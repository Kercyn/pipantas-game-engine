#pragma once

/*! \file */

#include "luaconf.h"
#include "CParallax.hpp"

#include <string>

class SIParallax final
{
	public:
		SIParallax() = delete;
		SIParallax(std::string Key);
		~SIParallax() = default;

		void SetSpeed(LUA_NUMBER NewSpeed) const;
		void SetEnabled(bool Flag) const;
		void SetRenderFront(bool Flag) const;

		LUA_NUMBER GetSpeed() const;
		bool IsEnabled() const;
		bool IsRenderedFront() const;

	private:
		CParallax& Parallax;
};
