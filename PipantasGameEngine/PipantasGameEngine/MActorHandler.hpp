#pragma once

#include <vector>
#include <string>

#include "IManager.hpp"
#include "AActor.hpp"
#include "ACharacter.hpp"
#include "MapElements.hpp"
#include "TIDGenerator.hpp"
#include "AccessKeys.hpp"

class MActorHandler final : public IManager
{
	public:
		MActorHandler() = default;
		~MActorHandler() override = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;
		
		//! Resets scene-specific containers so that they're ready for a new scene.
		void Reset();

		void UpdateActorBodies();

		//! Parses the actor properties file and returns the actor's animation keys.
		std::set<std::string> GetActorAnimKeys(std::string Key) const;

		//! Updates the actor's information based on the spawner object's properties.
		void UpdateActorInformation(const KeySceneSwitcher&, const MapElements::Object& SpawnerObj);
		void GenerateAudioAreaColliders(const KeySceneSwitcher&, const MapElements::ObjectLayer& Layer);

		void AddActor(const KeySceneSwitcher&, const MapElements::Object& Spawner, EActorType Type);

		ACharacter* GetPlayer();
		AActor* GetActor(std::string Key);
		std::vector<AActor*> GetAllActors();

	private:
		TIDGenerator<2000> IDGenerator;

		// We will always have one (and only one) actor of type Player.
		// This points to the player actor stored in the Actors map and is cast to APlayer in Initialize()
		ACharacter* Player;
		std::map<std::string, std::unique_ptr<AActor>> Actors;
};
