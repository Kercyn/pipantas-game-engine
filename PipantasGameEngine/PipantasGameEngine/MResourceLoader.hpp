#pragma once

#include <string>
#include <map>
#include <set>

#include "IManager.hpp"
#include "BTexture.hpp"
#include "SDL.h"

#ifdef LoadString
	#undef LoadString
#endif

class MResourceLoader final : public IManager
{
	public:
		MResourceLoader() = default;
		~MResourceLoader() = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;
		
		/*! \brief Loads the resources specified in \p RequiredResources.
		Only loads resources that are not already loaded.
		*/
		void LoadSceneResources(const std::set<std::string>& RequiredResources) const;

		//! Unloads any resources not contained in the RequiredResources vector.
		void UnloadUnusedResources(const std::set<std::string>& RequiredResources) const;

		//! \brief Reads a file from the resources folder and returns its contents.
		std::string GetFileContents(std::string Path) const;
		
		//! Returns the requested map's data as text. Parsing, if needed, must be done by the caller. Throws an error if it fails.
		std::string GetRawMap(std::string ID) const;

		void AddResourcePath(std::string Key, std::string Path);

		//! Creates a texture from the SDL_Surface and stores it using the ID supplied.
		void CreateTextureFromSurface(std::string ID, SDL_Surface* Surface) const;

		void LoadDefaultResources(const KeyGameCoordinator&) const;
		void LoadShaders(const KeyGameCoordinator&) const;

	private:
		//! Associates asset IDs to their paths.
		std::map<std::string, std::string> ResourcePaths;

		void UnloadMusic(std::string ID) const;
		void UnloadChunk(std::string ID) const;
		void UnloadTexture(std::string ID) const;
		void UnloadFont(std::string ID) const;

		void PopulateResourcePaths();

		/*! \brief Loads a texture to memory.
		Initially, an SDL surface is created from the desired image and then the surface
		is transformed into a GL texture. Finally, the texture is stored in the Textures map.
		The dimensions of the texture are stored in the TextureDimensions map.
		\param ID The key of the texture we want to load.
		*/
		void LoadTexture(std::string ID) const;

		void LoadString(std::string ID, std::string Path) const;

		void LoadFont(std::string ID) const;

		void LoadFontData(std::string ID) const;

		void LoadChunk(std::string ID) const;

		void LoadMusic(std::string ID) const;
};
