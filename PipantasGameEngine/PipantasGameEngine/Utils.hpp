#pragma once

/*! \file */
#pragma warning (disable:4996)

#include "SDL.h"
#include "zlib.h"
#include "SDL_stdinc.h"
#include "PEndPoint.hpp"

#include <string>
#include <sstream>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <vector>
#include <iostream>
#include <cstdint>
#include <limits>
#include <tchar.h>

/*!	\brief Contains utility functions used throughout the application.
*/
namespace Utils
{
	//! Machine epsilon
	constexpr float EPSILON = std::numeric_limits<float>().epsilon();
	constexpr double PI = 3.14159265358979323846264338327950288;

	bool EndpointComparator(const PEndPoint& A, const PEndPoint& B);

	/*! \brief Extracts the file name, including the extension, from a full path.
	\param Path The full path
	\return The extracted filename
	*/
	std::string ExtractFilename(std::string Path);

	/*! \brief Transforms a string value into boolean.
	\param s String to be transformed
	\return true if lowercase \c s equals to "true", false otherwise
	*/
	bool ToBool(std::string Str);
	bool ToBool(char* CStr);
	bool ToBool(int Value);
	bool ToBool(float Value);

	/*! \brief Transforms T value into boolean.
	\param Value T value
	\return true if \c Value is not equal to 0, false otherwise
	*/
	template<class T> bool ToBool(T Value) { return (Value != 0) ? true : false; }

	/*!	\brief Strips a string of newlines.
	\param str String to be stripped
	\return \c str without newlines
	*/
	std::string StripCRLF(const std::string str);

	/*!	\brief Trims a string of whitespace.
	\param Str String to be trimmed
	\return \c str without whitespace
	*/
	std::string Trim(std::string Str);

	std::vector<std::string> Split(std::string Str, TCHAR Delim);

	/*!	\brief Compresses a string.
	Compress/decompress snippets by Timo Bingmann
	http://panthema.net/2007/0328-ZLibString.html<br>
	Uses the zlib library.
	\param str String to be compressed
	\param compressionLevel
	\return Compressed string
	\sa http://www.zlib.net/manual.html#Constants
	*/
	std::string strcompress(std::string const& str, int compressionLevel = Z_BEST_COMPRESSION);

	/*!	\brief Decompresses a string

	Compress/decompress snippets by Timo Bingmann
	http://panthema.net/2007/0328-ZLibString.html<br>
	Uses the zlib library.
	\param str String to be decompressed
	\return Decompressed string
	*/
	std::string strdecompress(std::string const& str);

	/*
	Copyright (C) 2004-2008 Ren� Nyffenegger

	This source code is provided 'as-is', without any express or implied
	warranty. In no event will the author be held liable for any damages
	arising from the use of this software.

	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this source code must not be misrepresented; you must not
	claim that you wrote the original source code. If you use this source code
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original source code.

	3. This notice may not be removed or altered from any source distribution.

	Ren� Nyffenegger rene.nyffenegger@adp-gmbh.ch
	*/
	static const std::string base64_chars =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz"
		"0123456789+/";
	static inline bool is_base64(unsigned char c) { return (isalnum(c) || (c == '+') || (c == '/')); }
	std::string base64_encode(unsigned char const*, unsigned int);
	std::string base64_decode(std::string const&);
}
