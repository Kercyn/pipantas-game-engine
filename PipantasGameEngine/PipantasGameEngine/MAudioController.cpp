#include "MAudioController.hpp"
#include "SDL_mixer.h"
#include "Managers.hpp"
#include "glm\gtx\vector_angle.hpp"

#include <boost\algorithm\clamp.hpp>
#include <random>

void Pipantas::MusicFinishedCallback()
{
	AudioController.CurrentMusicArea = nullptr;
}

void Pipantas::ChannelFinishedCallback(int Channel)
{
	AudioController.ChannelPool.ReleaseID(Channel);
	AudioController.ChannelOfArea.right.erase(Channel);
}

void MAudioController::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	Mix_HookMusicFinished(&Pipantas::MusicFinishedCallback);
	Mix_ChannelFinished(&Pipantas::ChannelFinishedCallback);

	Logger.Log(ELogLevel::Info, "MAudioController initialized.");
}

void MAudioController::Shutdown(const KeyGameCoordinator&)
{
	HaltAll();
	
	Logger.Log(ELogLevel::Info, "MAudioController shutdown.");
}

void MAudioController::Reset()
{
	ChannelPool.Reset();
}

void MAudioController::HaltAll() const
{
	HaltMusic();
	HaltChannel(MAudioController::ALL_CHANNELS);
}

void MAudioController::PauseAll() const
{
	PauseMusic();
	PauseChannel(MAudioController::ALL_CHANNELS);
}

void MAudioController::ResumeAll() const
{
	ResumeMusic();
	ResumeChannel(MAudioController::ALL_CHANNELS);
}

void MAudioController::SetMasterVolume(int NewVolume) const
{
	SetMusicVolume(NewVolume);
	SetChannelVolume(MAudioController::ALL_CHANNELS, NewVolume);
}

void MAudioController::FadeInArea(AAudioArea* const AudioArea)
{
	if (AudioArea->AreaType == EAreaType::Music)
	{
		BMixMusic& Music = ResourceStore.RequestMusic(AudioArea->SoundID);
		CurrentMusicArea = AudioArea;

		AudioPlayback.FadeInMusic({}, Music, AudioArea->Loops, AudioArea->FadeTime);
		SetMusicPosition(AudioArea->StartPosition);

		QueuedMusicArea = nullptr;
	}
	else if (AudioArea->AreaType == EAreaType::SFX)
	{
		typedef decltype(ChannelOfArea)::value_type AreaChannel;

		auto& Chunk = ResourceStore.RequestChunk(AudioArea->SoundID);

		std::size_t Channel;
		if (ChannelOfArea.left.count(AudioArea) != 0)
		{
			Channel = ChannelOfArea.left.at(AudioArea);
		}
		else
		{
			Channel = ChannelPool.RequestID();
			ChannelOfArea.insert(AreaChannel(AudioArea, Channel));
		}

		AudioPlayback.FadeInChannel({}, Channel, Chunk, AudioArea->Loops, AudioArea->FadeTime);
	}
}

void MAudioController::FadeOutArea(AAudioArea* const AudioArea)
{
	if (AudioArea->AreaType == EAreaType::Music)
	{
		AudioPlayback.FadeOutMusic({}, AudioArea->FadeTime);
	}
	else if (AudioArea->AreaType == EAreaType::SFX)
	{
		const std::size_t Channel = ChannelOfArea.left.at(AudioArea);
		AudioPlayback.FadeOutChannel({}, Channel, AudioArea->FadeTime);
	}
}

void MAudioController::SetMusicPosition(double Position) const
{
	switch (Mix_GetMusicType(nullptr))
	{
		case MUS_OGG:
			// Nothing we need to do; Mix_SetMusicPosition for OGG jumps to <Position> seconds from the beginning.
			break;

		case MUS_MP3:
			// Need to rewind the music first, since Mix_SetMusicPosition for MP3 jumps Position seconds ahead
			// of the *current* position and not from the beginning.
			RewindMusic();
			break;

		default:
			Logger.Log(ELogLevel::Warning, "Only MP3 and OGG formats support custom playback position.");
			break;
	}

	if (Mix_SetMusicPosition(Position) == -1)
	{
		Logger.Log(ELogLevel::Warning, "Failed to set music position for " + CurrentMusicArea->GetID() + ": " + Mix_GetError());
	}
}

void MAudioController::SetMusicVolume(int NewVolume) const
{
	NewVolume = boost::algorithm::clamp(NewVolume, 0, 100);
	int ActualVolume = (NewVolume * MIX_MAX_VOLUME) / 100;
	Mix_VolumeMusic(ActualVolume);
}

void MAudioController::SetChannelVolume(int Channel, int NewVolume) const
{
	NewVolume = boost::algorithm::clamp(NewVolume, 0, 100);
	int ActualVolume = (NewVolume * MIX_MAX_VOLUME) / 100;

	if (Channel == MAudioController::ALL_CHANNELS)
	{
		for (std::size_t i = 0; i < ChannelPool.GetTotalAllocations(); i++)
		{
			Mix_Volume(i, ActualVolume);
		}
	}
	else
	{
		Mix_Volume(Channel, ActualVolume);
	}
	
}

void MAudioController::RewindMusic() const
{
	Mix_RewindMusic();
}

void MAudioController::PauseMusic() const
{
	if (Mix_PausedMusic() == 1)
	{
		Mix_PauseMusic();
	}
}

void MAudioController::ResumeMusic() const
{
	Mix_ResumeMusic();
}

void MAudioController::HaltMusic() const
{
	Mix_HaltMusic();
}

void MAudioController::PauseChannel(int Channel) const
{
	if (Channel == MAudioController::ALL_CHANNELS)
	{
		for (std::size_t i = 0; i < ChannelPool.GetTotalAllocations(); i++)
		{
			Mix_Pause(i);
		}
	}
	else
	{
		Mix_Pause(Channel);
	}
}

void MAudioController::ResumeChannel(int Channel) const
{
	if (Channel == MAudioController::ALL_CHANNELS)
	{
		for (std::size_t i = 0; i < ChannelPool.GetTotalAllocations(); i++)
		{
			Mix_Resume(i);
		}
	}
	else
	{
		Mix_Resume(Channel);
	}
}

void MAudioController::HaltChannel(int Channel) const
{
	if (Channel == MAudioController::ALL_CHANNELS)
	{
		for (std::size_t i = 0; i < ChannelPool.GetTotalAllocations(); i++)
		{
			Mix_HaltChannel(i);
		}
	}
	else
	{
		Mix_HaltChannel(Channel);
	}
}

void MAudioController::AddOnTriggerEvent(EPhysicsTrigger Type, AAudioArea* AudioArea)
{
	if (AudioArea->AreaType == EAreaType::Music)
	{
		switch (Type)
		{
			case EPhysicsTrigger::Enter:
			{
				if (AudioArea->PlayOnEnter())
				{
					QueuedMusicArea = AudioArea;
				}
				break;
			}

			case EPhysicsTrigger::Exit:
				if (AudioArea->ID == CurrentMusicArea->ID)
				{
					FadeOutArea(AudioArea);
				}
				break;

			case EPhysicsTrigger::Stay:
				if (!CurrentMusicArea)
				{
					QueuedMusicArea = AudioArea;
				}
				break;
		}
	}
	else if (AudioArea->AreaType == EAreaType::SFX)
	{
		switch (Type)
		{
			case EPhysicsTrigger::Enter:
				if (AudioArea->PlayOnEnter())
				{
					FadeInArea(AudioArea);
				}
				break;

			case EPhysicsTrigger::Exit:
			{
				FadeOutArea(AudioArea);
				break;
			}

			case EPhysicsTrigger::Stay:
			{
				auto Shape = AudioArea->GetCollider(AAudioArea::COLLIDER_NAME)->GetShape();

				if (Shape->GetType() == EShapeType::Circle)
				{
					int Channel = ChannelOfArea.left.at(AudioArea);
					// [0, 360)
					Sint16 Angle;
					// [0, 255]
					Uint8 Distance;

					// The player is the only entity in the game that acts as a microphone,
					// so he acts as the center of reference for 3D settings. 
					auto PlayerPos = ActorHandler.GetPlayer()->GetBody()->GetPosition();

					const SCircle* const Circle = static_cast<SCircle*>(Shape);
					auto CircleCenter = Circle->GetPosition();

					float DistanceRaw = glm::distance(PlayerPos, CircleCenter);
					float AngleRaw = glm::angle(PlayerPos, CircleCenter);

					if (DistanceRaw < AudioArea->MinDistance)
					{
						Distance = 0;
					}
					else
					{
						DistanceRaw = std::round((DistanceRaw * 255) / Circle->GetRadius());
						Distance = (DistanceRaw > 255.0f) ? 255u : static_cast<Uint8>(DistanceRaw);
					}

					Angle = static_cast<Sint16>(std::round(AngleRaw));
					if (Mix_SetPosition(Channel, Angle, Distance) == 0)
					{
						Logger.Log(ELogLevel::Warning, "Could not set position for channel " + std::to_string(Channel) + " (" + AudioArea->GetID() + "): " + Mix_GetError());
					}
				}

				break;
			}
		}
	}
}

void MAudioController::Tick()
{
	// If music is not playing and it's not paused.
	if ((CurrentMusicArea == nullptr) && (Mix_PausedMusic() == 0))
	{
		if (QueuedMusicArea != nullptr)
		{
			FadeInArea(QueuedMusicArea);
			
			QueuedMusicArea = nullptr;
		}
	}
}
