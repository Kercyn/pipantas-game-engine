#include "PSAP.hpp"
#include "Managers.hpp"
#include "PEndPoint.hpp"

#include <algorithm>
#include <utility>
#include <experimental/set>

PSAP::PSAP()
	: AllColliders(CollisionEngine.GetColliders()), Endpoints(&Utils::EndpointComparator)
{
}


const SAABB& PSAP::GetBoundaries() const
{
	return Boundaries;
}

const std::map<std::uint16_t, PCollider*>& PSAP::GetColliders() const
{
	return SAPColliders;
}

void PSAP::UpdateColliderHullEndpoints(const PCollider* const Collider)
{
	std::uint16_t ID = Collider->GetID();
	const SAABB& ConvexHull = Collider->GetHull();
	const auto& RotationMat = Collider->GetShape()->GetRotationMat();
	const auto& ScaleMat = Collider->GetShape()->GetScaleMat();
	const glm::vec2& Position = Collider->GetAbsolutePosition();

	PEndPoint TempEndpoint;
	
	// Hulls don't rotate, so we multiply by the scale matrix only.

	TempEndpoint.ColliderID = ID;
	TempEndpoint.Value = (ConvexHull.GetMin() * ScaleMat).x + Position.x;
	TempEndpoint.bIsMax = false;
	Endpoints.insert(TempEndpoint);

	TempEndpoint.ColliderID = ID;
	TempEndpoint.Value = (ConvexHull.GetMax() * ScaleMat).x + Position.x;
	TempEndpoint.bIsMax = true;
	Endpoints.insert(TempEndpoint);
}

void PSAP::UpdateStaticColliderEndpoints()
{
	for (const auto& KVP : AllColliders)
	{
		PCollider* Collider = KVP.second;

		// We don't care whether the collider is active or not;
		// its active state may change but the endpoints won't add themselves then...
		if (Collider->IsStatic())
		{
			bool bHullIntersectsSAP = MCollisionEngine::BroadAABBToAABB(&Collider->GetHull(), &Boundaries);

			if (bHullIntersectsSAP)
			{
				SAPColliders.emplace(Collider->GetID(), Collider);
				UpdateColliderHullEndpoints(Collider);
			}
		}
	}
}

void PSAP::UpdateDynamicColliderEndpoints()
{
	auto& CollidersCapture = AllColliders;

	std::experimental::erase_if(Endpoints, [&CollidersCapture](PEndPoint EP) { return ! CollidersCapture.at(EP.ColliderID)->IsStatic(); });

	for (const auto& KVP : AllColliders)
	{
		PCollider* Collider = KVP.second;

		// We don't care whether the collider is active or not;
		// its active state may change but the endpoints won't add themselves then...
		if (! Collider->IsStatic())
		{
			bool bHullIntersectsSAP = MCollisionEngine::BroadAABBToAABB(&Collider->GetHull(), &Boundaries);

			if (bHullIntersectsSAP)
			{
				SAPColliders.emplace(Collider->GetID(), Collider);
				UpdateColliderHullEndpoints(Collider);
			}
		}
	}
}

std::vector<std::pair<std::uint16_t, std::uint16_t>> PSAP::GeneratePairs()
{
	std::vector<std::pair<std::uint16_t, std::uint16_t>> Pairs;

	for (const auto& Endpoint : Endpoints)
	{
		if (Endpoint.bIsMax)
		{
			// Max endpoint found, remove its collider from the active intervals.
			// The collider is guaranteed to exist since for every max endpoint there's a min one,
			// so the collider has already been added to ActiveIntervals.
			ActiveIntervals.erase(Endpoint.ColliderID);
		}
		else
		{
			for (auto ActiveColliderID : ActiveIntervals)
			{
				const PCollider* const ActiveCollider = SAPColliders.at(ActiveColliderID);
				const PCollider* const EndpointOwner = SAPColliders.at(Endpoint.ColliderID);

				// If either collider (or its parent) is disabled,
				// or both colliders are static or their layers don't collide,
				// don't bother doing more checks.
				if (ActiveCollider->IsActive() && EndpointOwner->IsActive())
				{					
					bool bIsStaticColliderPair = ActiveCollider->IsStatic() && EndpointOwner->IsStatic();

					if (!bIsStaticColliderPair)
					{
						bool bLayersCollide = CollisionEngine.DoLayersCollide(ActiveCollider->GetLayer(), EndpointOwner->GetLayer());

						if (bLayersCollide)
						{
							bool bHullsCollide = CollisionEngine.BroadAABBToAABB(&ActiveCollider->GetHull(), &EndpointOwner->GetHull());

							if (bHullsCollide)
							{
								Pairs.emplace_back(Endpoint.ColliderID, ActiveColliderID);
							}

						} // if (!bIsStaticColliderPair)
					} // if (all colliders and their parents are active)
				} // for (auto ActiveColliderID : ActiveIntervals)
			} // else
			
			// Min endpoint found, add its collider to the active intervals.
			ActiveIntervals.insert(Endpoint.ColliderID);
		}
	}

	return Pairs;
}
