#include "MActorHandler.hpp"
#include "Managers.hpp"
#include "rapidxml.hpp"
#include "AText.hpp"
#include "AAudioArea.hpp"
#include "PTriggerCollider.hpp"
#include "Pipanikos.hpp"

#include <memory>

void MActorHandler::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	Logger.Log(ELogLevel::Info, "MActorHandler initialized.");
}

void MActorHandler::Shutdown(const KeyGameCoordinator&)
{
	Logger.Log(ELogLevel::Info, "MActorHandler shutdown.");
}

void MActorHandler::Reset()
{
	IDGenerator.Reset();
	Actors.clear();
}

void MActorHandler::UpdateActorBodies()
{
	for (const auto& KVP : Actors)
	{
		auto& Actor = KVP.second;

		if (Actor->GetActorType() == EActorType::Character)
		{
			ACharacter* Character = static_cast<ACharacter*>(Actor.get());
			const BTexture& CurrTexture = Character->GetCurrentTexture();
			auto Body = Character->GetBody()->Clone();


			switch (Character->GetBody()->GetType())
			{
				case EShapeType::OBB:
				{

					SOBB* OBB = static_cast<SOBB*>(Body.get());
					OBB->SetSize(CurrTexture.Height, CurrTexture.Width / Character->GetCurrentAnim().GetTotalFrames());
					Actor->SetBody({}, Body->Clone());

					break;
				}

				case EShapeType::Circle:
				{
					SCircle* Circle = static_cast<SCircle*>(Body.get());
					// CurrTexture Height and Width should be the same, since it's a circle.
					Circle->SetRadius(CurrTexture.Height);
					Actor->SetBody({}, Body->Clone());
					break;
				}
			}
		}
	}
}

std::set<std::string> MActorHandler::GetActorAnimKeys(std::string Key) const
{
	using namespace rapidxml;

	std::set<std::string> AnimationKeys;

	std::string ActorPropertiesRaw = ResourceStore.RequestString(Key + ResourceStore.Keys.ACTOR_PROPERTIES_SUFFIX);
	std::vector<TCHAR> ActorProperties = std::vector<TCHAR>(ActorPropertiesRaw.c_str(), ActorPropertiesRaw.c_str() + ActorPropertiesRaw.length() + 1);

	xml_document<> XMLDoc;
	XMLDoc.parse<0>(ActorProperties.data());

	auto AnimationNode = XMLDoc.first_node("actor")->first_node("animations")->first_node("animation");

	while (AnimationNode != nullptr)
	{
		std::string AnimKey = Key + "_" + AnimationNode->first_attribute("key")->value();
		AnimationKeys.emplace(AnimKey);

		AnimationNode = AnimationNode->next_sibling();
	}

	return AnimationKeys;
}

void MActorHandler::UpdateActorInformation(const KeySceneSwitcher&, const MapElements::Object& SpawnerObj)
{
	auto ID = SpawnerObj.Properties.GetProperty(AActor::ActorProperties.A_ID);
	if (ID)
	{
		auto Actor = Actors[*ID].get();

		if (Actor->GetActorType() == EActorType::Character)
		{
			auto Character = static_cast<ACharacter*>(Actors[*ID].get());

			Character->LoadProperties(Character->GetKey() + ResourceStore.Keys.ACTOR_PROPERTIES_SUFFIX);
			Character->UpdateInformation({}, SpawnerObj);
			Character->OverridePropFileAttributes({}, SpawnerObj);
		}
		else if (Actor->GetActorType() == EActorType::Audio)
		{
			auto AudioArea = static_cast<AAudioArea*>(Actors[*ID].get());
			AudioArea->UpdateInformation({}, SpawnerObj);
		}
		else if (Actor->GetActorType() == EActorType::Text)
		{
			auto Text = static_cast<AText*>(Actors[*ID].get());
			Text->UpdateInformation({}, SpawnerObj);
			Text->OverridePropFileAttributes({}, SpawnerObj);
		}
		else
		{
			auto Actor = Actors[*ID].get();
			Actor->LoadProperties(Actor->GetKey() + ResourceStore.Keys.ACTOR_PROPERTIES_SUFFIX);
			Actor->UpdateInformation({}, SpawnerObj);
			Actor->OverridePropFileAttributes({}, SpawnerObj);
		}
	}
	else
	{
		throw(Pipanikos("ID attribute not found for " + SpawnerObj.Name, __FILE__, __LINE__));
	}
}

void MActorHandler::GenerateAudioAreaColliders(const KeySceneSwitcher&, const MapElements::ObjectLayer& Layer)
{
	for (auto& SoundArea : Layer.Objects)
	{
		auto ID = SoundArea.Properties.GetProperty(AActor::ActorProperties.A_ID);
		// We know A_ID exists; it has already been parsed.
		auto Actor = Actors.at(*ID).get();
		auto& Colliders = Actor->GetColliders({});

		Colliders.emplace_back(std::make_unique<PTriggerCollider>());

		auto TriggerCollider = static_cast<PTriggerCollider*>(Colliders.back().get());

		TriggerCollider->ID = CollisionEngine.GenerateColliderID();
		TriggerCollider->Name = AAudioArea::COLLIDER_NAME;
		TriggerCollider->AttachedActor = nullptr;
		TriggerCollider->bIsStatic = true;
		TriggerCollider->TriggerType = ETriggerType::Audio;
		TriggerCollider->bEnabledSelf = true;
		TriggerCollider->bNeedsUpdating = false;
		TriggerCollider->Offset = glm::vec2();

		if (auto CollisionLayerAttr = Layer.Properties.GetProperty(MapElements::Layer::ATTR_COLLISIONLAYER))
		{
			TriggerCollider->Layer = CollisionEngine.GetLayer(*CollisionLayerAttr);
		}
		else
		{
			throw(Pipanikos("CollisionLayer attribute missing from " + Layer.Name, __FILE__, __LINE__));
		}

		TriggerCollider->SetProperties(SoundArea.Properties.GetAllProperties());

		const auto Map = SceneSwitcher.GetCurrentMap();

		glm::vec2 Position;
		Position.x = SoundArea.Position.x;
		Position.y = (Map->GetHeight() * Map->GetTileSize().x) - SoundArea.Position.y;

		auto Shape = SoundArea.Properties.GetProperty(AudioMapProperties::SHAPE);
		if (Shape)
		{
			if (*Shape == AudioMapValues::SHAPE_RECT)
			{
				TriggerCollider->Shape = std::make_unique<SOBB>();
				SOBB* OBB = static_cast<SOBB*>(TriggerCollider->Shape.get());

				OBB->SetPosition(Position);
				OBB->SetSize(SoundArea.Height, SoundArea.Width);

				Actor->SetBody({}, OBB->Clone());
			}
			else if (*Shape == AudioMapValues::SHAPE_CIRCLE)
			{
				if (std::abs(SoundArea.Height - SoundArea.Width) > Utils::EPSILON)
				{
					throw(Pipanikos("Parsed shape is not a circle: " + std::to_string(SoundArea.Width) + "x" + std::to_string(SoundArea.Height), __FILE__, __LINE__));
				}
				else
				{
					TriggerCollider->Shape = std::make_unique<SCircle>();
					SCircle* Circle = static_cast<SCircle*>(TriggerCollider->Shape.get());

					Circle->SetPosition(Position);
					Circle->SetRadius(SoundArea.Height / 2.0f);

					Actor->SetBody({}, Circle->Clone());
				}
			}
			else
			{
				throw(Pipanikos("Invalid shape attribute " + *Shape + " for " + SoundArea.Name, __FILE__, __LINE__));
			}
		}
		else
		{
			throw(Pipanikos("No shape attribute found for " + SoundArea.Name, __FILE__, __LINE__));
		}

		EShapeType ShapeType = TriggerCollider->GetShape()->GetType();
		if (ShapeType == EShapeType::OBB)
		{
			static_cast<SOBB*>(TriggerCollider->GetShape())->SetRotation(SoundArea.Rotation);
		}
		else if (ShapeType == EShapeType::Circle)
		{
			static_cast<SCircle*>(TriggerCollider->GetShape())->SetRotation(SoundArea.Rotation);
		}

		TriggerCollider->AttachedActor = Actor;
		TriggerCollider->Update();

		for (const auto& Collider : Colliders)
		{
			CollisionEngine.RegisterCollider(Collider.get(), Collider->IsStatic());
		}
	}
}

void MActorHandler::AddActor(const KeySceneSwitcher&, const MapElements::Object& Spawner, EActorType Type)
{
	auto ID = Spawner.Properties.GetProperty(AActor::ActorProperties.A_ID);
	auto Key = Spawner.Properties.GetProperty(AActor::ActorProperties.KEY);

	if (!ID)
	{
		throw(Pipanikos("No A_ID attribute found for " + Spawner.Name, __FILE__, __LINE__));
	}
	else if ((!Key) && (Type != EActorType::Audio))
	{
		throw(Pipanikos("No key attribute found for " + Spawner.Name, __FILE__, __LINE__));
	}
	else
	{
		switch (Type)
		{
			case EActorType::Actor:
				Actors.emplace(*ID, std::make_unique<AActor>());
				Renderer.InitializeActorRenderData(*ID);
				break;
			
			case EActorType::Audio:
				Actors.emplace(*ID, std::make_unique<AAudioArea>());
				break;

			case EActorType::Character:
				Actors.emplace(*ID, std::make_unique<ACharacter>());
				Renderer.InitializeActorRenderData(*ID);

				if (Key == ResourceStore.Keys.PLAYER_ACTOR_KEY)
				{
					Player = static_cast<ACharacter*>(Actors.at(*ID).get());
				}
				break;

			case EActorType::Text:
				Actors.emplace(*ID, std::make_unique<AText>());
				Renderer.InitializeActorRenderData(*ID);
				break;
		}

		auto Actor = Actors.at(*ID).get();
		Actor->SetID({}, *ID);

		if (Type == EActorType::Audio)
		{
			Key = ID;
		}

		Actor->SetKey({}, *Key);
		Actor->SetActorType({}, Type);
	}
}

ACharacter* MActorHandler::GetPlayer()
{
	return Player;
}

AActor* MActorHandler::GetActor(std::string ID)
{
	if (Actors.count(ID) != 0)
	{
		return Actors.at(ID).get();
	}
	else
	{
		throw(Pipanikos("Could not find Actor " + ID, __FILE__, __LINE__));
		return nullptr;
	}
}

std::vector<AActor*> MActorHandler::GetAllActors()
{
	std::vector<AActor*> AllActors;
	AllActors.reserve(Actors.size());

	for (const auto& KVP : Actors)
	{
		AllActors.push_back(KVP.second.get());
	}

	return AllActors;
}
