#include "MPairGenerator.hpp"
#include "Managers.hpp"
#include "CMap.hpp"
#include "Pipanikos.hpp"

#include <algorithm>

void MPairGenerator::Initialize(const KeyGameCoordinator &, const std::vector<std::string>&)
{
	Logger.Log(ELogLevel::Info, "MPairGenerator initialized.");
}

void MPairGenerator::Shutdown(const KeyGameCoordinator &)
{
	Logger.Log(ELogLevel::Info, "MPairGenerator shutdown.");
}

void MPairGenerator::Reset()
{
	SAPs.clear();
}

const std::vector<PSAP>& MPairGenerator::GetSAPs() const
{
	return SAPs;
}

const std::vector<std::pair<std::uint16_t, std::uint16_t>>& MPairGenerator::GetCollidingPairs() const
{
	return CollidingPairs;
}

void MPairGenerator::UpdateSAPAreas(const KeySceneSwitcher&)
{
	// Each SAP's size, which is (SAPs per axis) / (Map size on that axis).
	glm::vec2 SAPSize;
	const auto CurrentMap = SceneSwitcher.GetCurrentMap();

	SDL_Point TotalSAP = { 1, 1 };
	if (auto SapX = CurrentMap->GetProperty(CMap::PROP_SAP_X))
	{
		TotalSAP.x = std::stoi(*SapX);
	}
	else
	{
		throw(Pipanikos(CMap::PROP_SAP_X + " attribute not found", __FILE__, __LINE__));
	}

	if (auto SapY = CurrentMap->GetProperty(CMap::PROP_SAP_Y))
	{
		TotalSAP.y = std::stoi(*SapY);
	}
	else
	{
		throw(Pipanikos(CMap::PROP_SAP_Y + " attribute not found", __FILE__, __LINE__));
	}

	SAPSize.x = (CurrentMap->GetWidth() * CurrentMap->GetTileSize().x) / (float)TotalSAP.x;
	SAPSize.y = (CurrentMap->GetHeight() * CurrentMap->GetTileSize().y) / (float)TotalSAP.y;

	// Create and configure new SAPs.
	for (int i = 0; i < TotalSAP.x; i++)
	{
		for (int j = 0; j < TotalSAP.y; j++)
		{
			SAPs.emplace_back(PSAP());
			auto& SAPRef = SAPs.back();

			// SAP boundaries
			SAPRef.Boundaries.SetSize(SAPSize);

			// The SAP's original position is at the origin, so we translate it by half its size so that it's entirely
			// on the positive axes, then we move it to its proper position.
			auto x = (i * SAPSize.x) + (SAPSize.x / 2.0f);
			auto y = (j * SAPSize.y) + (SAPSize.y / 2.0f);
			SAPRef.Boundaries.SetPosition(x, y);
		}
	}

	for (auto& SAP : SAPs)
	{
		SAP.UpdateStaticColliderEndpoints();
	}
}

void MPairGenerator::GeneratePairs(const KeyCollisionEngine&)
{
	CollidingPairs.clear();

	for (auto& SAP : SAPs)
	{
		SAP.UpdateDynamicColliderEndpoints();

		auto GeneratedPairs = SAP.GeneratePairs();
		CollidingPairs.insert(CollidingPairs.end(), GeneratedPairs.begin(), GeneratedPairs.end());
	}
}
