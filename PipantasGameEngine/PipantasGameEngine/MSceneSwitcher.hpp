#pragma once

/*! \file */

#include "IManager.hpp"
#include "CScene.hpp"
#include "CMap.hpp"

#include <string>
#include <set>
#include <vector>
#include <memory>

/*! \brief Manager class responsible for switching between scenes.
*/
class MSceneSwitcher final : public IManager
{
	public:
		MSceneSwitcher() = default;
		~MSceneSwitcher() = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;

		/*! \brief Changes the current scene
		\param ID The unique identifier of the scene's map, as defined in the gamedata file.
		*/
		void ChangeScene(std::string ID);

		// If I attempt to return a const reference, unique_ptrs throw errors at multiple points.
		const CMap* const GetCurrentMap() const;

		std::string GetFirstSceneKey() const;

		const CScene* const GetCurrentScene() const;

	private:
		std::unique_ptr<CScene> CurrentScene;

		std::string FirstSceneKey;

		//! Returns the IDs of the resources needed for the current map. IDs of resources that are already loaded are not included.
		std::set<std::string> CalculateSceneResources() const;

		//! Associates the keys and paths of parallaxes that are used in the current map.
		void PopulateParallaxPaths() const;
};
