#include "PLine.hpp"
#include "SDL_stdinc.h"
#include "Managers.hpp"
#include "Utils.hpp"
#include "glm\gtx\projection.hpp"
#include "TAxisDataGroup.hpp"

#include <limits>
#include <functional>

Line::Line(glm::vec2 a_, glm::vec2 b_)
	: a(a_), b(b_)
{
}

glm::vec2 Line::CalculatePenetrationVector(const Line& Other) const
{
	// There's definitely a better way to design/implement this functionality, but yolo

	glm::vec2 PenetrationVec;
	glm::vec2 ThisDir = glm::normalize(a - b);
	//glm::vec2 OtherDir = glm::normalize(a - b);

	TAxisDataGroup<float> ThisMax;
	TAxisDataGroup<float> ThisMin;
	TAxisDataGroup<float> OtherMax;
	TAxisDataGroup<float> OtherMin;

	ThisMax.X = (a.x > b.x) ? a.x : b.x;
	ThisMin.X = (a.x > b.x) ? b.x : a.x;
	OtherMax.X = (Other.a.x > Other.b.x) ? Other.a.x : Other.b.x;
	OtherMin.X = (Other.a.x > Other.b.x) ? Other.b.x : Other.a.x;

	ThisMax.Y = (a.y > b.y) ? a.y : b.y;
	ThisMin.Y = (a.y > b.y) ? b.y : a.y;
	OtherMax.Y = (Other.a.y > Other.b.y) ? Other.a.y : Other.b.y;
	OtherMin.Y = (Other.a.y > Other.b.y) ? Other.b.y : Other.a.y;

	// ThisDir == OtherDir, so we can use either one
	if (std::abs(ThisDir.x) < Utils::EPSILON)
	{
		// Lines are vertical

		PenetrationVec.x = 0.0f;

		if ((ThisMax.Y > OtherMin.Y) && (ThisMax.Y <= OtherMax.Y))				
		{
			PenetrationVec.y = ThisMax.Y - OtherMin.Y;
		}
		else if ((OtherMax.Y > ThisMin.Y) && (OtherMax.Y <= ThisMax.Y))
		{
			PenetrationVec.y = OtherMax.Y - ThisMin.Y;
		}
		else if (((ThisMax.Y >= OtherMax.Y) && (ThisMin.Y <= OtherMin.Y)) ||
			((OtherMax.Y >= ThisMax.Y) && (OtherMin.Y <= ThisMin.Y)))
		{
			PenetrationVec.y = std::numeric_limits<float>::max();
		}
	}
	else if (std::abs(ThisDir.y) < Utils::EPSILON)
	{
		// Lines are horizontal

		PenetrationVec.y = 0.0f;

		if ((ThisMax.X > OtherMin.X) && (ThisMax.X <= OtherMax.X))
		{
			PenetrationVec.x = ThisMax.X - OtherMin.X;
		}
		else if ((OtherMax.X > ThisMin.X) && (OtherMax.X <= ThisMax.X))
		{
			PenetrationVec.x = OtherMax.X - ThisMin.X;
		}
		else if (((ThisMax.X >= OtherMax.X) && (ThisMin.X <= OtherMin.X)) ||
			((OtherMax.X >= ThisMax.X) && (OtherMin.X <= ThisMin.X)))
		{
			PenetrationVec.x = std::numeric_limits<float>::max();
		}
	}
	else
	{
		// We could have compared y coords, but it's the same.
		// If at some point signs do matter I'll need to re-write this but it's already bad enough.
		if ((ThisMax.X > OtherMin.X) && (ThisMax.X <= OtherMax.X) ||
			((ThisMax.X > OtherMax.X) && (ThisMin.X < OtherMin.X)))
		{
			PenetrationVec.x = ThisMax.X - OtherMin.X;
			PenetrationVec.y = ThisMax.Y - OtherMin.Y;
		}
		else if ((OtherMax.X > ThisMin.X) && (OtherMax.X <= ThisMax.X) ||
			((OtherMax.X > ThisMax.X) && (OtherMin.X < ThisMin.X)))
		{
			PenetrationVec.x = OtherMax.X - ThisMin.X;
			PenetrationVec.y = OtherMax.Y - ThisMin.Y;
		}
	}

	return PenetrationVec;
}

float Line::GetLength() const
{
	return std::sqrt(std::pow(b.x - a.x, 2) + std::pow(b.y - b.y, 2));
}

bool Line::operator<(const Line& lhs) const
{
	return GetLength() < lhs.GetLength();
}

bool Line::operator>(const Line& lhs) const
{
	return GetLength() > lhs.GetLength();
}

bool Line::operator<=(const Line& lhs) const
{
	return GetLength() <= lhs.GetLength();
}

bool Line::operator>=(const Line& lhs) const
{
	return GetLength() >= lhs.GetLength();
}

bool Line::operator==(const Line& lhs) const
{
	return GetLength() == lhs.GetLength();
}

bool Line::operator!=(const Line& lhs) const
{
	return GetLength() != lhs.GetLength();
}
