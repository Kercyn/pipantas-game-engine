#include "MScriptMiddleware.hpp"
#include "Managers.hpp"
#include "ScriptInterfaces.hpp"
#include "rapidxml.hpp"
#include "Utils.hpp"
#include "Pipanikos.hpp"

MScriptMiddleware::MScriptMiddleware()
	: State(sel::State(true))
{
}

void MScriptMiddleware::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	// Member functions
	State["PActor"].SetClass<SIActor, std::string>(
		"GetPosition", &SIActor::GetPosition,
		"GetKey", &SIActor::GetKey,
		"GetID", &SIActor::GetID,
		"GetProperty", &SIActor::GetProperty,
		"GetColliderID", &SIActor::GetColliderID,
		"HasCollider", &SIActor::HasCollider,
		"IsEnabled", &SIActor::IsEnabled,
		"Translate", &SIActor::Translate,
		"Rotate", &SIActor::Rotate,
		"SetPosition", &SIActor::SetPosition,
		"SetRotation", &SIActor::SetRotation,
		"SetOrientation", &SIActor::SetOrientation,
		"SetProperty", &SIActor::SetProperty,
		"Enable", &SIActor::Enable,
		"Disable", &SIActor::Disable);

	State["PCharacter"].SetClass<SICharacter, std::string>(
		"GetPosition", &SICharacter::GetPosition,
		"GetOrientation", &SICharacter::GetOrientation,
		"GetKey", &SICharacter::GetKey,
		"GetID", &SICharacter::GetID,
		"GetProperty", &SICharacter::GetProperty,
		"GetColliderID", &SICharacter::GetColliderID,
		"HasCollider", &SICharacter::HasCollider,
		"IsEnabled", &SICharacter::IsEnabled,
		"Translate", &SICharacter::Translate,
		"Rotate", &SICharacter::Rotate,
		"SetPosition", &SICharacter::SetPosition,
		"SetRotation", &SICharacter::SetRotation,
		"SetOrientation", &SICharacter::SetOrientation,
		"SetProperty", &SICharacter::SetProperty,
		"Enable", &SICharacter::Enable,
		"Disable", &SICharacter::Disable,
		"ChangeAnimation", &SICharacter::ChangeAnimation);

	State["PText"].SetClass<SIText, std::string>(
		"GetPosition", &SIText::GetPosition,
		"GetKey", &SIText::GetKey,
		"GetID", &SIText::GetID,
		"GetProperty", &SIText::GetProperty,
		"GetColliderID", &SIText::GetColliderID,
		"IsEnabled", &SIText::IsEnabled,
		"Translate", &SIText::Translate,
		"Rotate", &SIText::Rotate,
		"SetPosition", &SIText::SetPosition,
		"SetRotation", &SIText::SetRotation,
		"SetOrientation", &SIText::SetOrientation,
		"SetProperty", &SIText::SetProperty,
		"SetText", &SIText::SetText,
		"Enable", &SIText::Enable,
		"Disable", &SIText::Disable);

	State["PCamera"].SetClass<SICamera>(
		"GetPosition", &SICamera::GetPosition,
		"GetZoom", &SICamera::SetZoom,
		"Translate", &SICamera::Translate,
		"SetPosition", &SICamera::SetPosition,
		"SetZoom", &SICamera::SetZoom);

	State["PGame"].SetClass<SIGame>(
		"GetDeltaTime", &SIGame::GetDeltaTime,
		"GetFixedDeltaTime", &SIGame::GetFixedDeltaTime,
		"GetCharacters", &SIGame::GetCharacters,
		"GetActors", &SIGame::GetActors,
		"GetTexts", &SIGame::GetTexts,
		"GetCurrentSceneKey", &SIGame::GetCurrentSceneKey,
		"ChangeScene", &SIGame::ChangeScene,
		"SetDebugPhysics", &SIGame::SetDebugPhysics,
		"Raycast", &SIGame::Raycast,
		"IsKeyPressed", &SIGame::IsKeyPressed,
		"PrintLoadedResources", &SIGame::PrintLoadedResources,
		"PrintActorProperties", &SIGame::PrintActorProperties,
		"AnimInfLoop", &SIGame::AnimInfLoop);

	State["PCollider"].SetClass<SICollider, LUA_INTEGER>(
		"GetAttachedActorID", &SICollider::GetAttachedActorID,
		"IsActive", &SICollider::IsActive,
		"IsEnabled", &SICollider::IsEnabled,
		"HasAttachedActor", &SICollider::HasAttachedActor,
		"GetName", &SICollider::GetName,
		"GetSize", &SICollider::GetSize,
		"GetOffset", &SICollider::GetOffset,
		"Enable", &SICollider::Enable,
		"Disable", &SICollider::Disable,
		"SetOffset", &SICollider::SetOffset);

	State["PTriggerCollider"].SetClass<SITriggerCollider, LUA_INTEGER>(
		"GetAttachedActorID", &SITriggerCollider::GetAttachedActorID,
		"GetProperty", &SITriggerCollider::GetProperty,
		"IsActive", &SITriggerCollider::IsActive,
		"IsEnabled", &SITriggerCollider::IsEnabled,
		"HasAttachedActor", &SITriggerCollider::HasAttachedActor,
		"GetName", &SITriggerCollider::GetName,
		"GetSize", &SITriggerCollider::GetSize,
		"GetOffset", &SITriggerCollider::GetOffset,
		"Enable", &SITriggerCollider::Enable,
		"Disable", &SITriggerCollider::Disable,
		"SetOffset", &SITriggerCollider::SetOffset);

	State.Load(ResourceStore.Keys.SCRIPT_INIT_FILE_PATH);
	Logger.Log(ELogLevel::Trace, "Loaded init script.");

	for (const auto& Path : GetScriptPaths())
	{
		State.LoadString(ResourceLoader.GetFileContents(Path));
		Logger.Log(ELogLevel::Trace, "Loaded script at " + Path);
	}

	Logger.Log(ELogLevel::Trace, "Running user Initialize.");
	Run("Initialize");

	Logger.Log(ELogLevel::Info, "MScriptMiddleware initialized.");
}

void MScriptMiddleware::Shutdown(const KeyGameCoordinator&)
{
	Logger.Log(ELogLevel::Info, "MScriptMiddleware shutdown.");
}

void MScriptMiddleware::LoadMapScripts(std::string MapKey)
{
	auto PathProp = SceneSwitcher.GetCurrentMap()->GetProperty(CMap::PROP_SCRIPTFILE);
	if (PathProp)
	{
		State.LoadString(ResourceLoader.GetFileContents(ResourceStore.Keys.SCRIPT_PATH + *PathProp));
		Logger.Log(ELogLevel::Trace, "Loaded script at " + ResourceStore.Keys.SCRIPT_PATH + *PathProp);
	}
	else
	{
		throw(Pipanikos("Map " + SceneSwitcher.GetCurrentScene()->GetKey() + " has no " + CMap::PROP_SCRIPTFILE + " property.", __FILE__, __LINE__));
	}
}

void MScriptMiddleware::ProcessInputEvent(std::string Keyname, EInputType Type)
{
	switch (Type)
	{
		case EInputType::KeyDown:
			State["OnKeyDown"](Keyname);
			State["SceneOnKeyDown"](Keyname);
			break;

		case EInputType::KeyUp:
			State["OnKeyUp"](Keyname);
			State["SceneOnKeyUp"](Keyname);
			break;
	}
}

void MScriptMiddleware::AddOnTriggerEvent(EPhysicsTrigger TriggerType, std::string OwnerName, LUA_INTEGER TriggerColliderID, LUA_INTEGER OtherColliderID)
{
	std::string FuncName = OwnerName;

	switch (TriggerType)
	{
		case EPhysicsTrigger::Enter:
			FuncName += "_OnTriggerEnter";
			break;

		case EPhysicsTrigger::Exit:
			FuncName += "_OnTriggerExit";
			break;

		case EPhysicsTrigger::Stay:
			FuncName += "_OnTriggerStay";
			break;
	}

	OnTriggerEvents.emplace_back(std::make_pair(FuncName, std::make_pair(TriggerColliderID, OtherColliderID)));
}

void MScriptMiddleware::Update()
{
	Run("Update");
	Run("SceneUpdate");
}

void MScriptMiddleware::FixedUpdate()
{
	Run("FixedUpdate");
	Run("SceneFixedUpdate");

	for (const auto& EventData : OnTriggerEvents)
	{
		Run(EventData.first, EventData.second.first, EventData.second.second);
	}

	OnTriggerEvents.clear();
}

void MScriptMiddleware::OnLevelLoad()
{
	Run("OnLevelLoad");
	Run("SceneOnLevelLoad");
}

sel::State& MScriptMiddleware::GetState()
{
	return State;
}

std::vector<std::string> MScriptMiddleware::GetScriptPaths() const
{
	using namespace rapidxml;

	std::vector<std::string> ScriptPaths;

	std::string GameDataRaw = ResourceStore.RequestString(ResourceStore.Keys.GAMEDATA_FILE_KEY);
	std::vector<TCHAR> GameData(GameDataRaw.c_str(), GameDataRaw.c_str() + GameDataRaw.size() + 1);

	xml_document<> XMLDoc;
	XMLDoc.parse<0>(GameData.data());

	auto ScriptsNode = XMLDoc.first_node("game")->first_node("assets")->first_node("scripts");
	if (ScriptsNode)
	{
		auto ScriptNode = ScriptsNode->first_node("script");

		while (ScriptNode != nullptr)
		{
			auto PathAttr = ScriptNode->first_attribute("path");

			if (PathAttr)
			{
				ScriptPaths.emplace_back(ResourceStore.Keys.SCRIPT_PATH + PathAttr->value());
			}
			else
			{
				throw(Pipanikos("Script has no path attribute.", __FILE__, __LINE__));
			}

			ScriptNode = ScriptNode->next_sibling();
		}
	}

	return ScriptPaths;
}
