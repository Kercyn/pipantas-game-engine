#pragma once

/*! \file */

#include <map>
#include "IManager.hpp"
#include "SDL.h"

enum class EInputType
{
	KeyDown,
	KeyUp
};

/*! \brief Manager class that retisters user input and sends input events to the Input Handler.
*/
class MInputRegistrar final : public IManager
{
	public:
		MInputRegistrar();
		~MInputRegistrar() = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;

		/*! Registers user input, updating the ButtonPressed map accordingly.*/
		void RegisterInput();

		/*! Returns whether a specific key is being pressed or not.
		\param Keycode The code of the key we're interested in.
		\return true if Keycode is being pressed, false if not.
		*/
		bool IsButtonPressed(SDL_Keycode Keycode);

	private:
		//! Map of keys currently being pressed.
		std::map<int, bool> ButtonPressed;
};
