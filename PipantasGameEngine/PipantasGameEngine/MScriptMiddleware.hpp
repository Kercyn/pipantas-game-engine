#pragma once

/*! \file */

#include "IManager.hpp"
#include "selene.h"
#include "MInputRegistrar.hpp"

#include <map>
#include <tuple>
#include <string>
#include <vector>
#include <utility>

enum class EPhysicsTrigger;

class MScriptMiddleware final : public IManager
{
	public:
		MScriptMiddleware();
		~MScriptMiddleware() = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;

		template <typename... Args>
		void Run(std::string FuncName, Args... args)
		{
			State[FuncName.c_str()](std::forward<Args>(args)...);
		}

		template <typename... Args>
		void Run(std::string Object, std::string MethodName, Args... args)
		{
			State[Object.c_str()][MethodName.c_str()](std::forward<Args>(args)...);
		}

		void LoadMapScripts(std::string MapKey);

		void ProcessInputEvent(std::string Keyname, EInputType Type);
		void AddOnTriggerEvent(EPhysicsTrigger TriggerType, std::string OwnerName, LUA_INTEGER TriggerColliderID, LUA_INTEGER OtherColliderID);

		void Update();
		void FixedUpdate();
		void OnLevelLoad();

		sel::State& GetState();

	private:
		sel::State State;
		
		//! Returns the paths to all the scripts the game uses.
		std::vector<std::string> GetScriptPaths() const;

		//! Vector of OnTriggerXX events fired by the physics manager. Contains the function name and the IDs of both colliders.
		std::vector<std::pair<std::string, std::pair<LUA_INTEGER, LUA_INTEGER>>> OnTriggerEvents;
};
