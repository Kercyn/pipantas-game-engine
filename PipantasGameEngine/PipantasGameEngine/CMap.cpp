#include "CMap.hpp"
#include "zlib.h"
#include "rapidxml.hpp"
#include "Utils.hpp"
#include "Managers.hpp"
#include "PCollider.hpp"
#include "PTriggerCollider.hpp"
#include "AActor.hpp"
#include "AText.hpp"

#include <boost\optional\optional.hpp>
#include <string>
#include <vector>
#include <Windows.h>
#include <regex>

const std::string CMap::OG_TYPE = "Type";
const std::string CMap::OG_ACTORS = "Actors";
const std::string CMap::OG_CHARACTERS = "Characters";
const std::string CMap::OG_SOUNDS = "Sounds";
const std::string CMap::OG_TEXT = "Text";
const std::string CMap::OG_TRIGGERS = "Triggers";

const std::string CMap::PROP_SAP_X = "totalSAP_X";
const std::string CMap::PROP_SAP_Y = "totalSAP_Y";
const std::string CMap::PROP_SCRIPTFILE = "ScriptFile";

void CMap::Load(std::string ID_)
{
	using namespace rapidxml;
	using namespace MapElements;

	auto MapDataRaw = ResourceLoader.GetRawMap(ID_);
	if (MapDataRaw.empty())
	{
		throw(Pipanikos("Map " + ID + " is empty.", __FILE__, __LINE__));
	}

	std::vector<TCHAR> MapData(MapDataRaw.begin(), MapDataRaw.end());

	xml_document<> XMLDoc;
	XMLDoc.parse<0>(MapData.data());

	auto MapNode = XMLDoc.first_node("map");
	auto TilesetNode = MapNode->first_node("tileset");
	auto LayerNode = MapNode->first_node("layer");

	auto PropertiesNode = MapNode->first_node("properties");
	if (PropertiesNode != nullptr)
	{
		auto PropertyNode = PropertiesNode->first_node("property");

		// Map properties
		while (PropertyNode != nullptr)
		{
			std::string Key = PropertyNode->first_attribute("name")->value();
			std::string Value = PropertyNode->first_attribute("value")->value();

			Map.Properties.AddProperty(Key, Value);

			PropertyNode = PropertyNode->next_sibling("property");
		}
	}

	Map.Width = std::stoi(MapNode->first_attribute("width")->value());
	Map.Height = std::stoi(MapNode->first_attribute("height")->value());
	Map.TileWidth = std::stoi(MapNode->first_attribute("tilewidth")->value());
	Map.TileHeight = std::stoi(MapNode->first_attribute("tileheight")->value());

	// Tilesets
	// Maps must have ONE tileset.
	auto ImageNode = TilesetNode->first_node("image");

	Tileset.FirstGID = std::stoi(TilesetNode->first_attribute("firstgid")->value());
	Tileset.Name = TilesetNode->first_attribute("name")->value();
	Tileset.TileHeight = std::stoi(TilesetNode->first_attribute("tileheight")->value());
	Tileset.TileWidth = std::stoi(TilesetNode->first_attribute("tilewidth")->value());
	if (TilesetNode->first_attribute("transparency"))
	{
		Tileset.Transparency = std::stoi(TilesetNode->first_attribute("transparency")->value());
	}
	else
	{
		Tileset.Transparency = -1;
	}

	Tileset.SourceImage.Source = ImageNode->first_attribute("source")->value();
	Tileset.SourceImage.Width = std::stoi(ImageNode->first_attribute("width")->value());
	Tileset.SourceImage.Height = std::stoi(ImageNode->first_attribute("height")->value());
	Tileset.TileToTilesetRatio.x = (float)Tileset.TileWidth / Tileset.SourceImage.Width;
	Tileset.TileToTilesetRatio.y = (float)Tileset.TileHeight / Tileset.SourceImage.Height;

	auto TileNode = TilesetNode->first_node("tile");
	while (TileNode != nullptr)
	{
		int ID = std::stoi(TileNode->first_attribute("id")->value());
		auto PropertyNode = TileNode->first_node("properties")->first_node("property");

		while (PropertyNode != nullptr)
		{
			std::string Key = PropertyNode->first_attribute("name")->value();
			std::string Value = PropertyNode->first_attribute("value")->value();
				
			Tileset.TileProperties[ID].AddProperty(Key, Value);

			PropertyNode = PropertyNode->next_sibling();
		}
		TileNode = TileNode->next_sibling();
	}
		
	ResourceLoader.AddResourcePath(Tileset.Name, ResourceStore.Keys.TILESET_PATH + Utils::ExtractFilename(Tileset.SourceImage.Source));

	// Layers
	while (LayerNode != nullptr)
	{
		Layer TempLayer;
		TempLayer.Tiles.reserve(Map.Height * Map.Width);

		TempLayer.Name = LayerNode->first_attribute("name")->value();
		TempLayer.Width = std::stoi(LayerNode->first_attribute("width")->value());
		TempLayer.Height = std::stoi(LayerNode->first_attribute("height")->value());

		if (LayerNode->first_attribute("visible"))
		{
			TempLayer.bIsVisible = (std::stoi(LayerNode->first_attribute("visible")->value()) == 0) ? false : true;
		}
		else
		{
			TempLayer.bIsVisible = true;
		}

		if (LayerNode->first_attribute("opacity"))
		{
			TempLayer.Opacity = std::stof(LayerNode->first_attribute("opacity")->value());
		}
		else
		{
			TempLayer.Opacity = 1.0f;
		}

		auto TempProperties = LayerNode->first_node("properties");

		if (TempProperties != nullptr)
		{
			auto TempProperty = TempProperties->first_node("property");

			while (TempProperty != nullptr)
			{
				std::string Key = TempProperty->first_attribute("name")->value();
				std::string Value = TempProperty->first_attribute("value")->value();

				TempLayer.Properties.AddProperty(Key, Value);

				TempProperty = TempProperty->next_sibling("property");
			}
		}

		auto DataNode = LayerNode->first_node("data");

		if (DataNode != nullptr)
		{
			if (DataNode->first_attribute("encoding") && DataNode->first_attribute("compression"))
			{
				if ((std::string(DataNode->first_attribute("encoding")->value()) == "base64") && (std::string(DataNode->first_attribute("compression")->value()) == "zlib"))
				{
					std::string Data = Utils::Trim(DataNode->first_node()->value());
					Data = Utils::base64_decode(Data);
					Data = Utils::strdecompress(Data);

					if ((Data.length() / 4) == (TempLayer.Height * TempLayer.Width))
					{
						for (std::size_t i = 0; i < Data.length(); i += 4)
						{
							Tile TempTile;
							TempTile.GID = (Data[i] |
								Data[i + 1] << 8 |
								Data[i + 2] << 16 |
								Data[i + 3] << 24);

							TempLayer.AddTile(TempTile);
						}
					}
					else
					{
						throw(Pipanikos(ID + " size does not match the parsed map size value.", __FILE__, __LINE__));
					}
				}
				else
				{
					throw(Pipanikos("Wrong encoding or compression value for " + ID, __FILE__, __LINE__));
				}
			}
			else
			{
				throw(Pipanikos(ID + " has no encoding or compression attributes", __FILE__, __LINE__));
			}
		}

		Layers.emplace_back(TempLayer);
		LayerNode = LayerNode->next_sibling("layer");
	}

	auto ObjectLayerNode = MapNode->first_node("objectgroup");

	// Object groups and Objects
	while (ObjectLayerNode != nullptr)
	{
		ObjectLayer TempObjectLayer;
		TempObjectLayer.Name = ObjectLayerNode->first_attribute("name")->value();

		// Object group properties
		auto OGProperties = ObjectLayerNode->first_node("properties");
		if (OGProperties != nullptr)
		{
			auto PropertyNode = OGProperties->first_node("property");
			while (PropertyNode != nullptr)
			{
				std::string Key;
				std::string Value;

				Key = PropertyNode->first_attribute("name")->value();
				Value = PropertyNode->first_attribute("value")->value();

				TempObjectLayer.Properties.AddProperty(Key, Value);

				PropertyNode = PropertyNode->next_sibling();
			}
		}

		auto ObjectNode = ObjectLayerNode->first_node("object");

		// Object group objects
		while (ObjectNode != nullptr)
		{
			Object TempObject;

			auto NameAttr = ObjectNode->first_attribute("name");
			auto TypeAttr = ObjectNode->first_attribute("type");

			TempObject.Name = (NameAttr != nullptr) ? NameAttr->value() : "";
			TempObject.Type = (TypeAttr != nullptr) ? TypeAttr->value() : "";
			TempObject.Position.x = std::stof(ObjectNode->first_attribute("x")->value());
			TempObject.Position.y = std::stof(ObjectNode->first_attribute("y")->value());
			TempObject.Width = std::stof(ObjectNode->first_attribute("width")->value());
			TempObject.Height = std::stof(ObjectNode->first_attribute("height")->value());
				
			// We need to move the objects slightly to correspond to the visual rectangular representation of the map.
			// Tiled's x and y attributes refer to the leftmost and topmost point respectively, not to the center of the rectangle.
			// This is NOT done for text layers; we want the text to start from the leftmost and topmost point of the rectangle.
			auto LayerType = TempObjectLayer.Properties.GetProperty(CMap::OG_TYPE);
			if (LayerType)
			{
				if (*LayerType != CMap::OG_TEXT)
				{
					TempObject.Position.x += TempObject.Width / 2.0f;
					TempObject.Position.y += TempObject.Height / 2.0f;
				}
			}

			auto RotationAttr = ObjectNode->first_attribute("rotation");
			TempObject.Rotation = (RotationAttr != nullptr) ? std::stof(RotationAttr->value()) : 0.0f;

			auto ObjectPropertiesNode = ObjectNode->first_node("properties");

			if (ObjectPropertiesNode != nullptr)
			{
				auto ObjectPropertyNode = ObjectPropertiesNode->first_node("property");

				while (ObjectPropertyNode != nullptr)
				{
					std::string Key;
					std::string Value;

					Key = ObjectPropertyNode->first_attribute("name")->value();
					Value = ObjectPropertyNode->first_attribute("value")->value();

					TempObject.Properties.AddProperty(Key, Value);
					ObjectPropertyNode = ObjectPropertyNode->next_sibling();
				}
			}

			TempObjectLayer.AddObject(TempObject);
			ObjectNode = ObjectNode->next_sibling("object");
		}

		ObjectLayers.emplace_back(TempObjectLayer);
		ObjectLayerNode = ObjectLayerNode->next_sibling("objectgroup");
	}

	// Iamge layers
	auto ImageLayerNode = MapNode->first_node("imagelayer");

	while (ImageLayerNode != nullptr)
	{
		ImageLayer TempImageLayer;
		TempImageLayer.Name = ImageLayerNode->first_attribute("name")->value();

		if (auto ImageNode = ImageLayerNode->first_node("image"))
		{
			auto PathAttr = ImageNode->first_attribute("source");
			if (PathAttr)
			{
				TempImageLayer.Path = PathAttr->value();
				TempImageLayer.Height = std::stof(ImageNode->first_attribute("height")->value());
				TempImageLayer.Width = std::stof(ImageNode->first_attribute("width")->value());
			}
			else
			{
				Logger.Log(ELogLevel::Warning, "imagelayer " + TempImageLayer.Name + " has no image node.");
			}
		}
		else
		{
			throw(Pipanikos("imagelayer " + TempImageLayer.Name + " has no image node.", __FILE__, __LINE__));
		}

		auto PropertiesNode = ImageLayerNode->first_node("properties");
		if (PropertiesNode != nullptr)
		{
			auto PropertyNode = PropertiesNode->first_node("property");

			while (PropertyNode != nullptr)
			{
				std::string Key = PropertyNode->first_attribute("name")->value();
				std::string Value = PropertyNode->first_attribute("value")->value();

				TempImageLayer.Properties.AddProperty(Key, Value);
				PropertyNode = PropertyNode->next_sibling();
			}
		}

		ImageLayers.emplace_back(TempImageLayer);
		ImageLayerNode = ImageLayerNode->next_sibling("imagelayer");
	}
	
	CalculateTileMetrics();
	SetObjectLayerTypes();
	GenerateColliders();
	LoadTextData();
}

int CMap::GetHeight() const
{
	return Map.Height;
}

int CMap::GetWidth() const
{
	return Map.Width;
}

glm::vec2 CMap::GetTileSize() const
{
	return glm::vec2(Map.TileWidth, Map.TileHeight);
}

const std::vector<MapElements::ObjectLayer>& CMap::GetObjectLayers() const
{
	return ObjectLayers;
}

const std::vector<MapElements::ImageLayer>& CMap::GetImageLayers() const
{
	return ImageLayers;
}

std::vector<MapElements::ObjectLayer> CMap::GetObjectLayersOfType(MapElements::EObjectLayerType Type) const
{
	std::vector<MapElements::ObjectLayer> ObjectLayersOfType;

	for (const auto& ObjLayer : ObjectLayers)
	{
		if (ObjLayer.Type == Type)
		{
			ObjectLayersOfType.emplace_back(ObjLayer);
		}
	}

	return ObjectLayersOfType;
}

boost::optional<std::string> CMap::GetProperty(std::string Key) const
{
	return Map.Properties.GetProperty(Key);
}

const std::map<std::string, std::string>& CMap::GetTextData() const
{
	return TextData;
}

void CMap::CalculateTileMetrics()
{
	for (auto& Layer : Layers)
	{
		for (auto& Tile : Layer.Tiles)
		{
			// We're only interested in calculating metrics for tiles that will actually get rendered.
			if (Tile.GID != 0)
			{
				// The amount of tiles the tileset contains on each row/column.
				SDL_Point TilesetSize;

				TilesetSize.x = Tileset.SourceImage.Width / Map.TileWidth;
				TilesetSize.y = Tileset.SourceImage.Height / Map.TileHeight;

				int ActualTileX = Tile.GID - 1;

				// Map a 1D index to a 2D index. Some minor hacking involved, you saw nothing...
				if ((ActualTileX % TilesetSize.x) == 0)
				{
					Tile.Index.x = TilesetSize.x;
					Tile.Index.y = (ActualTileX / TilesetSize.x);
				}
				else
				{
					Tile.Index.x = (ActualTileX % TilesetSize.x);
					Tile.Index.y = ActualTileX / TilesetSize.x;
				}

				// Set normalized pixel offset for the tile. Do da math, it actually works!
				// It will be so funny when at some point I pinpoint a bug arising from the math here...
				// ^ It actually happened like 10 minutes after I wrote that. Oh well, I guess it's fixed now!
				Tile.Offset.x = (float)(Tile.Index.x * Tileset.TileWidth) / Tileset.SourceImage.Width;
				Tile.Offset.y = (float)(Tile.Index.y * Tileset.TileHeight) / Tileset.SourceImage.Height;
			}
		}
	}
}

void CMap::GenerateColliders()
{
	// unique_ptr instead of plain objects because the rectangles change from pointers stored in the Overlaps vector.
	std::vector<std::unique_ptr<SAABB>> Rectangles;

	for (const auto& Layer : Layers)
	{
		if (Layer.bIsVisible)
		{
			// Physical (non-trigger) colliders
			if (auto HasCollidersProp = Layer.Properties.GetProperty("GenerateColliders"))
			{
				bool bHasColliders = Utils::ToBool(*HasCollidersProp);
				if (bHasColliders)
				{				
					for (int Column = 0; Column < Map.Width; Column++)
					{
						int StartY = -1;
						int EndY = -1;

						for (int Line = 0; Line < Map.Height; Line++)
						{
							MapElements::Tile Tile = Layer.Tiles[Map.Width * Line + Column];

							if (Tile.GID != 0)
							{
								if (StartY == -1)
								{
									StartY = Line;
								}

								EndY = Line;
							}
							else if (StartY != -1)
							{
								std::vector<SAABB*> Overlaps;
								
								for (const auto& Rect : Rectangles)
								{
									if ((Rect->GetMax().x == (Column - 1))
										&& (StartY <= Rect->GetMin().y)
										&& (EndY >= Rect->GetMax().y))
									{
										Overlaps.emplace_back(Rect.get());
									}
								}

								std::sort(Overlaps.begin(), Overlaps.end(), [](const SAABB* A, const SAABB* B) {
									return A->GetMin().y < B->GetMin().y;
								});

								for (auto& Rect : Overlaps)
								{
									if (StartY < Rect->GetMin().y)
									{
										SAABB NewRect;
										NewRect.SetMin(Column, StartY);
										NewRect.SetMax(Column, Rect->GetMin().y - 1);

										Rectangles.emplace_back(std::make_unique<SAABB>(NewRect));
										StartY = Rect->GetMin().y;
									}

									if (StartY == Rect->GetMin().y)
									{
										float NewMaxX = Rect->GetMax().x + 1;
										Rect->SetMax(NewMaxX, Rect->GetMax().y);

										if (EndY == Rect->GetMax().y)
										{
											StartY = -1;
											EndY = -1;
										}
										else if (EndY > Rect->GetMax().y)
										{
											StartY = Rect->GetMax().y + 1;
										}
									}
								}

								if (StartY != -1)
								{
									SAABB NewRect;
									NewRect.SetMin(Column, StartY);
									NewRect.SetMax(Column, EndY);

									Rectangles.emplace_back(std::make_unique<SAABB>(NewRect));
									StartY = -1;
									EndY = -1;
								}
							}
						} // Lines loop

						if (StartY != -1)
						{
							SAABB NewRect;
							NewRect.SetMin(Column, StartY);
							NewRect.SetMax(Column, EndY);

							Rectangles.emplace_back(std::make_unique<SAABB>(NewRect));
							StartY = -1;
							EndY = -1;
						}
					} // Columns loop

					for (const auto& Rect : Rectangles)
					{
						Colliders.emplace_back(std::make_unique<PCollider>());

						auto& Collider = Colliders.back();
						Collider->ID = CollisionEngine.GenerateColliderID();
						Collider->Name = "";
						Collider->AttachedActor = nullptr;
						Collider->bIsStatic = true;
						Collider->TriggerType = ETriggerType::None;
						Collider->bEnabledSelf = true;
						Collider->bNeedsUpdating = false;

						if (auto CollisionLayerAttr = Layer.Properties.GetProperty(MapElements::Layer::ATTR_COLLISIONLAYER))
						{
							Collider->Layer = CollisionEngine.GetLayer(*CollisionLayerAttr);
						}
						else
						{
							throw(Pipanikos("CollisionLayer attribute missing from " + Layer.Name, __FILE__, __LINE__));
						}

						int Width = (Rect->GetMax().x - Rect->GetMin().x + 1) * Map.TileWidth;
						int Height = (Rect->GetMax().y - Rect->GetMin().y + 1) * Map.TileHeight;

						Collider->Offset.x = (Rect->GetMin().x * Map.TileWidth) + (Width / 2.0f);
						// The map coordinates are upside-down in regards to game coordinates, 
						// so we need to push the colliders down according to their height.
						Collider->Offset.y = ((Map.Height - Rect->GetMin().y - (Height / Map.TileHeight)) * Map.TileHeight) + (Height / 2.0f);

						Collider->Shape = std::make_unique<SOBB>();
						SOBB* ColliderShape = static_cast<SOBB*>(Collider->Shape.get());
						ColliderShape->SetSize(Height, Width);
						ColliderShape->SetPosition(Collider->Offset);
						Collider->Update();
					}
				} // if (bHasColliders)
			} // if (HasColliderProp)
			//else if (auto IsTriggerLayerProp = Layer.Properties.GetProperty("IsTriggerLayer"))
		} // if (Layer.bIsVisible)
	} // for (const auto& Layer : Layers)

	auto TriggerLayers = GetObjectLayersOfType(MapElements::EObjectLayerType::Triggers);

	for (const auto& TriggerLayer : TriggerLayers)
	{
		for (const auto& Trigger : TriggerLayer.Objects)
		{
			Colliders.emplace_back(std::make_unique<PTriggerCollider>());

			auto TriggerCollider = static_cast<PTriggerCollider*>(Colliders.back().get());

			TriggerCollider->ID = CollisionEngine.GenerateColliderID();
			TriggerCollider->Name = Trigger.Name;
			TriggerCollider->AttachedActor = nullptr;
			TriggerCollider->bIsStatic = true;
			TriggerCollider->TriggerType = ETriggerType::Programmable;
			TriggerCollider->bEnabledSelf = true;
			TriggerCollider->bNeedsUpdating = false;
			
			if (auto CollisionLayerAttr = TriggerLayer.Properties.GetProperty(MapElements::Layer::ATTR_COLLISIONLAYER))
			{
				TriggerCollider->Layer = CollisionEngine.GetLayer(*CollisionLayerAttr);
			}
			else
			{
				throw(Pipanikos("CollisionLayer attribute missing from " + TriggerLayer.Name, __FILE__, __LINE__));
			}

			TriggerCollider->SetProperties(Trigger.Properties.GetAllProperties());

			TriggerCollider->Offset.x = Trigger.Position.x;
			TriggerCollider->Offset.y = (Map.Height * Map.TileHeight) - Trigger.Position.y;

			if (Trigger.Type == AudioMapValues::SHAPE_RECT)
			{
				TriggerCollider->Shape = std::make_unique<SOBB>();
				SOBB* ColliderShape = static_cast<SOBB*>(TriggerCollider->GetShape());

				ColliderShape->SetSize(Trigger.Height, Trigger.Width);
				ColliderShape->SetRotation(Trigger.Rotation);
				ColliderShape->SetPosition(TriggerCollider->Offset);
			}
			else if (Trigger.Type == AudioMapValues::SHAPE_CIRCLE)
			{
				TriggerCollider->Shape = std::make_unique<SCircle>();
				SCircle* ColliderShape = static_cast<SCircle*>(TriggerCollider->GetShape());

				ColliderShape->SetRadius(Trigger.Height);
				ColliderShape->SetRotation(Trigger.Rotation);
				ColliderShape->SetPosition(TriggerCollider->Offset);
			}

			TriggerCollider->Update();
		}
	}

	for (const auto& Collider : Colliders)
	{
		CollisionEngine.RegisterCollider(Collider.get(), Collider->bIsStatic);
	}
}

void CMap::SetObjectLayerTypes()
{
	for (auto& ObjectLayer : ObjectLayers)
	{
		ObjectLayer.Type = MapElements::EObjectLayerType::Other;

		auto TypeProp = ObjectLayer.Properties.GetProperty(CMap::OG_TYPE);
		if (TypeProp)
		{
			std::string Type = *TypeProp;

			if (Type == CMap::OG_ACTORS)
			{
				ObjectLayer.Type = MapElements::EObjectLayerType::Actors;
			}
			else if (Type == CMap::OG_CHARACTERS)
			{
				ObjectLayer.Type = MapElements::EObjectLayerType::Characters;
			}
			else if (Type == CMap::OG_SOUNDS)
			{
				ObjectLayer.Type = MapElements::EObjectLayerType::Sounds;
			}
			else if (Type == CMap::OG_TEXT)
			{
				ObjectLayer.Type = MapElements::EObjectLayerType::Text;
			}
			else if (Type == CMap::OG_TRIGGERS)
			{
				ObjectLayer.Type = MapElements::EObjectLayerType::Triggers;
			}
		}
	}
}

void CMap::LoadTextData()
{
	for (const auto& ObjLayer : ObjectLayers)
	{
		if (ObjLayer.Type == MapElements::EObjectLayerType::Text)
		{
			for (const auto& TextObj : ObjLayer.Objects)
			{
				if (auto FontName = TextObj.Properties.GetProperty(AText::TextProperties.FONT))
				{
					if (auto FontSize = TextObj.Properties.GetProperty(AText::TextProperties.SIZE))
					{
						if (auto ObjID = TextObj.Properties.GetProperty(AActor::ActorProperties.A_ID))
						{
							// This is built gradually as font data is parsed for each text object.
							std::string FontDataKey = ResourceStore.Keys.RES_PREFIX_FONTDATA;

							if (auto RenderingAttr = TextObj.Properties.GetProperty(AText::TextProperties.RENDERING))
							{
								if (*RenderingAttr == "blended" ||
									*RenderingAttr == "solid")
								{
									FontDataKey += '_' + *RenderingAttr;
								}
								else
								{
									Logger.Log(ELogLevel::Warning, *RenderingAttr + " is not a valid rendering type, defaulting to solid.");
									FontDataKey += "_solid";
								}
							}
							else
							{
								FontDataKey += "_solid";
							}

							auto StyleAttr = TextObj.Properties.GetProperty(AText::TextProperties.STYLE);
							std::string Style = (StyleAttr) ? *StyleAttr : "";

							std::transform(Style.begin(), Style.end(), Style.begin(), ::tolower);
							std::regex Regex("^(b?i?s?u?)k?$");

							if (!std::regex_match(Style, Regex))
							{
								Logger.Log(ELogLevel::Warning, Style + " is not a valid style, defaulting to no style.");
								FontDataKey += '_';
							}
							else
							{
								FontDataKey += '_' + Style;
							}

							if (auto ColorAttr = TextObj.Properties.GetProperty(AText::TextProperties.COLOR))
							{
								std::string ColorRaw = *ColorAttr;
								std::vector<std::string> ColorValues = Utils::Split(ColorRaw, ',');

								FontDataKey += '_' + ColorValues.at(0);
								FontDataKey += '_' + ColorValues.at(1);
								FontDataKey += '_' + ColorValues.at(2);
								FontDataKey += (ColorValues.size() == 4) ? "_" + ColorValues.at(3) : "_255";
							}
							else
							{
								FontDataKey += "_0_0_0_255";
							}

							TextData.emplace(*ObjID, FontDataKey);
						} // if (auto ObjID = TextObj.Properties.GetProperty(AActor::ActorProperties.A_ID))
						else
						{
							throw(Pipanikos("A_ID attribute not found for " + TextObj.Name, __FILE__, __LINE__));
						}
					} // if (auto FontSize = TextObj.Properties.GetProperty(AActor::ActorProperties.SIZE))
					else
					{
						throw(Pipanikos("Size attribute not found for " + TextObj.Name, __FILE__, __LINE__));
					}
				} // if (auto FontName = TextObj.Properties.GetProperty(AActor::ActorProperties.FONT))
				else
				{
					throw(Pipanikos("Font attribute not found for " + TextObj.Name, __FILE__, __LINE__));
				}
			} // for (const auto& TextObj : ObjLayer.Objects
		} // if (ObjLayer.Type == MapElements::EObjectLayerType::TEXT)
	} // for (const auto& ObjLayer : ObjectLayers)
}
