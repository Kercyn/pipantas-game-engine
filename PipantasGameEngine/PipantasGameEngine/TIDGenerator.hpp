#pragma once

/*! \file */

#include <bitset>
#include <cstddef>

#include "Utils.hpp"
#include "Pipanikos.hpp"

/*! \brief Provides a mechanism that generates reusable unique IDs.
*/
template <std::size_t N>
class TIDGenerator final
{
	public:
		TIDGenerator() = default;
		~TIDGenerator() = default;

		std::size_t RequestID();

		void ReleaseID(std::size_t ID)
		{
			IDPool.reset(ID);
		}

		//! Returns how many IDs the generator has currently allocated.
		std::size_t GetTotalAllocations() const noexcept
		{
			return IDPool.count();
		}

		void Reset() noexcept
		{
			IDPool.reset();
		}

	private:
		std::bitset<N> IDPool;
};

// Implementation written here to avoid circular dependencies with managers that include TIDGenerator.hpp
#include "Managers.hpp"

template <std::size_t N>
std::size_t TIDGenerator<N>::RequestID()
{
	if (IDPool.all())
	{
		throw(Pipanikos("Could not generate ID. Pool size: " + std::to_string(N), __FILE__, __LINE__));
	}
	else
	{
		for (std::size_t i = 0; i < N; i++)
		{
			if (! IDPool.test(i))
			{
				IDPool.set(i);
				return i;
			}
		}
	}
}
