#include "SICamera.hpp"
#include "Managers.hpp"

SICamera::SICamera()
	: Camera(Renderer.GetCamera()), State(ScriptMiddleware.GetState())
{
}

std::tuple<LUA_NUMBER, LUA_NUMBER> SICamera::GetPosition() const
{
	const auto& CameraPos = Camera.GetPosition();

	return std::make_tuple(CameraPos.x, CameraPos.y);
}

float SICamera::GetZoom() const
{
	return Camera.GetPosition().z;
}

void SICamera::Translate(LUA_NUMBER x, LUA_NUMBER y) const
{
	Camera.Translate(x, y);
}

void SICamera::SetPosition(LUA_NUMBER x, LUA_NUMBER y) const
{
	Camera.SetPosition(glm::vec2(x, y));
}

void SICamera::SetZoom(LUA_NUMBER NewZoom) const
{
	Camera.SetZoom(NewZoom);
}
