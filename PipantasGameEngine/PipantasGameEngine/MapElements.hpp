#pragma once

/*! \file */

#include "CProperties.hpp"
#include "glm\glm.hpp"
#include "SDL.h"

#include <string>
#include <vector>
#include <map>

/*! \brief Includes map elements that are used in Tiled maps.
*/
namespace MapElements
{
	enum class EObjectLayerType
	{
		Actors,
		Characters,
		Sounds,
		Text,
		Triggers,
		Other
	};

	struct Map final
	{
		int Width;
		int Height;
		int TileWidth;
		int TileHeight;
		CProperties Properties;

		Map() : Width(0), Height(0), TileWidth(0), TileHeight(0)
		{}
	};

	struct TilesetImage final
	{
		std::string Source;
		int Width;
		int Height;
		TilesetImage() : Width(0), Height(0)
		{}
	};

	struct Tileset final
	{
		int FirstGID;
		std::string Name;
		int TileWidth;
		int TileHeight;
		int Transparency;
		TilesetImage SourceImage;
		
		// Normalized (Tile size / Tileset size)
		glm::vec2 TileToTilesetRatio;

		//! Tile-specific user-defined properties.
		std::map<int, CProperties> TileProperties;

		Tileset() : FirstGID(0), TileWidth(0), TileHeight(0), Transparency(0)
		{
			TileToTilesetRatio.x = TileToTilesetRatio.y = 0.0f;
		}
	};
	
	struct Tile final
	{
		int GID;

		/*! The 2D index of the tile on the tileset.
		If, for example, the tile is on the second column third row on the tileset, its
		index would be (1, 2)
		*/
		SDL_Point Index;

		//! The tile's normalized pixel offset in the tileset, so that the renderer knows where exactly it's located.
		glm::vec2 Offset;

		Tile() : GID(0)
		{
			Index.x = Index.y = 0;
			Offset.x = Offset.y = 0.0f;
		}
	};

	struct Layer final
	{
		static const std::string ATTR_COLLISIONLAYER;

		std::string Name;
		float Opacity;
		bool bIsVisible;
		int Height;
		int Width;
		std::vector<Tile> Tiles;
		CProperties Properties;

		Layer() : Opacity(0.0f), bIsVisible(false), Height(0), Width(0)
		{}

		void AddTile(Tile T)
		{
			Tiles.emplace_back(T);
		}
	};

	struct Object final
	{
		glm::vec2 Position;
		float Width;
		float Height;
		float Rotation;
		std::string Name;
		std::string Type;
		CProperties Properties;

		Object() : Position(glm::vec2()), Width(0.0f), Height(0.0f)
		{}
	};

	struct ObjectLayer final
	{
		std::string Name;
		std::vector<Object> Objects;
		CProperties Properties;
		EObjectLayerType Type;

		ObjectLayer() = default;

		inline void AddObject(Object Object_)
		{
			Objects.emplace_back(Object_);
		}
	};

	struct ImageLayer final
	{
		std::string Name;
		std::string Path;
		float Width;
		float Height;
		CProperties Properties;

		ImageLayer() = default;
	};
}
