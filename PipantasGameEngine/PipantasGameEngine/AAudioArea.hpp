#pragma once

/*! \file */

#include <string>
#include <memory>

#include "AActor.hpp"
#include "BMixMusic.hpp"
#include "PCollider.hpp"
#include "MapElements.hpp"

class CMap;
class MAudioController;
class MAudioPlayback;

struct AudioMapProperties final
{
	static const std::string SOUND_ID;
	static const std::string FADE_TIME;
	static const std::string LOOP;
	static const std::string ROLLOFF;
	static const std::string SHAPE;
	static const std::string TYPE;
	static const std::string VOLUME;
	static const std::string PLAY_ON_ENTER;
	static const std::string PLAY_AT_POS;
	static const std::string MIN_DISTANCE;
};

struct AudioMapValues final
{
	static const std::string TYPE_MUSIC;
	static const std::string TYPE_SFX;
	static const std::string ROLLOFF_NONE;
	static const std::string ROLLOFF_LOG;
	static const std::string ROLLOFF_LINEAR;
	static const std::string SHAPE_RECT;
	static const std::string SHAPE_CIRCLE;
};

enum class ERolloffType
{
	None,
	Logarithmic,
	Linear
};

enum class EAreaType
{
	Music,
	SFX
};

class AAudioArea : public AActor
{
	public:
		AAudioArea();
		~AAudioArea() = default;

		bool PlayOnEnter() const;

		void UpdateInformation(KeyActorHandler, const MapElements::Object& AudioArea) override;

		static const int START_AT_RANDOM;
		static const std::string COLLIDER_NAME;

		static const AudioMapProperties AudioProperties;
		static const AudioMapValues AudioValues;
		
		friend CMap;
		friend MAudioController;
		friend MAudioPlayback;

	private:
		std::string SoundID;
		ERolloffType Rolloff;
		EAreaType AreaType;
		bool bPlayOnEnter;
		int Volume;
		int Loops;
		int FadeTime;
		float MinDistance;
		
		//! The position from which the music starts playing. SFX always start from the beginning.
		double StartPosition;
};
