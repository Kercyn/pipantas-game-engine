#include "CCamera.hpp"
#include "Managers.hpp"
#include "Utils.hpp"
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "Pipanikos.hpp"

#include <array>

//const float CCamera::MaxVerticalAngle = 85.0f;

CCamera::CCamera()
	: Position(0.0f, 0.0f, 5.0f),
	HorizontalAngle(0.0f),
	VerticalAngle(0.0f),
	FieldOfView(5.0f),
	NearPlane(0.01f),
	FarPlane(1000000.0f)
{
}

const glm::vec3& CCamera::GetPosition() const
{
	return Position;
}

float CCamera::GetFoV() const
{
	return FieldOfView;
}

float CCamera::GetNearPlane() const
{
	return NearPlane;
}

float CCamera::GetFarPlane() const
{
	return FarPlane;
}

glm::mat4 CCamera::GetOrientation() const
{
	glm::mat4 Orientation;
	Orientation = glm::rotate(Orientation, glm::radians(VerticalAngle), glm::vec3(1, 0, 0));
	Orientation = glm::rotate(Orientation, glm::radians(HorizontalAngle), glm::vec3(0, 1, 0));
	
	return Orientation;
}

void CCamera::SetPosition(const glm::vec2& Position_)
{
	ParallaxLogic.OnCameraTranslate({}, Position_ - glm::vec2(Position.x, Position.y));
	Position.x = Position_.x;
	Position.y = Position_.y;
}

void CCamera::SetNearFarPlanes(float Near, float Far)
{
	if (Near < 0.0f)
	{
		throw(Pipanikos("Near: " + std::to_string(Near), __FILE__, __LINE__));
	}

	if (Near > Far)
	{
		throw(Pipanikos("Near: " + std::to_string(Near) + " Far: " + std::to_string(Far), __FILE__, __LINE__));
	}
	else
	{
		NearPlane = Near;
		FarPlane = Far;
	}
}

void CCamera::SetZoom(float NewZoom)
{
	Position.z = NewZoom;
}

void CCamera::Translate(const glm::vec2& Offset)
{
	Position.x += Offset.x;
	Position.y += Offset.y;
	ParallaxLogic.OnCameraTranslate({}, Offset);
}

void CCamera::Translate(float x, float y)
{
	Position.x += x;
	Position.y += y;

	ParallaxLogic.OnCameraTranslate({}, glm::vec2(x, y));
}

/*void CCamera::OffsetOrientation(float Up, float Right)
{
	VerticalAngle += Up;
	HorizontalAngle += Right;
	NormalizeAngles();
}

void CCamera::LookAt(const glm::vec3& Position_)
{
	if (Position_ == Position)
	{
		throw(Pipanikos("Can't look at the position of the camera!", __FILE__, __LINE__));
	}
	else
	{
		glm::vec3 Direction = glm::normalize(Position_ - Position);

		VerticalAngle = glm::radians(asinf(-Direction.y));
		HorizontalAngle = -glm::radians(atan2f(-Direction.x, -Direction.z));
		
		NormalizeAngles();
	}
}*/

glm::vec3 CCamera::Forward() const
{
	glm::vec4 Forward = glm::inverse(GetOrientation()) * glm::vec4(0, 0, -1, 1);
	
	return glm::vec3(Forward);
}

glm::vec3 CCamera::Right() const
{
	glm::vec4 Right = glm::inverse(GetOrientation()) * glm::vec4(1, 0, 0, 1);
	
	return glm::vec3(Right);
}

glm::vec3 CCamera::Up() const
{
	glm::vec4 Up = glm::inverse(GetOrientation()) * glm::vec4(0, 1, 0, 1);

	return glm::vec3(Up);
}

glm::mat4 CCamera::Projection() const
{
	return glm::perspective(glm::radians(FieldOfView), 1.0f, NearPlane, FarPlane);
}

glm::mat4 CCamera::View() const
{
	return GetOrientation() * glm::translate(glm::mat4(), -Position);
}

glm::mat4 CCamera::Matrix() const
{
	return Projection() * View();
}

/*void CCamera::NormalizeAngles()
{
	HorizontalAngle = fmodf(HorizontalAngle, 360.0f);

	//fmodf can return negative values, but this will make them all positive.
	if (HorizontalAngle < 0.0f)
	{
		HorizontalAngle += 360.0f;
	}

	if (VerticalAngle > MaxVerticalAngle)
	{
		VerticalAngle = MaxVerticalAngle;
	}
	else if (VerticalAngle < -MaxVerticalAngle)
	{
		VerticalAngle = -MaxVerticalAngle;
	}
}*/