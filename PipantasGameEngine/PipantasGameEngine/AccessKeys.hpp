#pragma once

/*! \file */


#include <boost\noncopyable.hpp>

/*!	Access keys are used to control who can call specific functions.

For example, a Manager's Initialize and Shutdown functions should only be called by the 
Game Coordinator. Hence in order to call them, the caller must provide a Game Coordinator key.
*/

class MGameCoordinator;
class MLogger;
class MPhysFS;
class MResourceStore;
class MResourceLoader;
class MActorHandler;
class MSceneSwitcher;
class MRenderer;
class MFileParser;
class MInputRegistrar;
class MCollisionEngine;
class MScriptMiddleware;
class MAudioController;
class MAudioPlayback;
class MParallaxLogic;

class BShaderProgram;
class CCamera;

// SDL defines main as SDL_main, so we need to include this in order to befriend the proper main.
#include "SDL_main.h"

class KeyGameCoordinator final : public boost::noncopyable
{
	private:
		KeyGameCoordinator() = default;
		friend MGameCoordinator;

		// Needing this means there's probably some bad design hidden somewhere...
		friend int ::main(int argc, char* argv[]);
};

class KeyLogger final : public boost::noncopyable
{
	private:
		KeyLogger() = default;
		friend MLogger;
};

class KeyPhysFS final : public boost::noncopyable
{
	private:
		KeyPhysFS() = default;
		friend MPhysFS;
};

class KeyResourceStore final : public boost::noncopyable
{
	private:
		KeyResourceStore() = default;
		friend MResourceStore;
};

class KeyResourceLoader final : public boost::noncopyable
{
	private:
		KeyResourceLoader() = default;
		friend MResourceLoader;
};

class KeyActorHandler final : public boost::noncopyable
{
	private:
		KeyActorHandler() = default;
		friend MActorHandler;
};

class KeySceneSwitcher final : public boost::noncopyable
{
	private:
		KeySceneSwitcher() = default;
		friend MSceneSwitcher;
};

class KeyRenderer final : public boost::noncopyable
{
	private:
		KeyRenderer() = default;
		friend MRenderer;
};

class KeyFileParser final : public boost::noncopyable
{
	private:
		KeyFileParser() = default;
		friend MFileParser;
};

class KeyInputRegistrar final : public boost::noncopyable
{
	private:
		KeyInputRegistrar() = default;
		friend MInputRegistrar;
};

class KeyCollisionEngine final : public boost::noncopyable
{
	private:
		KeyCollisionEngine() = default;
		friend MCollisionEngine;
};

class KeyScriptMiddleware final : public boost::noncopyable
{
	private:
		KeyScriptMiddleware() = default;
		friend MScriptMiddleware;
};

class KeyAudioController final : public boost::noncopyable
{
	private:
		KeyAudioController() = default;
		friend MAudioController;
};

class KeyAudioPlayback final : public boost::noncopyable
{
	private:
		KeyAudioPlayback() = default;
		friend MAudioPlayback;
};

class KeyParallaxLogic final : public boost::noncopyable
{
	private:
		KeyParallaxLogic() = default;
		friend MParallaxLogic;
};

class KeyShaderProgram final : public boost::noncopyable
{
	private:
		KeyShaderProgram() = default;
		friend BShaderProgram;
};

class KeyCamera final : public boost::noncopyable
{
	private:
		KeyCamera() = default;
		friend CCamera;
};
