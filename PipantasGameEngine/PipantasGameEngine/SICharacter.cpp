#include "SICharacter.hpp"
#include "Managers.hpp"
#include "Pipanikos.hpp"

SICharacter::SICharacter(std::string Key)
{
	// No null checks needed; Actor Manager will throw an error if the actor doesn't exist.
	Character = static_cast<ACharacter*>(ActorHandler.GetActor(Key));
}

void SICharacter::ChangeAnimation(std::string Key, int Loop, bool bPlayInReverse) const
{
	Character->ChangeAnimation(Key, Loop, bPlayInReverse);
}

std::tuple<LUA_NUMBER, LUA_NUMBER> SICharacter::GetPosition() const
{
	const auto& Position = Character->GetBody()->GetPosition();

	return std::make_tuple(static_cast<LUA_NUMBER>(Position.x), static_cast<LUA_NUMBER>(Position.y));
}

std::tuple<LUA_NUMBER, LUA_NUMBER> SICharacter::GetOrientation() const
{
	const auto& Orientation = Character->GetOrientation();

	return std::make_tuple(static_cast<LUA_NUMBER>(Orientation.x), static_cast<LUA_NUMBER>(Orientation.y));
}

std::string SICharacter::GetKey() const
{
	return Character->GetKey();
}

std::string SICharacter::GetID() const
{
	return Character->GetID();
}

std::string SICharacter::GetProperty(std::string PropKey) const
{
	return Character->GetProperty(PropKey);
}

bool SICharacter::IsEnabled() const
{
	return Character->IsEnabled();
}

bool SICharacter::HasCollider(std::string Name) const
{
	return (Character->GetCollider(Name) != nullptr);
}

LUA_INTEGER SICharacter::GetColliderID(std::string Name) const
{
	if (auto Collider = Character->GetCollider(Name))
	{
		return static_cast<LUA_INTEGER>(Collider->GetID());
	}
	else
	{
		throw(Pipanikos("Could not find collider " + Name, __FILE__, __LINE__));
	}
}

void SICharacter::Translate(LUA_NUMBER x, LUA_NUMBER y)
{
	float dt = GameCoordinator.GetDeltaTime();
	Character->Translate(x * dt, y * dt);
}

void SICharacter::Rotate(LUA_NUMBER Angle)
{
	float dt = GameCoordinator.GetDeltaTime();
	Character->Rotate(Angle * dt);
}

void SICharacter::SetPosition(LUA_NUMBER NewX, LUA_NUMBER NewY)
{
	Character->SetPosition(NewX, NewY);
}

void SICharacter::SetRotation(LUA_NUMBER NewRotation)
{
	Character->SetRotation(NewRotation);
}

void SICharacter::SetOrientation(LUA_NUMBER OrientX, LUA_NUMBER OrientY)
{
	Character->SetOrientation(OrientX, OrientY);
}

void SICharacter::SetProperty(std::string PropKey, std::string Value)
{
	Character->SetProperty(PropKey, Value);
}

void SICharacter::Enable()
{
	Character->Enable();
}

void SICharacter::Disable()
{
	Character->Disable();
}

