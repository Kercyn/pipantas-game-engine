#pragma once

/*! \file */

#include <map>
#include <string>

#include <boost/optional.hpp>

/*! \brief Encapsulates object properties so that they can be manipulated in a safe way.
std::optional or even std::experimental::optional have not yet been implemented in VS2015.
This class uses Boost's implementation that can be found here http://www.boost.org/doc/libs/1_61_0/libs/optional/doc/html/index.html
*/
class CProperties final
{
	public:
		CProperties() = default;
		~CProperties() = default;

		void AddProperty(std::string Key, std::string Value);
		boost::optional<std::string> GetProperty(std::string Key) const;
		const std::map<std::string, std::string>& GetAllProperties() const;

	private:
		std::map<std::string, std::string> Properties;
};
