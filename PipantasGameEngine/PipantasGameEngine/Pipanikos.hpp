#pragma once

/*! \file */

#include <exception>
#include <string>

class Pipanikos final : public std::exception
{
	public:
		explicit Pipanikos(const char* const Message_, std::string File, int Line) throw();
		explicit Pipanikos(const std::string& Message_, std::string File, int Line) throw();
		~Pipanikos() throw() override = default;

		const char* what() const override;

	private:
		std::string Message;
};
