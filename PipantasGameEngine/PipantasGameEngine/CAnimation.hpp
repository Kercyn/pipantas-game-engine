#pragma once

/*! \file */

#include "SAABB.hpp"

#include <string>
#include <map>
#include <cstdint>

class MResourceLoader;
class AActor;
class ACharacter;

/*! \brief Class that holds information for a single animation.
*/
class CAnimation
{
	public:
		CAnimation();
		~CAnimation() = default;

		void Play();

		std::string GetKey() const;
		int GetTotalFrames() const;
		float GetTimePerFrame() const;
		int GetCurrentFrame() const;
		float GetNormFrameWidth() const;
		
		static const int INFINITE_LOOP = -1;

		friend MResourceLoader;
		friend AActor;
		friend ACharacter;

	private:
		/*! \brief The animation's identifier.
		Animations are placed in a map, but we need to store the key for debugging purposes
		(to know which animation is currently playing etc...)*/
		std::string Key;

		//! The ID of the character this animation belongs to.
		std::string CharacterID;

		//! How many frames the animation consists of.
		std::uint8_t TotalFrames;

		//! How much time each animation frame playes. 1 / animation FPS
		float TimePerFrame;

		//! The index of the current frame of the animation.
		std::uint8_t CurrentFrame;

		//! The normalized width of an animation frame. Used to find a frame's position/width in the animation texture.
		float NormFrameWidth;

		//! How much time the animation is playing the current frame.
		float TimeOnThisFrame;

		//! How many times the animation should loop. Set to INFINITE_LOOP to play indefinitely.
		int Loop;

		//! How many times the animation has looped already.
		std::uint8_t TimesLooped;

		//! If true, the animation will start playing from the last frame to the first.
		bool bPlayInReverse;

		//! If true, the animation will go to its initial frame the next time it's played.
		bool bResetFrames;

		std::map<std::uint8_t, std::string> Triggers;
};
