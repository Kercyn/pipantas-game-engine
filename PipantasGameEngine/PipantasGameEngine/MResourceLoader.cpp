#include "MResourceLoader.hpp"
#include "SDL_ttf.h"
#include "SDL_image.h"
#include "Managers.hpp"
#include "rapidxml.hpp"
#include "AText.hpp"
#include "Pipanikos.hpp"
#include "ErrorHandling.hpp"

#include <algorithm>

using RWopsPtr = std::unique_ptr<SDL_RWops, decltype(&SDL_FreeRW)>;
using SurfacePtr = std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)>;
using FontPtr = std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)>;
using MusicPtr = std::unique_ptr<Mix_Music, decltype(&Mix_FreeMusic)>;
using ChunkPtr = std::unique_ptr<Mix_Chunk, decltype(&Mix_FreeChunk)>;

void MResourceLoader::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	if (TTF_Init() < 0)
	{
		throw(Pipanikos(TTF_GetError(), __FILE__, __LINE__));
	}
	else
	{
		Logger.Log(ELogLevel::Info, "MResourceLoader initialized.");
	}

	PopulateResourcePaths();
}

void MResourceLoader::Shutdown(const KeyGameCoordinator&)
{
	if (TTF_WasInit() != 0)
	{
		ResourceStore.Strings.clear();
		
		while (ResourceStore.Textures.size() > 0)
		{
			UnloadTexture(ResourceStore.Textures.begin()->first);
		}

		while (ResourceStore.Chunks.size() > 0)
		{
			UnloadChunk(ResourceStore.Chunks.begin()->first);
		}

		while (ResourceStore.MusicFiles.size() > 0)
		{
			UnloadMusic(ResourceStore.MusicFiles.begin()->first);
		}

		while (ResourceStore.Fonts.size() > 0)
		{
			UnloadFont(ResourceStore.Fonts.begin()->first);
		}

		ResourceStore.FontData.clear();
		ResourceStore.Shaders.clear();

		TTF_Quit();
		Logger.Log(ELogLevel::Info, "MResourceLoader shutdown.");
	}
	else
	{
		Logger.Log(ELogLevel::Warning, "SDL_TTF has not been initialized.");
	}
}

void MResourceLoader::LoadShaders(const KeyGameCoordinator&) const
{
	BShader TempVert;
	BShader TempFrag;

	TempVert.Load(ResourceStore.Keys.SHADER_PATH + ResourceStore.Keys.SHAPE_VERT_SH, GL_VERTEX_SHADER);
	TempFrag.Load(ResourceStore.Keys.SHADER_PATH + ResourceStore.Keys.SHAPE_FRAG_SH, GL_FRAGMENT_SHADER);

	ResourceStore.Shaders[ResourceStore.Keys.SHADER_PROG_SHAPE].Create();
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_SHAPE).AddShader(TempVert);
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_SHAPE).AddShader(TempFrag);
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_SHAPE).Link();

	TempVert.Load(ResourceStore.Keys.SHADER_PATH + ResourceStore.Keys.MAP_VERT_SH, GL_VERTEX_SHADER);
	TempFrag.Load(ResourceStore.Keys.SHADER_PATH + ResourceStore.Keys.MAP_FRAG_SH, GL_FRAGMENT_SHADER);

	ResourceStore.Shaders[ResourceStore.Keys.SHADER_PROG_MAP].Create();
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_MAP).AddShader(TempVert);
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_MAP).AddShader(TempFrag);
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_MAP).Link();

	TempVert.Load(ResourceStore.Keys.SHADER_PATH + ResourceStore.Keys.ACTOR_VERT_SH, GL_VERTEX_SHADER);
	TempFrag.Load(ResourceStore.Keys.SHADER_PATH + ResourceStore.Keys.ACTOR_FRAG_SH, GL_FRAGMENT_SHADER);

	ResourceStore.Shaders[ResourceStore.Keys.SHADER_PROG_ACTOR].Create();
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_ACTOR).AddShader(TempVert);
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_ACTOR).AddShader(TempFrag);
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_ACTOR).Link();

	TempVert.Load(ResourceStore.Keys.SHADER_PATH + ResourceStore.Keys.PARALLAX_VERT_SH, GL_VERTEX_SHADER);
	TempFrag.Load(ResourceStore.Keys.SHADER_PATH + ResourceStore.Keys.PARALLAX_FRAG_SH, GL_FRAGMENT_SHADER);

	ResourceStore.Shaders[ResourceStore.Keys.SHADER_PROG_PARALLAX].Create();
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_PARALLAX).AddShader(TempVert);
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_PARALLAX).AddShader(TempFrag);
	ResourceStore.Shaders.at(ResourceStore.Keys.SHADER_PROG_PARALLAX).Link();
}

void MResourceLoader::UnloadMusic(std::string ID) const
{
	ResourceStore.MusicFiles.erase(ID);
	ResourceStore.LoadedResources.erase(ID);
}

void MResourceLoader::UnloadChunk(std::string ID) const
{
	ResourceStore.Chunks.erase(ID);
	ResourceStore.LoadedResources.erase(ID);
}

void MResourceLoader::UnloadTexture(std::string ID) const
{
	GL_CALL(glDeleteTextures(1, &ResourceStore.Textures.at(ID).ID));
	ResourceStore.Textures.erase(ID);
	ResourceStore.LoadedResources.erase(ID);
}

void MResourceLoader::UnloadFont(std::string ID) const
{
	ResourceStore.Fonts.erase(ID);
	ResourceStore.LoadedResources.erase(ID);
}

void MResourceLoader::PopulateResourcePaths()
{
	using namespace rapidxml;

	// Add pre-defined keys and paths to the Strings collection.
	LoadString(ResourceStore.Keys.GAMEDATA_FILE_KEY, ResourceStore.Keys.GAMEDATA_FILE_PATH);
	LoadString(ResourceStore.Keys.PRELOADS_FILE_KEY, ResourceStore.Keys.PRELOADS_FILE_PATH);

	std::string GameDataRaw = ResourceStore.RequestString(ResourceStore.Keys.GAMEDATA_FILE_KEY);
	std::vector<TCHAR> GameData(GameDataRaw.c_str(), GameDataRaw.c_str() + GameDataRaw.size() + 1);

	xml_document<> XMLDoc;
	XMLDoc.parse<0>(GameData.data());

	auto GameNode = XMLDoc.first_node("game");
	if (GameNode == nullptr)
	{
		throw(Pipanikos("Game node not found on " + ResourceStore.Keys.GAMEDATA_FILE_PATH, __FILE__, __LINE__));
	}

	auto AssetsNode = GameNode->first_node("assets");

	auto MapsNode = AssetsNode->first_node("maps");
	if (MapsNode == nullptr)
	{
		throw(Pipanikos("Maps node not found on " + ResourceStore.Keys.GAMEDATA_FILE_PATH, __FILE__, __LINE__));
	}

	auto MapNode = MapsNode->first_node("map");

	// Maps
	while (MapNode != nullptr)
	{
		std::string Key = MapNode->first_attribute("key")->value();
		std::string Path = MapNode->first_attribute("path")->value();

		ResourcePaths.emplace(Key, ResourceStore.Keys.MAP_PATH + Path);

		MapNode = MapNode->next_sibling("map");
	}

	if (auto SoundsNode = AssetsNode->first_node("sounds"))
	{
		// Music
		if (auto MusicNode = SoundsNode->first_node("music"))
		{
			auto SoundNode = MusicNode->first_node("sound");

			while (SoundNode)
			{
				std::string Key = SoundNode->first_attribute("key")->value();
				std::string Path = SoundNode->first_attribute("path")->value();

				ResourcePaths.emplace(Key, ResourceStore.Keys.MUSIC_PATH + Path);

				SoundNode = SoundNode->next_sibling("sound");
			}
		}

		// SFX
		if (auto SFXNode = SoundsNode->first_node("sfx"))
		{
			auto SoundNode = SFXNode->first_node("sound");

			while (SoundNode)
			{
				std::string Key = SoundNode->first_attribute("key")->value();
				std::string Path = SoundNode->first_attribute("path")->value();

				ResourcePaths.emplace(Key, ResourceStore.Keys.SFX_PATH + Path);

				SoundNode = SoundNode->next_sibling("sound");
			}
		}
	}

	// Fonts
	if (auto FontsNode = AssetsNode->first_node("fonts"))
	{
		auto FontNode = FontsNode->first_node("font");

		while (FontNode)
		{
			std::string Key = FontNode->first_attribute("key")->value();
			std::string Path = FontNode->first_attribute("path")->value();

			ResourcePaths.emplace(Key, ResourceStore.Keys.FONT_PATH + Path);

			FontNode = FontNode->next_sibling();
		}
	}

	// Actors
	auto ActorNode = GameNode->first_node("actors")->first_node("actor");

	while (ActorNode != nullptr)
	{
		// Load the actor's properties file. If the actor is a character, proceed to loading other assets as well.

		std::string ActorKey = ActorNode->first_attribute("key")->value();
		std::string ActorType = ActorNode->first_attribute("type")->value();
		std::string ActorPath = ActorNode->first_attribute("path")->value();

		ActorPath += "/";

		ResourcePaths.emplace(ActorKey, ActorPath);

		std::string ActorPropFileKey = ActorKey + ResourceStore.Keys.ACTOR_PROPERTIES_SUFFIX;
		std::string ActorPropFilePath = ResourceStore.Keys.ACTOR_PATH + ActorPath + ResourceStore.Keys.ACTOR_PROPERTIES_FILE;

		LoadString(ActorPropFileKey, ActorPropFilePath);

		if (ActorType == "character")
		{
			std::string ActorPropertiesRaw = ResourceStore.RequestString(ActorPropFileKey);
			std::vector<TCHAR> ActorProperties = std::vector<TCHAR>(ActorPropertiesRaw.c_str(), ActorPropertiesRaw.c_str() + ActorPropertiesRaw.length() + 1);

			xml_document<> XMLDoc;
			XMLDoc.parse<0>(ActorProperties.data());

			auto AnimationNode = XMLDoc.first_node("actor")->first_node("animations")->first_node("animation");

			while (AnimationNode != nullptr)
			{
				std::string AnimKey = ActorKey + "_" + AnimationNode->first_attribute("key")->value();
				std::string AnimPath = ResourceStore.Keys.ACTOR_PATH + ActorPath + ResourceStore.Keys.ACTOR_ANIM_PATH + AnimationNode->first_attribute("file")->value();

				ResourcePaths.emplace(AnimKey, AnimPath);

				AnimationNode = AnimationNode->next_sibling();
			}
		}

		ActorNode = ActorNode->next_sibling();
	}

	// Parallaxes
	auto ParallaxesNode = GameNode->first_node("assets")->first_node("parallaxes");
	if (ParallaxesNode != nullptr)
	{
		auto ParallaxNode = ParallaxesNode->first_node("parallax");
		while (ParallaxNode != nullptr)
		{
			if (auto KeyAttr = ParallaxNode->first_attribute("key"))
			{
				if (auto PathAttr = ParallaxNode->first_attribute("path"))
				{
					ResourcePaths.emplace(KeyAttr->value(), ResourceStore.Keys.PARALLAX_PATH + PathAttr->value());
				}
				else
				{
					throw(Pipanikos("Path attribute not found for parallax " + std::string(KeyAttr->value()) + ".", __FILE__, __LINE__));
				}
			}
			else
			{
				throw(Pipanikos("Key attribute not found for parallax.", __FILE__, __LINE__));
			}

			ParallaxNode = ParallaxNode->next_sibling();
		}
	}

	// Asset paths for map-related assets (tileset textures, map sounds, NPC/enemy assets etc) will be added on CMap::Load.
}

void MResourceLoader::LoadDefaultResources(const KeyGameCoordinator&) const
{
	SurfacePtr Surface(IMG_Load(ResourceStore.Keys.DEFAULT_TEXTURE_PATH.c_str()), SDL_FreeSurface);
	CreateTextureFromSurface(ResourceStore.Keys.DEFAULT_TEXTURE_KEY, Surface.get());

	ResourceStore.FontData.emplace(ResourceStore.Keys.DEFAULT_FONT_DATA_KEY, BTTFFontData());
	auto& FD = ResourceStore.FontData.at(ResourceStore.Keys.DEFAULT_FONT_DATA_KEY);

	FD.Key = ResourceStore.Keys.DEFAULT_FONT_DATA_KEY;
	FD.Rendering = EFontRendering::Solid;
	FD.Attributes = TTF_STYLE_NORMAL;
	FD.bKerning = false;
	SDL_Color Color;
	Color.r = Color.g = Color.b = 0u;
	Color.a = 255u;
	FD.Color = Color;

	ResourceStore.Fonts.emplace(ResourceStore.Keys.DEFAULT_FONT_KEY, BTTFFont(FontPtr(nullptr, nullptr), RWopsPtr(nullptr, nullptr), ResourceStore.Keys.DEFAULT_FONT_KEY, 32));
	auto& Font = ResourceStore.Fonts.at(ResourceStore.Keys.DEFAULT_FONT_KEY);
	ResourceStore.LoadedResources.emplace(ResourceStore.Keys.DEFAULT_FONT_KEY);

	FontPtr FontRaw(TTF_OpenFont(ResourceStore.Keys.DEFAULT_FONT_PATH.c_str(), 32), TTF_CloseFont);
	if (FontRaw == nullptr)
	{
		throw(Pipanikos("Null font pointer for " + ResourceStore.Keys.DEFAULT_FONT_KEY + ": " + std::string(TTF_GetError()), __FILE__, __LINE__));
	}
	else
	{
		Font.FontData = std::vector<PHYSFS_sint64>();
		Font.RWops = nullptr;
		Font.Font = std::move(FontRaw);
		ResourceStore.LoadedResources.emplace(ResourceStore.Keys.DEFAULT_FONT_DATA_KEY);
	}

	
	MusicPtr Music(Mix_LoadMUS(ResourceStore.Keys.DEFAULT_MUSIC_PATH.c_str()), Mix_FreeMusic);
	RWopsPtr RWops(nullptr, SDL_FreeRW);
	ResourceStore.MusicFiles.emplace(ResourceStore.Keys.DEFAULT_MUSIC_KEY, BMixMusic(std::move(Music), std::move(RWops)));
	ResourceStore.LoadedResources.emplace(ResourceStore.Keys.DEFAULT_MUSIC_KEY);

	ChunkPtr Chunk(Mix_LoadWAV(ResourceStore.Keys.DEFAULT_SFX_PATH.c_str()), Mix_FreeChunk);
	ResourceStore.Chunks.emplace(ResourceStore.Keys.DEFAULT_SFX_KEY, BMixChunk(std::move(Chunk)));
	ResourceStore.LoadedResources.emplace(ResourceStore.Keys.DEFAULT_SFX_KEY);
}

void MResourceLoader::CreateTextureFromSurface(std::string ID, SDL_Surface* Surface) const
{
	if (Surface == nullptr)
	{
		throw(Pipanikos("Given surface is null for texture " + ID + std::string("\n\t") + std::string(IMG_GetError()), __FILE__, __LINE__));
	}
	else
	{
		GLuint TextureID;
		GL_CALL(glGenTextures(1, &TextureID));
		GL_CALL(glBindTexture(GL_TEXTURE_2D, TextureID));
		GLenum Mode = (Surface->format->BytesPerPixel == 4) ? GL_RGBA : GL_RGB;

		GL_CALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
		GL_CALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
		GL_CALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
		GL_CALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));

		GL_CALL(glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, Surface->w, Surface->h));
		GL_CALL(glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, Surface->w, Surface->h, Mode, GL_UNSIGNED_BYTE, Surface->pixels));

		// Storing texture to cache
		ResourceStore.Textures.emplace(ID, BTexture());
		ResourceStore.Textures[ID] = BTexture(TextureID, Surface->h, Surface->w);

		ResourceStore.LoadedResources.emplace(ID);
	}
}

void MResourceLoader::LoadSceneResources(const std::set<std::string>& RequiredResources) const
{
	using namespace MapElements;

	for (const auto& ResourceID : RequiredResources)
	{
		// Check the key prefix to figure which type of resource it is
		const auto Prefix = ResourceID.substr(0, 3);

		if (Prefix == ResourceStore.Keys.RES_PREFIX_MUSIC)
		{
			LoadMusic(ResourceID);
		}
		else if (Prefix == ResourceStore.Keys.RES_PREFIX_SFX)
		{
			LoadChunk(ResourceID);
		}
		else if (Prefix == ResourceStore.Keys.RES_PREFIX_FONT)
		{
			LoadFont(ResourceID);
		}
		else if (Prefix == ResourceStore.Keys.RES_PREFIX_FONTDATA)
		{
			LoadFontData(ResourceID);
		}
		else if (Prefix == ResourceStore.Keys.RES_PREFIX_TEXT)
		{
			std::string ActorID = ResourceID.substr(3, ResourceID.size());
			auto Actor = ActorHandler.GetActor(ActorID);

			if (Actor->GetActorType() == EActorType::Text)
			{
				auto TextActor = static_cast<AText*>(Actor);
				TextActor->UpdateTexture();
			}
			else
			{
				throw(Pipanikos(ActorID + " is not a valid text actor ID.", __FILE__, __LINE__));
			}
		}
		else
		{
			// Couldn't find an appropriate prefix, so it must be a parallax or a tileset, and they're both textures!
			LoadTexture(ResourceID);
		}
	}
}

void MResourceLoader::UnloadUnusedResources(const std::set<std::string>& RequiredResources) const
{
	const auto& LoadedResources = ResourceStore.GetLoadedResources();
	std::set<std::string> UnusedResources;

	std::set_difference(LoadedResources.begin(), LoadedResources.end(), RequiredResources.begin(), RequiredResources.end(), std::inserter(UnusedResources, UnusedResources.begin()));
	
	for (const auto& ResourceID : UnusedResources)
	{
		// We want the default assets to stay loaded at all times.
		if ((ResourceID != ResourceStore.Keys.DEFAULT_FONT_DATA_KEY)
			&& (ResourceID != ResourceStore.Keys.DEFAULT_FONT_KEY)
			&& (ResourceID != ResourceStore.Keys.DEFAULT_TEXTURE_KEY)
			&& (ResourceID != ResourceStore.Keys.DEFAULT_MUSIC_KEY)
			&& (ResourceID != ResourceStore.Keys.DEFAULT_SFX_KEY))
		{
			const auto Prefix = ResourceID.substr(0, 3);

			if (Prefix == ResourceStore.Keys.RES_PREFIX_MUSIC)
			{
				UnloadMusic(ResourceID);
			}
			else if (Prefix == ResourceStore.Keys.RES_PREFIX_SFX)
			{
				UnloadChunk(ResourceID);
			}
			else if (Prefix == ResourceStore.Keys.RES_PREFIX_FONT)
			{
				UnloadFont(ResourceID);
			}
			else if (Prefix == ResourceStore.Keys.RES_PREFIX_FONTDATA)
			{
				// Do nothing; the FontData container will be emptied anyway.
			}
			else
			{
				UnloadTexture(ResourceID);
			}
		}
	}

	ResourceStore.FontData.clear();
}

std::string MResourceLoader::GetFileContents(std::string Path) const
{
	if (PhysFS.mount(ResourceStore.Keys.GAME_FILE) == 0)
	{
		throw(Pipanikos("Failed to mount " + ResourceStore.Keys.GAME_FILE + ": " + PHYSFS_getLastError(), __FILE__, __LINE__));
	}

	std::vector<TCHAR> FileData;

	BPhysFSFilePtr File(Path);
	PHYSFS_sint64 FileLength = PhysFS.fileLength(File);

	PhysFS.read<TCHAR>(File, FileData, 1, FileLength);

	return std::string(FileData.begin(), FileData.end());
}

std::string MResourceLoader::GetRawMap(std::string ID) const
{
	std::string MapContents;

	if (ResourcePaths.count(ID) == 0)
	{
		throw(Pipanikos("Map path not found for " + ID, __FILE__, __LINE__));
	}
	else
	{
		std::string Path = ResourcePaths.at(ID);

		if (PhysFS.exists(Path) != 0)
		{
			BPhysFSFilePtr File(Path);

			PHYSFS_sint64 MapSize = PhysFS.fileLength(File);
			std::vector<TCHAR> MapData(MapSize);

			PhysFS.read<TCHAR>(File, MapData, 1, MapSize);

			MapContents = std::string(MapData.begin(), MapData.end());
		}
		else
		{
			throw(Pipanikos(Path + " does not exist", __FILE__, __LINE__));
		}

		return MapContents;
	}
}

void MResourceLoader::AddResourcePath(std::string Key, std::string Path)
{
	ResourcePaths.emplace(Key, Path);
}

void MResourceLoader::LoadTexture(std::string ID) const
{
	if (ResourcePaths.count(ID) == 0)
	{
		Logger.Log(ELogLevel::Warning, "Texture path not found for " + ID);
	}
	else
	{
		std::string Path = ResourcePaths.at(ID);

		if (PhysFS.exists(Path) != 0)
		{
			BPhysFSFilePtr File(Path);

			PHYSFS_sint64 ImageSize = PhysFS.fileLength(File);

			std::vector<PHYSFS_sint64> ImageData(ImageSize);
			PhysFS.read<PHYSFS_sint64>(File, ImageData, 1, ImageSize);

			// Create an SDL surface from an image
			RWopsPtr RW(SDL_RWFromMem(ImageData.data(), ImageSize), SDL_FreeRW);
			SurfacePtr Surface(IMG_Load_RW(RW.get(), 0), SDL_FreeSurface);

			// Create GL texture from surface
			CreateTextureFromSurface(ID, Surface.get());
		}
		else
		{
			Logger.Log(ELogLevel::Warning, Path + " does not exist for ID " + ID);
		}
	}
}

void MResourceLoader::LoadString(std::string ID, std::string Path) const
{
	if (PhysFS.mount(ResourceStore.Keys.GAME_FILE) == 0)
	{
		throw(Pipanikos("Failed to mount " + ResourceStore.Keys.GAME_FILE + "\n" + PHYSFS_getLastError(), __FILE__, __LINE__));
	}

	std::vector<TCHAR> FileData;

	BPhysFSFilePtr File(Path);
	PHYSFS_sint64 FileLength = PhysFS.fileLength(File);

	PhysFS.read<TCHAR>(File, FileData, 1, FileLength);

	ResourceStore.Strings.emplace(ID, std::string(FileData.data()));
}

void MResourceLoader::LoadFont(std::string ID) const
{
	int Size;

	try
	{
		Size = std::stoi(ID.substr(ID.find_last_of('_') + 1, ID.size()));
	}
	catch (const std::logic_error& e)
	{
		Logger.Log(ELogLevel::Warning, std::string(e.what()) + ": " + ID.substr(ID.find_last_of('_') + 1, ID.size()));
	}

	auto FontPathID = ID.substr(0, ID.find_last_of('_'));

	if (ResourcePaths.count(FontPathID) == 0)
	{
		Logger.Log(ELogLevel::Warning, "Font path not found for " + FontPathID);
	}
	else
	{
		std::string Path = ResourcePaths.at(FontPathID);

		if (PhysFS.exists(Path) != 0)
		{
			BPhysFSFilePtr File(Path);

			PHYSFS_sint64 FontFileSize = PhysFS.fileLength(File);

			ResourceStore.Fonts.emplace(ID, BTTFFont(FontPtr(nullptr, nullptr), RWopsPtr(nullptr, nullptr), ID, Size));
			auto& Font = ResourceStore.Fonts.at(ID);

			Font.FontData = std::vector<PHYSFS_sint64>(FontFileSize);

			PhysFS.read<PHYSFS_sint64>(File, Font.FontData, 1, FontFileSize);

			RWopsPtr RWops(SDL_RWFromMem(Font.FontData.data(), FontFileSize), SDL_FreeRW);

			if (RWops == nullptr)
			{
				Logger.Log(ELogLevel::Warning, "Null RW pointer for " + ID + ": " + std::string(SDL_GetError()));
			}
			else
			{
				FontPtr FontRaw = FontPtr(TTF_OpenFontRW(RWops.get(), 0, Size), TTF_CloseFont);

				if (FontRaw == nullptr)
				{
					Logger.Log(ELogLevel::Warning, "Null font pointer for " + ID + ": " + std::string(TTF_GetError()));
				}
				else
				{
					Font.Font = std::move(FontRaw);
					Font.RWops = std::move(RWops);

					ResourceStore.LoadedResources.emplace(ID);
				}
			}
		}
		else
		{
			Logger.Log(ELogLevel::Warning, Path + " does not exist for ID " + ID);
		}
	}
}

void MResourceLoader::LoadFontData(std::string ID) const
{
	auto Parts = Utils::Split(ID, '_');

	// Remove the prefix
	Parts.erase(Parts.begin());

	ResourceStore.FontData.emplace(ID, BTTFFontData());
	auto& FD = ResourceStore.FontData.at(ID);

	FD.Key = ID;

	std::string Rendering = Parts.at(0);

	if (Rendering == "blended")
	{
		FD.Rendering = EFontRendering::Blended;
	}
	else if (Rendering == "solid")
	{
		FD.Rendering = EFontRendering::Solid;
	}
	// No need for further checks; we've already done all checks needed at CMap::GetTextData.

	std::string AttributesRaw = Parts.at(1);
	int Attributes = TTF_STYLE_NORMAL;
	Attributes |= (std::count(AttributesRaw.begin(), AttributesRaw.begin(), 'b')) ? 0x00 : TTF_STYLE_BOLD;
	Attributes |= (std::count(AttributesRaw.begin(), AttributesRaw.begin(), 'i')) ? 0x00 : TTF_STYLE_ITALIC;
	Attributes |= (std::count(AttributesRaw.begin(), AttributesRaw.begin(), 's')) ? 0x00 : TTF_STYLE_STRIKETHROUGH;
	Attributes |= (std::count(AttributesRaw.begin(), AttributesRaw.begin(), 'u')) ? 0x00 : TTF_STYLE_UNDERLINE;
	bool bKerning = (std::count(AttributesRaw.begin(), AttributesRaw.begin(), 'k') == 1);

	FD.Attributes = Attributes;
	FD.bKerning = bKerning;

	SDL_Color Color;

	try
	{
		Color.r = std::stoi(Parts.at(2));
		Color.g = std::stoi(Parts.at(3));
		Color.b = std::stoi(Parts.at(4));
		Color.a = std::stoi(Parts.at(5));
	}
	catch (const std::logic_error& e)
	{
		Logger.Log(ELogLevel::Warning, "Could not parse colors for " + ID + ": " + std::string(e.what()));
		Color.r = Color.g = Color.b = 0u;
		Color.a = 255u;
	}

	FD.Color = Color;

	ResourceStore.LoadedResources.emplace(ID);
}

void MResourceLoader::LoadChunk(std::string ID) const
{
	using ChunkPtr = std::unique_ptr<Mix_Chunk, decltype(&Mix_FreeChunk)>;

	if (ResourcePaths.count(ID) == 0)
	{
		Logger.Log(ELogLevel::Warning, "Chunk path not found for " + ID);
	}
	else
	{
		std::string Path = ResourcePaths.at(ID);

		if (PhysFS.exists(Path) != 0)
		{
			BPhysFSFilePtr File(Path);

			PHYSFS_sint64 ChunkSize = PhysFS.fileLength(File);

			ResourceStore.Chunks.emplace(ID, BMixChunk(ChunkPtr(nullptr, nullptr)));
			auto& Chunk = ResourceStore.Chunks.at(ID);

			std::vector<PHYSFS_sint64> ChunkData(ChunkSize);
			PhysFS.read<PHYSFS_sint64>(File, ChunkData, 1, ChunkSize);

			// Create an SDL surface from an image
			RWopsPtr RW(SDL_RWFromMem(ChunkData.data(), ChunkSize), SDL_FreeRW);
			ChunkPtr ChunkRaw(Mix_LoadWAV_RW(RW.get(), 0), Mix_FreeChunk);

			if (!ChunkRaw)
			{
				Logger.Log(ELogLevel::Warning, "Failed to load " + ID + ": " + std::string(Mix_GetError()));
			}

			Chunk.Chunk = std::move(ChunkRaw);

			ResourceStore.LoadedResources.emplace(ID);
		}
		else
		{
			Logger.Log(ELogLevel::Warning, Path + " does not exist for ID " + ID);
		}
	}
}

void MResourceLoader::LoadMusic(std::string ID) const
{
	using MusicPtr = std::unique_ptr<Mix_Music, decltype(&Mix_FreeMusic)>;

	if (ResourcePaths.count(ID) == 0)
	{
		Logger.Log(ELogLevel::Warning, "Music path not found for " + ID);
	}
	else
	{
		using MusicPtr = std::unique_ptr<Mix_Music, decltype(&Mix_FreeMusic)>;

		std::string Path = ResourcePaths.at(ID);

		if (PhysFS.exists(Path) != 0)
		{
			BPhysFSFilePtr File(Path);

			PHYSFS_sint64 MusicSize = PhysFS.fileLength(File);

			ResourceStore.MusicFiles.emplace(ID, BMixMusic(MusicPtr(nullptr, nullptr), RWopsPtr(nullptr, nullptr)));
			auto& Music = ResourceStore.MusicFiles.at(ID);

			Music.MusicData = std::vector<PHYSFS_sint64>(MusicSize);

			PhysFS.read<PHYSFS_sint64>(File, Music.MusicData, 1, MusicSize);

			RWopsPtr RWops(SDL_RWFromMem(Music.MusicData.data(), MusicSize), SDL_FreeRW);

			if (RWops == nullptr)
			{
				Logger.Log(ELogLevel::Warning, Mix_GetError());
			}
			else
			{
				MusicPtr MusicRaw(Mix_LoadMUS_RW(RWops.get(), 0), Mix_FreeMusic);

				if (MusicRaw == nullptr)
				{
					Logger.Log(ELogLevel::Warning, "Failed to load " + ID + ": " + std::string(Mix_GetError()));
				}
				else
				{
					Music.Music = std::move(MusicRaw);
					Music.RWops = std::move(RWops);

					ResourceStore.LoadedResources.emplace(ID);
				}
			}
		}
		else
		{
			Logger.Log(ELogLevel::Warning, Path + " does not exist for ID " + ID);
		}
	}
}
