#include "BTexture.hpp"

#include <memory>

BTexture::BTexture()
	: ID(0), Height(0), Width(0)
{
}

BTexture::BTexture(GLuint ID_, std::uint32_t Height_, std::uint32_t Width_)
	: ID(ID_), Height(Height_), Width(Width_)
{
}

BTexture& BTexture::operator=(const BTexture& Other)
{
	if (*this != Other)
	{
		ID = Other.ID;
		Height = Other.Height;
		Width = Other.Width;
	}

	return *this;
}

BTexture::operator GLuint() const
{
	return ID;
}
