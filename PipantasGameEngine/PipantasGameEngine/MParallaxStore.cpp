#include "MParallaxStore.hpp"
#include "Managers.hpp"
#include "Utils.hpp"
#include "Pipanikos.hpp"

void MParallaxStore::Initialize(const KeyGameCoordinator &, const std::vector<std::string>&)
{
	Logger.Log(ELogLevel::Info, "MParallaxStore initialized.");
}

void MParallaxStore::Shutdown(const KeyGameCoordinator &)
{
	Logger.Log(ELogLevel::Info, "MParallaxStore shutdown.");
}

void MParallaxStore::Reset()
{
	CurrentGroup.clear();
}

const std::map<std::string, CParallax>& MParallaxStore::GetCurrentGroup() const
{
	return CurrentGroup;
}

CParallax& MParallaxStore::GetParallax(std::string Key)
{
	if (CurrentGroup.count(Key) > 0)
	{
		return CurrentGroup.at(Key);
	}
	else
	{ 
		throw(Pipanikos("Could not find parallax with key " + Key, __FILE__, __LINE__));
	}
}

std::map<std::string, CParallax>& MParallaxStore::GetCurrentGroup(const KeyParallaxLogic&)
{
	return CurrentGroup;
}

void MParallaxStore::UpdateParallaxesForScene()
{
	const auto& ImageLayers = SceneSwitcher.GetCurrentMap()->GetImageLayers();

	for (const auto& ImageLayer : ImageLayers)
	{
		CParallax TempParallax;

		// We know the key property exists (CMap would have thrown an error if it didn't find them),
		// so we can use its operator* without checking if it exists.
		std::string Key = *ImageLayer.Properties.GetProperty(ActorMapProperties::KEY);

		TempParallax.Size = glm::vec2(ImageLayer.Width, ImageLayer.Height);

		auto SpeedAttr = ImageLayer.Properties.GetProperty("Speed");
		auto OffsetXAttr = ImageLayer.Properties.GetProperty("OffsetX");
		auto OffsetYAttr = ImageLayer.Properties.GetProperty("OffsetY");
		auto RenderFrontAttr = ImageLayer.Properties.GetProperty("RenderInFront");
		auto EnabledAttr = ImageLayer.Properties.GetProperty("Enabled");

		TempParallax.Speed = (SpeedAttr) ? std::stof(*SpeedAttr) : 0.0f;
		TempParallax.Offset.x = (OffsetXAttr) ? std::stof(*OffsetXAttr) : 0.0f;
		TempParallax.Offset.y = (OffsetYAttr) ? std::stof(*OffsetYAttr) : 0.0f;
		TempParallax.bRenderFront = (RenderFrontAttr) ? Utils::ToBool(*RenderFrontAttr) : false;
		TempParallax.bIsEnabled = (EnabledAttr) ? Utils::ToBool(*EnabledAttr) : true;

		std::string TypeRaw = *ImageLayer.Properties.GetProperty("Type");

		if (TypeRaw == "bi_x")
		{
			TempParallax.Type = EParallaxType::Bi_X;
		}
		else if (TypeRaw == "bi_y")
		{
			TempParallax.Type = EParallaxType::Bi_Y;
		}
		else if (TypeRaw == "fixed")
		{
			TempParallax.Type = EParallaxType::Fixed;
		}
		else if (TypeRaw == "multi")
		{
			TempParallax.Type = EParallaxType::Multi;
		}
		else
		{
			throw(Pipanikos(TypeRaw + " is not an appropriate parallax type for " + Key + ".", __FILE__, __LINE__));
		}
		
		CurrentGroup.emplace(Key, TempParallax);
	}
}
