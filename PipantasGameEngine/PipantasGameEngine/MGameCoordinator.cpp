#include "MGameCoordinator.hpp"
#include "Managers.hpp"
#include "rapidxml.hpp"
#include "Defs.hpp"

#include <thread>
#include <chrono>
#include <numeric>

void MGameCoordinator::Initialize(const KeyGameCoordinator& Key, const std::vector<std::string>& Args)
{
	GameState = EGameState::Initializing;

	Logger.Initialize(Key, Args);
	PhysFS.Initialize(Key, Args);
	ResourceStore.Initialize(Key, Args);
	ResourceLoader.Initialize(Key, Args);

	using namespace rapidxml;

	std::string GameDataRaw = ResourceStore.RequestString(ResourceStore.Keys.GAMEDATA_FILE_KEY);
	std::vector<TCHAR> GameData(GameDataRaw.c_str(), GameDataRaw.c_str() + GameDataRaw.size() + 1);

	xml_document<> XMLDoc;
	XMLDoc.parse<0>(GameData.data());

	auto GameNode = XMLDoc.first_node("game");
	Pipantas::Defs::GAME_TITLE = GameNode->first_attribute("name")->value();
	Pipantas::Defs::GAME_VERSION = GameNode->first_attribute("version")->value();

	auto FixedDTNode = GameNode->first_node("physics")->first_attribute("fixed_dt");
	if (FixedDTNode)
	{
		FixedDeltaTime = std::stof(FixedDTNode->value()) / 100.0f;
	}
	else
	{
		Logger.Log(ELogLevel::Warning, "fixed_dt property not found while parsing physics node. Defaulting fixed delta time to 2.");
		FixedDeltaTime = 2 / 100.0f;
	}

	ActorHandler.Initialize(Key, Args);
	SceneSwitcher.Initialize(Key, Args);
	Renderer.Initialize(Key, Args);
	FileParser.Initialize(Key, Args);
	InputRegistrar.Initialize(Key, Args);
	CollisionEngine.Initialize(Key, Args);
	PairGenerator.Initialize(Key, Args);
	ScriptMiddleware.Initialize(Key, Args);
	AudioPlayback.Initialize(Key, Args);
	AudioController.Initialize(Key, Args);
	ParallaxStore.Initialize(Key, Args);
	ParallaxLogic.Initialize(Key, Args);

	ResourceLoader.LoadShaders({});
	ResourceLoader.LoadDefaultResources({});

	Logger.Log(ELogLevel::Trace, "Set fixed dt to " + std::to_string(FixedDeltaTime));

	SceneSwitcher.ChangeScene(SceneSwitcher.GetFirstSceneKey());
	Renderer.InitializeActorRenderData(ResourceStore.Keys.PLAYER_ACTOR_ID);

	Logger.Log(ELogLevel::Info, "MGameCoordinator initialized.");
}

void MGameCoordinator::Shutdown(const KeyGameCoordinator& Key)
{
	AudioController.Shutdown(Key);
	AudioPlayback.Shutdown(Key);
	ScriptMiddleware.Shutdown(Key);
	PairGenerator.Shutdown(Key);
	CollisionEngine.Shutdown(Key);
	InputRegistrar.Shutdown(Key);
	FileParser.Shutdown(Key);
	SceneSwitcher.Shutdown(Key);
	ActorHandler.Shutdown(Key);
	ResourceLoader.Shutdown(Key);
	ResourceStore.Shutdown(Key);
	PhysFS.Shutdown(Key);
	Renderer.Shutdown(Key);
	ParallaxStore.Shutdown(Key);
	ParallaxLogic.Shutdown(Key);
	Logger.Shutdown(Key);
}

void MGameCoordinator::Run(const std::vector<std::string>& Args)
{
	std::chrono::high_resolution_clock Clock;
	
	//�� ��� �� ����� ������ ���� � ��� ��� �� ��� ������ ������� �� ������� ��� ����� ���� ����� ��� � �������� ��� ����� �� ��� ���� ������� ��� ��� �� ���� ������ ����� ��� ��� ���� ������� ��� ����� ���� �������� ��� ����� ��� �� ���������� ��� �� ���� �� �� ���� ������ �� ��� ��� ����� �� ������ ��� ���� ���� ������� ��� ����� ������ �� ���������� ��� �� �� ���� ���� ������ ���� ���� ����.

	auto CurrentTime = Clock.now();
	Accumulator = 0.0f;

	while (GameState != EGameState::Exit)
	{
		auto NewTime = Clock.now();

		DeltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(NewTime - CurrentTime).count() / 1000.0f;
		if (DeltaTime > 0.25f)
		{
			DeltaTime = 0.25f;
		}

		CurrentTime = NewTime;

		Accumulator += DeltaTime;
		
		InputRegistrar.RegisterInput();

		//while (Accumulator >= FixedDeltaTime)
		{
			if (GameState == EGameState::Playing)
			{
				ScriptMiddleware.FixedUpdate();
				CollisionEngine.Tick();
				AudioController.Tick();
			}
			Accumulator -= FixedDeltaTime;
		}

		if (GameState == EGameState::Playing)
		{
			for (auto Actor : ActorHandler.GetAllActors())
			{
				if (Actor->GetActorType() == EActorType::Character)
				{
					auto Character = static_cast<ACharacter*>(Actor);
					Character->GetCurrentAnim().Play();
				}
			}

			ActorHandler.UpdateActorBodies();
			ScriptMiddleware.Update();
		}
		Renderer.Render();
	}
}

void MGameCoordinator::RequestExit()
{
	GameState = EGameState::Exit;
}

void MGameCoordinator::RequestLoading(const KeySceneSwitcher &)
{
	GameState = EGameState::Loading;
}

void MGameCoordinator::FinishedLoading(const KeySceneSwitcher &)
{
	GameState = EGameState::Playing;
}

void MGameCoordinator::WindowLostFocus()
{
	GameState = EGameState::Paused;
}

void MGameCoordinator::WindowGainedFocus()
{
	GameState = EGameState::Playing;
}

EGameState MGameCoordinator::GetGameState() const
{
	return GameState;
}

float MGameCoordinator::GetDeltaTime() const
{
	return DeltaTime;
}

float MGameCoordinator::GetFixedDeltaTime() const
{
	return FixedDeltaTime;
}
