#pragma once

/*! \file */

#include <string>
#include <map>

#include "CMap.hpp"

class MSceneSwitcher;

class CScene final
{
	public:
		CScene() = default;
		~CScene() = default;

		friend class MSceneSwitcher;

		const std::map<std::string, std::string>& GetProperties() const;
		std::string GetKey() const;

	private:
		// The scene's unique identifier.
		std::string Key;

		// The scene's level map.
		CMap LevelMap;

		// Various user-defined properties and their values (score, total deaths etc.). 
		std::map<std::string, std::string> Properties;

		/*! \brief Loads the scene's map.
		Parses scene data acquired from the resource manager and loads the needed maps.
		\param The name of the scene we want to load in the object, as defined in the gamedata file.
		*/
		void Load(std::string SceneName);
};
