#pragma once

/*! \file */

#include <string>
#include <map>
#include <set>
#include <vector>

#include "IManager.hpp"
#include "BMixChunk.hpp"
#include "BMixMusic.hpp"
#include "BShader.hpp"
#include "BShaderProgram.hpp"
#include "BTexture.hpp"
#include "BTTFFont.hpp"
#include "BTTFFontData.hpp"

class MResourceLoader;

//! Contains all the string keys the engine uses.
struct StringKeys
{
	StringKeys() = default;

	// Resource directories and files
	const std::string RESOURCES_DIR = "resources/";

	const std::string MUSIC_PATH = RESOURCES_DIR + "Music/";
	const std::string TILESET_PATH = RESOURCES_DIR + "Tilesets/";
	const std::string MAP_PATH = RESOURCES_DIR + "Maps/";
	const std::string SFX_PATH = RESOURCES_DIR + "SFX/";
	const std::string PARALLAX_PATH = RESOURCES_DIR + "Parallax/";
	const std::string SHADER_PATH = RESOURCES_DIR + "Shaders/";
	const std::string ACTOR_PATH = RESOURCES_DIR + "Actors/";
	const std::string ACTOR_ANIM_PATH = "Animations/";
	const std::string SCRIPT_PATH = RESOURCES_DIR + "Scripts/";
	const std::string FONT_PATH = RESOURCES_DIR + "Fonts/";

	const std::string GAME_FILE = "gamefiles.zip";
	const std::string GAMEDATA_FILE_PATH = "gamedata.xml";
	const std::string GAMEDATA_FILE_KEY = "GAMEDATA_FILE";
	const std::string ACTOR_PROPERTIES_FILE = "/properties.xml";
	const std::string PRELOADS_FILE_PATH = RESOURCES_DIR + "/preloads.xml";
	const std::string PRELOADS_FILE_KEY = "PRELOADS_FILE";
	const std::string SCRIPT_INIT_FILE_PATH = "init.lua";

	// Shaders
	const std::string SHAPE_VERT_SH = "shape_vert.glsl";
	const std::string SHAPE_FRAG_SH = "shape_frag.glsl";
	const std::string MAP_VERT_SH = "map_vert.glsl";
	const std::string MAP_FRAG_SH = "map_frag.glsl";
	const std::string PARALLAX_VERT_SH = "parallax_vert.glsl";
	const std::string PARALLAX_FRAG_SH = "parallax_frag.glsl";
	const std::string ACTOR_VERT_SH = "actor_vert.glsl";
	const std::string ACTOR_FRAG_SH = "actor_frag.glsl";

	// Shader programs
	const std::string SHADER_PROG_SHAPE = "SHAPE";
	const std::string SHADER_PROG_MAP = "MAP";
	const std::string SHADER_PROG_PARALLAX = "PARALLAX";
	const std::string SHADER_PROG_ACTOR = "ACTOR";

	// Buffer data
	const std::string RENDER_DATA_STATIC_COLLIDER = "STATIC_COLLIDER";
	const std::string RENDER_DATA_DYNAMIC_COLLIDER = "DYNAMIC_COLLIDER";
	const std::string RENDER_DATA_MAP_FRONT = "MAP_FRONT";
	const std::string RENDER_DATA_MAP_BACK = "MAP_BACK";
	const std::string RENDER_DATA_SAP = "SAP";

	// Default assets
	const std::string DEFAULT_TEXTURE_KEY = "PIPANTAS_DEFAULT_TEXTURE";
	const std::string DEFAULT_TEXTURE_PATH = "def_texture.png";
	const std::string DEFAULT_FONT_KEY = "PIPANTAS_DEFAULT_FONT";
	const std::string DEFAULT_FONT_PATH = "def_font.ttf";
	const std::string DEFAULT_FONT_DATA_KEY = "PIPANTAS_DEFAULT_FONT_DATA";
	const std::string DEFAULT_MUSIC_KEY = "PIPANTAS_DEFAULT_MUSIC";
	const std::string DEFAULT_MUSIC_PATH = "def_music.ogg";
	const std::string DEFAULT_SFX_KEY = "PIPANTAS_DEFAULT_SFX";
	const std::string DEFAULT_SFX_PATH = "def_sfx.wav";

	// Used to create the key the path to an actor's properties file.
	// For example, if you want to add the path to the player's properties file to the AssetPaths map,
	// you'd use PLAYER_ACTOR + ACTION_PROPERTIES_SUFFIX as a key.
	const std::string ACTOR_PROPERTIES_SUFFIX = "_PROP_FILE";
	const std::string PLAYER_ACTOR_ID = "Player";
	const std::string PLAYER_ACTOR_KEY = "PLAYER";
	const std::string CHARACTER_KEY = "character";

	// Resource prefixes
	const std::string RES_PREFIX_MUSIC = "MUS";
	const std::string RES_PREFIX_SFX = "SFX";
	const std::string RES_PREFIX_FONT = "FNT";
	const std::string RES_PREFIX_FONTDATA = "FND";
	const std::string RES_PREFIX_TEXT = "TFX";
};

class MResourceStore final : public IManager
{
	template <typename T>
	using ResourceCache = std::map<std::string, typename T>;
	using FontPtr = std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)>;
	using RWopsPtr = std::unique_ptr<SDL_RWops, decltype(&SDL_FreeRW)>;

	public:
		MResourceStore() = default;
		~MResourceStore() = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;
		
		const std::set<std::string>& GetLoadedResources() const;

		//! Returns the requested string. Throws an error if it fails.
		std::string RequestString(std::string ID) const;

		//! Returns the requested texture. Throws an error if it fails.
		const BShaderProgram& RequestShaderProgram(std::string ID) const;

		//! Returns the requested texture. If it fails, returns the engine's default texture.
		const BTexture& RequestTexture(std::string ID) const;
		
		//! Returns the requested font. If it fails, returns the engine's default font.
		BTTFFont& RequestFont(std::string ID);

		//! Returns the requested font data. If it fails, returns the engine's default font data.
		const BTTFFontData& RequestFontData(std::string ID) const;

		//! Returns the requested sound chunk. If it fails, returns the engine's default chunk, which is silent.
		BMixChunk& RequestChunk(std::string ID);

		//! Returns the requested music. If it fails, returns the engine's default music, which is silent.
		BMixMusic& RequestMusic(std::string ID);

		static StringKeys Keys;

		friend MResourceLoader;

	private:

		//! The IDs of all assets that are currently loaded.
		std::set<std::string> LoadedResources;

		//! Once loaded, strings stay loaded until the end of the game.
		ResourceCache<std::string> Strings;
				
		ResourceCache<BTexture> Textures;
		
		ResourceCache<BShaderProgram> Shaders;

		ResourceCache<BMixChunk> Chunks;

		ResourceCache<BMixMusic> MusicFiles;

		ResourceCache<BTTFFont> Fonts;

		ResourceCache<BTTFFontData> FontData;
};
