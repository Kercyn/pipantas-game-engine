#pragma once

/*! \file */

#include "IManager.hpp"

#include <string>
#include <map>

enum class ELogLevel : std::uint8_t
{
	Trace,
	Debug,
	Info,
	Warning,
	Error
};

class MLogger final : public IManager
{
	public:
		MLogger() ;
		~MLogger() = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>& Args) override;
		void Shutdown(const KeyGameCoordinator&) override;

		void Log(ELogLevel Level, std::string Message) const;

	private:
		//! No logging is performed for anything below this level.
		ELogLevel CurrentLoggingLevel;
		ELogLevel DefaultLoggingLevel;

		static const std::map<ELogLevel, std::string> LevelStringMap;
};
