#pragma once

/*! \file */

#include "PCollider.hpp"

#include <string>
#include <vector>
#include <map>
#include <cinttypes>

class MCollisionEngine;

class PTriggerCollider final : public PCollider
{
	public:
		bool IsTrigger() const override;

		void SetProperties(const std::map<std::string, std::string>& NewProperties);
		std::string GetProperty(std::string ID) const;

		friend MCollisionEngine;

	private:
		std::map<std::string, std::string> Properties;

		//! Vector that contains the colliders that are currently colliding with this one.
		std::vector<PCollider*> CurrCollisions;
	
		//! Vector that contains the IDs of the colliders that were colliding with this one in the previous physics tick.
		std::vector<std::uint16_t> PrevCollisions;
};
