#include "MPhysFS.hpp"
#include "Managers.hpp"
#include "Pipanikos.hpp"

#include <string>

void MPhysFS::Initialize(const KeyGameCoordinator&, const std::vector<std::string>& Args)
{
	if (PHYSFS_init(Args[0].c_str()) == 0)
	{
		throw(Pipanikos(PHYSFS_getLastError(), __FILE__, __LINE__));
	}
	else
	{
		Logger.Log(ELogLevel::Info, "MPhysFS initialized.");
	}
}

void MPhysFS::Shutdown(const KeyGameCoordinator&)
{
	if (PHYSFS_isInit() != 0)
	{
		if (PHYSFS_deinit() == 0)
		{
			throw(Pipanikos(PHYSFS_getLastError(), __FILE__, __LINE__));
		}
		else
		{
			Logger.Log(ELogLevel::Info, "MPhysFS shutdown.");
		}
	}
	else
	{
		Logger.Log(ELogLevel::Warning, "PHYSFS has not been initialized.");
	}
}

int MPhysFS::exists(std::string fname) const
{
	return PHYSFS_exists(fname.c_str());
}

int MPhysFS::mount(std::string newDir, std::string mountPoint, int appendToPath) const
{
	return PHYSFS_mount(newDir.c_str(), mountPoint.c_str(), appendToPath);
}

PHYSFS_sint64 MPhysFS::fileLength(BPhysFSFilePtr& handle) const
{
	PHYSFS_sint64 Length = PHYSFS_fileLength(handle);

	if (Length == -1)
	{
		throw(Pipanikos("Length is -1 for file " + handle.GetPath(), __FILE__, __LINE__));
	}

	return Length;
}
